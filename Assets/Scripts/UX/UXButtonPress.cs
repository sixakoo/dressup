﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    [RequireComponent (typeof(Button)), System.Serializable]
    public class UXButtonPress : MonoBehaviour //DD : test this
    {
        [SerializeField]
        public UX.AnimConfig config = UX.AnimConfig.Default;
        [SerializeField]
        public UX.AnimConfig preset = UX.AnimConfig.Default;
        [SerializeField]
        public Configuration configuration;
        [SerializeField, HideInInspector]
        private int index;
        [SerializeField, HideInInspector]
        public int Index
        {
            set
            {
                if (index != value)
                {
                    preset = UXManager.GetConfiguration(value);
                }
                index = value;
            }
            get
            {
                return index;
            }
        }

        public enum Configuration
        {
            Preset,
            Custom
        }

        private void AssignListener()
        {
            GetComponent<Button>().onClick.AddListener(delegate { });
        }
    }
}