﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UX
{
    public class UXDragSlot : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public float returnSpeed = 8;
        public bool dragOnSurfaces = true;
        [HideInInspector]
        public Image dragIcon;
        [HideInInspector]
        public Vector3 initialPos, startPos;
        //[HideInInspector]
        public Image image;
        //[HideInInspector]
        public Button button;
        public CanvasGroup canvasGroup;
        public string slotID;
        public UnityEngine.Events.UnityAction buttonDel;
        public bool dragable = true,
            useCustomValidZone = false,
            forceDisable = false;
        [HideInInspector]
        public bool
            validZone = false,
            isReturning;
            //isDragging;
        public bool hideImgWhenDrag = true;
        //[HideInInspector]
        //public bool dragging;
        [HideInInspector]
        public Vector2 initialSize;
        private RectTransform m_DraggingPlane;
        private Coroutine returnDragCoroutine;
        public static UXDragSlot draggedCard;

        public void Awake()
        {
            if(button == null)
            {
                if (!TryGetComponent(out button))
                {
                    button = gameObject.AddComponent<Button>();
                }
            }
            initialSize = new Vector2(image.rectTransform.rect.width, image.rectTransform.rect.height);
        }

        public void Start()
        {
            //if (canvasGroup == null)
            //{
            //    canvasGroup = GetComponent<CanvasGroup>();
            //}
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            //isDragging = true;
            if (draggedCard == null && dragable && !forceDisable)
            {
                draggedCard = this;
                var canvas = FindInParents<Canvas>(gameObject);
                if (canvas == null)
                {
                    return;
                }

                if (dragIcon != null)
                {
                    Destroy(dragIcon.gameObject);
                    this.image.enabled = true;
                    //if (returnDragCoroutine != null)
                    //{
                    //    isReturning = false;
                    //    //isRetrieving = false;
                    //    StopCoroutine(returnDragCoroutine);
                    //}
                }

                GameObject go = new GameObject("icon");
                go.transform.SetParent(canvas.transform, false);
                go.transform.SetAsLastSibling();
                dragIcon = go.AddComponent<Image>();

                image.sprite = this.image.sprite;
                if(hideImgWhenDrag)
                {
                    this.image.enabled = false;
                }
                RectTransform rt = ((RectTransform)transform);
                dragIcon.type = this.image.type;
                dragIcon.rectTransform.sizeDelta = new Vector2(rt.rect.width, rt.rect.height);
                dragIcon.rectTransform.anchorMin = rt.anchorMin;
                dragIcon.rectTransform.anchorMax = rt.anchorMax;
                dragIcon.rectTransform.pivot = rt.pivot;
                dragIcon.raycastTarget = false;

                startPos = ((RectTransform)transform).position;
                initialPos = ((Vector2)((RectTransform)transform).position) - eventData.position;

                if (dragOnSurfaces)
                {
                    m_DraggingPlane = transform as RectTransform;
                }
                else
                {
                    m_DraggingPlane = canvas.transform as RectTransform;
                }
                SetDraggedPosition(eventData);
                //dragging = true;
            }
        }

        public void OnDrag(PointerEventData data)
        {
            if (forceDisable)
                return;

            if (dragIcon != null)
            {
                SetDraggedPosition(data);
            }
        }

        private void SetDraggedPosition(PointerEventData data)
        {
            if (dragOnSurfaces && data.pointerEnter != null && data.pointerEnter.transform as RectTransform != null)
            {
                m_DraggingPlane = data.pointerEnter.transform as RectTransform;
                if (m_DraggingPlane == null)
                { return; }
            }

            var rt = dragIcon.transform as RectTransform;
            Vector3 globalMousePos;
            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlane, data.position, data.pressEventCamera, out globalMousePos))
            {
                rt.position = globalMousePos + initialPos;
                rt.rotation = m_DraggingPlane.rotation;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            //isDragging = false;
            //Debug.Log("+++++ " + gameObject.name +  ", isDragging: " + isDragging);
            if (!dragable)
            {
                return;
            }
            if(draggedCard == this)
            {
                draggedCard = null;
            }
            if (/*dragging &&*/ !forceDisable)
            {
                if (!useCustomValidZone /*&& dragable*/)
                {
                    if (RectTransformUtility.RectangleContainsScreenPoint(DrawArea.rectTransform, eventData.position, UXManager.uiCamera))//eventData.position, eventData.pressEventCamera))
                    {
                        validZone = true;
                        if (dragIcon != null)
                        {
                            Destroy(dragIcon.gameObject);
                        }
                    }
                    else
                    {
                        //dragable = false;
                        validZone = false;
                        //returnDragCoroutine = StartCoroutine(ReturnDrag());
                    }
                }
            }
        }

        public bool CustomValidZoneHandler(bool valid)
        {
            //if (dragable)
            //{
            if (valid)
            {
                validZone = true;
                if (dragIcon != null)
                {
                    Destroy(dragIcon.gameObject);
                }
            }
            else
            {
                validZone = false;
                //returnDragCoroutine = StartCoroutine(ReturnDrag());
            }
            //}
            return valid;
        }

        static public T FindInParents<T>(GameObject go) where T : Component
        {
            if (go == null) return null;

            if (go.TryGetComponent<T>(out var comp))
            { return comp; }

            Transform t = go.transform.parent;
            while (t != null && comp == null)
            {
                t.gameObject.TryGetComponent<T>(out comp);
                t = t.parent;
            }
            return comp;
        }

        //public IEnumerator ReturnDrag()
        //{
        //    dragable = false;
        //    isReturning = true;
        //    bool done = false;
        //    if (dragIcon != null)
        //    {
        //        while (!done)
        //        {
        //            if (dragIcon == null)
        //            {
        //                break;
        //            }
        //            dragIcon.rectTransform.position = Vector2.Lerp(dragIcon.rectTransform.position, startPos, Time.deltaTime * returnSpeed);
        //            if(Vector2.Distance(dragIcon.rectTransform.position, startPos) < 2f)
        //            {
        //                done = true;
        //            }
        //            yield return null;
        //        }
        //        if (dragIcon != null)
        //        {
        //            Destroy(dragIcon.gameObject);
        //        }
        //        this.image.enabled = true;
        //    }
        //    //isReturning = false;
        //    dragable = true;
        //    //ResetSlot();
        //    yield return new WaitForSeconds(0.1f);
        //    dragable = true;
        //}

        public void Interactable(bool interactable)
        {
            button.interactable = interactable;
            canvasGroup.interactable = interactable;
            canvasGroup.blocksRaycasts = interactable;
            dragable = interactable;
        }

        public void ForceDisable(bool availability)
        {
            forceDisable = availability;
        }

        //public void ResetSlot()
        //{
        //    //if (returnDragCoroutine != null)
        //    //{
        //    //    isReturning = false;
        //    //    StopCoroutine(returnDragCoroutine);
        //    //}
        //    if (dragIcon != null)
        //    {
        //        Destroy(dragIcon.gameObject);
        //    }
        //    this.image.enabled = true;
            
        //    //isReturning = false;
        //}
    }
}