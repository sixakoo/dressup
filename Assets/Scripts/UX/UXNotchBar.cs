﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXNotchBar : MonoBehaviour
    {
        public int totalNotch;
        public int val;
        public UXAnimator notchPrefab;
        public Image slotPrefab;
        public float notchInterval = 0.2f;
        public float notchSpacing = 3f;
        private Color initialColor;
        private List<UXAnimator> notches;
        private List<Image> notchImages;
        private Coroutine animCo;

        public void LoadBar(Color teamColor, int total = 8)
        {
            initialColor = teamColor;
            notches = new List<UXAnimator>();
            notchImages = new List<Image>();
            Clear();
            totalNotch = total;
            val = total;
            Rect barRect = ((RectTransform)transform).rect;
            Vector2 notchSpace = new Vector2(barRect.width / totalNotch, barRect.height);
            Vector2 notchSize = new Vector2(notchSpace.x - ((notchSpacing * (totalNotch - 1)) / totalNotch), notchSpace.y);
            for (int i = 0; i < totalNotch; i++)
            {
                Vector2 pos = new Vector2(i * notchSpace.x, 0);
                Image slot = Instantiate(slotPrefab, this.transform);
                slot.rectTransform.sizeDelta = notchSize;
                slot.rectTransform.anchoredPosition = pos;
                UXAnimator notch = Instantiate(notchPrefab, this.transform);
                RectTransform notchRt = ((RectTransform)notch.transform);
                notchRt.sizeDelta = notchSize;
                notchRt.anchoredPosition = pos;
                notches.Add(notch);
                Image img = notch.GetComponent<Image>();
                img.color = initialColor;
                notchImages.Add(img);
            }
        }

        public void UpdateBar(int newValue)
        {
            if(animCo != null)
            {
                StopCoroutine(animCo);
            }
            animCo = StartCoroutine(AnimateNotch(newValue));
        }

        private IEnumerator AnimateNotch(int newValue)
        {
            Refresh();
            //Increase
            if (newValue > val)
            {
                for (int i = 0; i < notches.Count; i++)
                {
                    if (i >= val && i < newValue)
                    {
                        notches[i].Play(0);
                        val++;
                        yield return new WaitForSeconds(notchInterval);
                    }
                }
            }
            else//Decrease
            {
                for (int i = notches.Count - 1; i >= 0; i--)
                {
                    if(i >= newValue && i < val)
                    {
                        notches[i].Play(1);
                        val--;
                        yield return new WaitForSeconds(notchInterval);
                    }
                }
            }
            val = newValue;
        }

        public void Clear()
        {
            foreach(Transform t in transform)
            {
                Destroy(t.gameObject);
            }
        }

        public void Refresh()
        {
            Rect barRect = ((RectTransform)transform).rect;
            Vector2 notchSpace = new Vector2(barRect.width / totalNotch, barRect.height);
            for (int i = 0; i < notches.Count; i++)
            {
                //reset position
                Vector2 pos = new Vector2(i * notchSpace.x, 0);
                RectTransform notchRt = ((RectTransform)notches[i].transform);
                notchRt.anchoredPosition = pos;
                //Reset alpha
                if (i < val) 
                {
                    notches[i].cg.alpha = 1;
                }
                else
                {
                    notches[i].cg.alpha = 0;
                }
            }
        }

        public void HighlightDamage(int damage, bool isShielded)
        {
            float output = isShielded ? damage /*+ Chess.Piece.StatusEffect.DefendMod*/ : damage;
            for (int i = notchImages.Count - 1; i >= 0; i--)
            {
                if (i >= val - output && i < val)
                {
                    notchImages[i].color = Color.red;
                }
            }
        }

        public void ClearHighlight()
        {
            for (int i = notchImages.Count - 1; i >= 0; i--)
            {
                notchImages[i].color = initialColor;
            }
        }
    }
}