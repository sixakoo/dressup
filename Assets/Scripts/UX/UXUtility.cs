﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace UX
{
    public class UXUtility : MonoBehaviour
    {
        public static string NumberRounding(int number)
        {
            if (number >= 1000 && number < 1000000)
            {
                return (((float)(number / 100)) / 10).ToString() + "K";
            }
            else if (number >= 1000000)
            {
                return (((float)(number / 100000)) / 10).ToString() + "M";
            }
            else
            {
                return number.ToString();
            }
        }

        public static void OnEnter(UnityEngine.Events.UnityAction _func)
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                _func();
            }
        }

        public static Rect RectTransformToScreenSpace(RectTransform transform)
        {
            Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
            return new Rect((Vector2)transform.position - (size * 0.5f), size);
        }

        public static bool OpenGallery(GalleryProcessDel func)
        {
            string[] imgInfo = GetPathFromGallery();
            if (imgInfo != null)
            {
                UXManager.RunCoroutine(LoadFromGallery(imgInfo[0], imgInfo[1], func));
                return true;
            }
            else
            {
                return false;
            }
        }

        private static string[] GetPathFromGallery()
        {
            AndroidJavaClass AndroidJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject Androidjo = AndroidJC.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass jc = new AndroidJavaClass("com.iservetech.galleryplugin.GalleryPlugin");
            jc.CallStatic("StartOpenGallery", Androidjo);
            string img_path = "Pending";
            string orientation = "Pending";
            do
            {
                img_path = jc.CallStatic<string>("returnPath");
                orientation = jc.CallStatic<string>("returnOrientation");
            }
            while (img_path == "Pending" || orientation == "Pending");

            if (orientation != "Activity Canceled")
            {
                Debug.Log("Received path: " + img_path);

                return new string[] { img_path, orientation };
            }
            else
            {
                return null;
            }
        }

        public delegate IEnumerator GalleryProcessDel(Texture2D texture);

        static Texture2D galleryImg;

        private static IEnumerator LoadFromGallery(string img_path, string orientation, GalleryProcessDel func)
        {
            DestroyImmediate(galleryImg);
            galleryImg = new Texture2D(2, 2);
            WWW w = new WWW(img_path);
            yield return w;
            switch (orientation)
            {
                default:
                    w.LoadImageIntoTexture(galleryImg);

                    //test = w.texture;
                    break;
                case "rotate_90":
                    galleryImg = RotateImage(w.texture, 90);
                    break;
                case "rotate_180":
                    galleryImg = RotateImage(w.texture, 180);
                    break;
                case "rotate_270":
                    galleryImg = RotateImage(w.texture, 270);
                    break;
                case "flip_hor":
                    galleryImg = FlipImage(w.texture, "horizontal");
                    break;
                case "flip_ver":
                    galleryImg = FlipImage(w.texture, "vertical");
                    break;
            }
            UXManager.RunCoroutine(func(galleryImg));
            //DestroyImmediate(buf_img, true);
        }

        public static Texture2D RotateImage(Texture2D originalTexture, int angle)
        {
            Texture2D result = null;
            int _height = originalTexture.height;
            int _width = originalTexture.width;
            if (angle != 90 && angle != 180 && angle != 270)
            {
                return result;
            }
            else
            {
                if (angle == 90 || angle == 270)
                {
                    result = new Texture2D(_height, _width);
                    for (int i = 0; i < _width; i++)
                    {
                        for (int j = 0; j < _height; j++)
                        {
                            if (angle == 90)
                                result.SetPixel(j, _width - 1 - i, originalTexture.GetPixel(i, j));
                            else if (angle == 270)
                                result.SetPixel(_height - 1 - j, i, originalTexture.GetPixel(i, j));
                        }
                    }
                }
                else if (angle == 180)
                {
                    result = new Texture2D(_width, _height);
                    for (int i = 0; i < _width; i++)
                    {
                        for (int j = 0; j < _height; j++)
                        {
                            result.SetPixel(_width - 1 - i, _height - 1 - j, originalTexture.GetPixel(i, j));
                        }
                    }
                }
            }
            return result;
        }

        public static Texture2D FlipImage(Texture2D originalTexture, string direction)
        {
            int _x = originalTexture.width;
            int _y = originalTexture.height;
            Texture2D flipped = new Texture2D(_x, _y);


            for (int i = 0; i < _x; i++)
            {
                for (int j = 0; j < _y; j++)
                {
                    if (direction == "horizontal")
                    {
                        flipped.SetPixel(_x - 1 - i, j, originalTexture.GetPixel(i, j));
                    }
                    else if (direction == "vertical")
                    {
                        flipped.SetPixel(i, _y - 1 - j, originalTexture.GetPixel(i, j));
                    }
                }
            }

            return flipped;
        }

        public static Texture2D RescaleImage(Texture2D originalTexture)
        {
            int _height = originalTexture.height;
            int _width = originalTexture.width;
            float factor = 0.0f;
            float size_limit = 1000.0f;
            int new_height = _height;
            int new_width = _width;
            if (_height > _width && _height > size_limit)
            {
                factor = size_limit / _height;
                new_width = (int)(_width * factor);
                new_height = (int)size_limit;
            }
            else if (_width > _height && _width > size_limit)
            {
                factor = size_limit / _width;
                new_width = (int)(_height * factor);
                new_height = (int)size_limit;
            }
            //TextureScale.Bilinear(originalTexture, new_width, new_height);
            originalTexture.Resize(new_width, new_height);

            return originalTexture;
        }

        public static void ClearTextFile(string path)
        {
            File.WriteAllText(path, "");
        }

        public static void WriteTextFile(string path, string text)
        {
            StreamWriter writer = new StreamWriter(path, true);
            writer.Write(text);
            writer.Close();
        }

        public static string ReadTextFile(string path)
        {
            StreamReader reader = new StreamReader(path);
            string t = reader.ReadToEnd();
            reader.Dispose();
            return t;
        }
    }

    public static class UI_Extension
    {
        public static string NumberRounding(this int number)
        {
            if (number >= 1000 && number < 1000000)
            {
                return (((float)(number / 100)) / 10).ToString() + "K";
            }
            else if (number >= 1000000)
            {
                return (((float)(number / 100000)) / 10).ToString() + "M";
            }
            else
            {
                return number.ToString();
            }
        }

        public static string AddComma(this int number)
        {
            string s = number.ToString();
            if(s.Length > ((number >= 0)?  3 : 4))
            {
                int j = 0;
                for (int i = 0; i < s.Length; i+=3)
                {
                    if (s.Length > i + j + ((number >= 0) ? 3 : 4))
                    {
                        s = s.Insert(s.Length - (i + j + 3), ",");
                        j += 1;
                    }
                }
                return s;
            }
            else
            {
                return number.ToString();
            }
        }

        public static string AddComma(this string s)
        {
            int number;
            if (!int.TryParse(s, out number))
            {
                return s;
            }
            if (s.Length > ((number >= 0) ? 3 : 4))
            {
                int j = 0;
                for (int i = 0; i < s.Length; i += 3)
                {
                    if (s.Length > i + j + ((number >= 0) ? 3 : 4))
                    {
                        s = s.Insert(s.Length - (i + j + 3), ",");
                        j += 1;
                    }
                }
                return s;
            }
            else
            {
                return s;
            }
        }

        public static Text ChangeText(this Button btn, string _text)
        {
            if (btn != null)
            {
                Text temp = btn.GetComponentInChildren<Text>();
                if (temp)
                {
                    btn.GetComponentInChildren<Text>().text = _text;
                }
                else
                {
                    Debug.Log("There is no Text component in the button", btn);
                }
                return temp;
            }
            return null;
        }

        public static Text ChangeText(this Button btn, Color _color)
	    {		
	        if (btn != null)		
	        {		
	            Text temp = btn.GetComponentInChildren<Text>();		
	            if (temp)		
	            {		
	                btn.GetComponentInChildren<Text>().color = _color;		
	            }		
	            else		
	            {		
	                Debug.Log("There is no Text component in the button", btn);		
	            }		
	            return temp;		
	        }		
	        return null;		
	    }

        public static Text OnText(this Button btn, bool _on)
        {
            if (btn != null)
            {
                Text temp = btn.GetComponentInChildren<Text>();
                if (temp)
                {
                    temp.gameObject.SetActive(_on);
                }
                else
                {
                    //Debug.Log("There is no RawImage component in the button", btn);
                }
                return temp;
            }
            return null;
        }

        public static RawImage ChangeRawImage(this Button btn, Texture _texture)
        {
            if (btn != null)
            {
                RawImage temp = btn.GetComponentInChildren<RawImage>();
                if (temp)
                {
                    btn.GetComponentInChildren<RawImage>().texture = _texture;
                }
                else
                {
                    //Debug.Log("There is no RawImage component in the button", btn);
                }
                return temp;
            }
            return null;
        }

        public static RawImage OnRawImage(this Button btn, bool _on)
        {
            if (btn != null)
            {
                RawImage temp = btn.GetComponentInChildren<RawImage>();
                if (temp)
                {
                    temp.gameObject.SetActive(_on);
                }
                else
                {
                    //Debug.Log("There is no RawImage component in the button", btn);
                }
                return temp;
            }
            return null;
        }

        public static void ChangeTabText(this Toggle toggle, string newName)
        {
            Text temp = toggle.GetComponentInChildren<Text>();

            if (temp)
            {
                toggle.GetComponentInChildren<Text>().text = newName;
            }
            else
            {
                Debug.Log("There is no Text component in ", toggle);
            }
        }

        public static void AdjustToRatio(this RawImage _rawImage, float _targetSize)
        {
            if (_rawImage != null && _rawImage.texture != null)
            {
                float ratio, adjusted;
                if (_rawImage.texture.width > _rawImage.texture.height)
                {
                    ratio = (float)_rawImage.texture.height / (float)_rawImage.texture.width;
                    adjusted = _targetSize * ratio;
                    LayoutElement le = _rawImage.GetComponent<LayoutElement>();
                    if (le != null)
                    {
                        le.minWidth = _targetSize;
                        le.minHeight = adjusted;
                    }
                    else
                    {
                        _rawImage.rectTransform.sizeDelta = new Vector2(_targetSize, adjusted);
                    }
                }
                else
                {
                    ratio = (float)_rawImage.texture.width / (float)_rawImage.texture.height;
                    adjusted = _targetSize * ratio;
                    _rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(adjusted, _targetSize);
                    LayoutElement le = _rawImage.GetComponent<LayoutElement>();
                    if (le != null)
                    {
                        le.minWidth = adjusted;
                        le.minHeight = _targetSize;
                    }
                    else
                    {
                        _rawImage.rectTransform.sizeDelta = new Vector2(adjusted, _targetSize);
                    }
                }
            }
        }

        public static bool IsLanscape(this RawImage _rawImage)
        {
            return _rawImage.texture.width > _rawImage.texture.height ? true : false;
        }

        public static bool IsEmpty(this Color color)
        {
            if (color.r == 0 && color.g == 0 && color.b == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string RemoveInstanceTag(this string s)
        {
            if (s.Contains(" (Instance)"))
            {
                s = s.Replace(" (Instance)", "");
            }
            return s;
        }

        public static string FirstCharToUpper(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                Debug.LogError("input should not be empty");
            }
            return char.ToUpper(input[0]) + input.Substring(1);
        }
    }
}