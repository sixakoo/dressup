﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXTab : MonoBehaviour
    {
        public Button[] tabButtons;
        public RectTransform[] tabContents;
        public Color selectedColor;
        public Color normalColor;
        private const float variant = 0.05f;

        private void Awake()
        {
            for(int i = 0; i < tabButtons.Length; i++)
            {
                int j = i;
                tabButtons[i].onClick.AddListener(() => TogglePanel(j));
            }
        }

        public void TogglePanel(int index)
        {
            Color unselected = normalColor;
            for (int i = 0; i < tabContents.Length; i++)
            {
                if(i == index)
                {
                    tabContents[i].gameObject.gameObject.SetActive(true);
                    tabButtons[i].image.color = selectedColor;
                }
                else
                {
                    tabContents[i].gameObject.gameObject.SetActive(false);
                    tabButtons[i].image.color = unselected;
                    unselected = new Color(unselected.r - variant, unselected.g - variant, unselected.b - variant);
                }
            }
        }
    }
}