﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UX
{
    public class UXSequencer : MonoBehaviour
    {
        [SerializeField, HideInInspector]
        public List<UXComponent> childUX;
        public float interval = 0.2f;
        public bool firstTimeOnly = false;
        public bool runOnEnable = true;
        public ArrangeChild arrangeChild;
        [HideInInspector]
        public int spacing = 0;
        private bool firstTime;
        private Coroutine activateCo;

        public void GetChildUX()
        {
            childUX = new List<UXComponent>();
            UXComponent[] arr = GetComponentsInChildren<UXComponent>(true);
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].triggerType == UXComponent.TriggerType.Sequence)
                {
                    childUX.Add(arr[i]);
                }
            }
        }

        public enum ArrangeChild
        {
            None,
            Horizontal,
            Vertical
        }

        public void ActivateSequence()
        {
            if(activateCo != null)
            {
                UXManager.StopCo(activateCo);
            }
            activateCo = UXManager.RunCoroutine(OnSequence());
        }

        public void DeactivateSequence()
        {
            if (activateCo != null)
            {
                UXManager.StopCo(activateCo);
            }
            activateCo = UXManager.RunCoroutine(OffSequence());
        }

        private IEnumerator OnSequence()
        {
            float timer = 0;
            float floor = 0;
            int i = 0;
            while (timer < childUX.Count * interval)
            {
                timer += Time.deltaTime;
                floor = Mathf.Floor(timer / interval);
                if (i != floor)
                {
                    i = (int)floor;
                    if((i - 1) < childUX.Count)
                    {
                        UX.OpenAnimation(childUX[i - 1].ID);
                    }
                }
                yield return null;
            }
            //check for missing child
            for (int j = 0; j < childUX.Count; j++)
            {
                if(childUX[j].gameObject.activeSelf == false)
                {
                    UX.OpenAnimation(childUX[j].ID);
                }
            }
        }

        private IEnumerator OffSequence()
        {
            float timer = 0;
            float floor = 0;
            int i = 0;
            while (timer < childUX.Count * interval)
            {
                timer += Time.deltaTime;
                floor = Mathf.Floor(timer / interval);
                if (i != floor)
                {
                    i = (int)floor;
                    if ((i - 1) < childUX.Count)
                    {
                        UX.CloseAnimation(childUX[i - 1].ID);
                    }
                }
                yield return null;
            }
            //check for missing child
            for (int j = 0; j < childUX.Count; j++)
            {
                if (childUX[j].gameObject.activeSelf == true)
                {
                    UX.CloseAnimation(childUX[j].ID);
                }
            }
        }

        private void Awake()
        {
            firstTime = true;
        }

        private void OnEnable()
        {
            ControlChildSize();
            if (firstTimeOnly)
            {
                if (firstTime)
                {
                    StartCoroutine(LoadSequence());
                }
            }
            else
            {
                StartCoroutine(LoadSequence());
            }
        }

        private IEnumerator LoadSequence()
        {
            yield return new WaitUntil(() => UXManager.ready);
            if (runOnEnable)
            {
                DeactivateAll();
                ActivateSequence();
                firstTime = false;
            }
        }

        private void DeactivateAll()
        {
            for (int i = 0; i < childUX.Count; i++)
            {
                childUX[i].Off();
            }
        }

        public void ControlChildSize()
        {
            RectTransform thisRect = ((RectTransform)transform);
            switch (arrangeChild)
            {
                case ArrangeChild.None:
                    break;
                case ArrangeChild.Horizontal:
                    Vector2 childSize2 = new Vector2((thisRect.sizeDelta.x - ((childUX.Count - 1) * spacing)) / childUX.Count, thisRect.sizeDelta.y);
                    for (int i = 0; i < childUX.Count; i++)
                    {
                        RectTransform childRect = (RectTransform)childUX[i].transform;
                        childRect.anchorMin = Vector2.up;
                        childRect.anchorMax = Vector2.up;
                        childRect.sizeDelta = new Vector2(childSize2.x, childSize2.y);
                        childRect.anchoredPosition = new Vector2((childSize2.x * i) + (spacing * i) + GetPivotOffset(childRect).x, -GetPivotOffset(childRect).y);
                    }
                    break;
                case ArrangeChild.Vertical:
                    Vector2 childSize = new Vector2(thisRect.sizeDelta.x, (thisRect.sizeDelta.y - ((childUX.Count - 1) * spacing)) / childUX.Count);
                    for (int i = 0; i < childUX.Count; i++)
                    {
                        RectTransform childRect = (RectTransform)childUX[i].transform;
                        childRect.anchorMin = Vector2.up;
                        childRect.anchorMax = Vector2.up;
                        childRect.sizeDelta = new Vector2(childSize.x, childSize.y);
                        childRect.anchoredPosition = new Vector2(GetPivotOffset(childRect).x, ((childSize.y * -i) + (spacing * -i)) - GetPivotOffset(childRect).y);
                    }
                    break;
            }
        }

        private Vector2 GetPivotOffset(RectTransform rt)
        {
            //Vector2 offset = rt.sizeDelta * rt.pivot;
            Vector2 offset = new Vector2(rt.sizeDelta.x - (rt.pivot.x * rt.sizeDelta.x), rt.sizeDelta.y - (rt.pivot.y * rt.sizeDelta.y));
            return offset;
        }
    }
}