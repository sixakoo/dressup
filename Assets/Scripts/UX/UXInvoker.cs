﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UX
{
    public class UXInvoker : MonoBehaviour
    { //CONTINUE LATER

        private int index;
        public UnityEvent RunProcess;
        public UnityEvent InvokeAfterProcess;


        private void Awake()
        {
            SetButton();
        }

        public void SetButton()
        {
            gameObject.GetComponent<Button>().onClick.AddListener(TestFunc);
        }

        public void TestFunc()
        {
            InvokeAfterProcess.Invoke();
        }

        public void OpenWindow(string text)
        {
            Debug.Log(text);
        }
    }
}