﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UX; //Put this namespace to access UX class

public class ScriptExample : MonoBehaviour {

    public Button
        openFirstPanelBtn,
        openSecondPageBtn,
        closeFirstPanelBtn,
        closeSecondPageBtn;

    private void Start()
    {
        //Examples
        //openFirstPanelBtn.onClick.AddListener(delegate { OpenExample("someID"); });
        //closeFirstPanelBtn.onClick.AddListener(delegate { CloseExample("someID"); });
        //openFirstPanelBtn.onClick.AddListener(delegate { UX.ToggleAnimation("someID"); });
    }

    public void OpenExample(string id)
    {
        UX.UX.OpenAnimation(id); //Open animation
    }

    public void CloseExample(string id)
    {
        UX.UX.CloseAnimation(id); //Close animation
    }

    public void ToggleExample(string id)
    {
        UX.UX.ToggleAnimation(id); //Toggle animation
    }
}
