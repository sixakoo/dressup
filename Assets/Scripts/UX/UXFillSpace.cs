﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXFillSpace : MonoBehaviour
    {
        public Direction direction;

        public enum Direction
        {
            Vertical,
            Horizontal
        }

        private void OnEnable()
        {
            Refresh();
        }

        public void Refresh()
        {
            if(gameObject.activeInHierarchy)
            {
                StartCoroutine(RefreshCo());
            }
        }

        private IEnumerator RefreshCo()
        {
            yield return new WaitForEndOfFrame();
            RectTransform parent = transform.parent as RectTransform;
            RectTransform thisRt = transform as RectTransform;
            Vector2 occupied = Vector2.zero;
            foreach (RectTransform rt in parent)
            {
                if (rt != thisRt)
                {
                    if(rt.TryGetComponent(out LayoutElement le) && le.ignoreLayout)
                    {
                        continue;
                    }
                    occupied += rt.sizeDelta;
                }
            }
            thisRt.sizeDelta = direction == Direction.Vertical ?
                new Vector2(parent.rect.width, parent.rect.height - occupied.y) : new Vector2(parent.rect.width - occupied.x, parent.rect.height);
        }

        public void OnCanvasGroupChanged()
        {
            if(gameObject.activeInHierarchy)
            {
                Refresh();
            }
        }
    }
}