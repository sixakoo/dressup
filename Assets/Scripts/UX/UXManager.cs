﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UX
{
    public class UXManager : MonoBehaviour
    {
        [SerializeField, HideInInspector]
        public static List<UXParent> uxParentList;
        public List<UX.UXStyle> uxStyleList;
        public static UXManager Instance { private set; get; }
        public static UnityEngine.Events.UnityAction
            loadedDel,
            uxParentDel;
        public static bool ready;
        public static bool inMenu;
        public int
            animationFPS = 60,
            idleFPS = 60;
        public int frameRate;
        public static Camera uiCamera;
        public static Vector2 lastSize;
        private const float
            defaultScrollSpeed = 0.05f,
            scrollStopThreshold = 0.01f;
        private float lastScrollTarget;
        private ScrollRect lastScrollRect;
        private static Coroutine scrollCo;
        private static ScrollMode scrollMode;
        private enum ScrollMode
        {
            anchoredPosition,
            normalized
        }

        private void Awake()
        {
            //Temp.LoadAllMenuScene();
            AssignInstance();
            uiCamera = Camera.main;
        }

        private IEnumerator Start()
        {
            if(uxParentDel != null)
            {
                uxParentDel();
            }

            //UX.GetAllUXComponents(uxParentList);
            yield return null;
            if (loadedDel != null)
            {
                loadedDel();
            }
            ready = true;
            //UX.OpenAnimation("Menu");
        }

        private void FrameRateControl()
        {
            if(inMenu && !Input.anyKey)
            {
                if (UX.animationList.Count > 0 || UX.externalAnimList.Count > 0)
                {
                    Application.targetFrameRate = animationFPS;
                }
                else
                {
                    Application.targetFrameRate = idleFPS;
                }
            }
            else
            {
                Application.targetFrameRate = animationFPS;
            }
            frameRate = Application.targetFrameRate;
            //ScrollRect sr;
        }
        
        private void Update()
        {
            UX.AnimationListControl();
            //FrameRateControl();
        }

        public void AssignInstance()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.LogError("Another instance of UXManager is already instantiated", Instance);
                Debug.LogError("Destroying the extra UXManager", gameObject);
                Destroy(this);
            }
        }

        public static void AddToList(UXParent uxParent)
        {
            if(uxParentList == null)
            {
                uxParentList = new List<UXParent>();
            }
            if(!uxParentList.Contains(uxParent))
            {
                uxParentList.Add(uxParent);
            }
        }

        public static List<UX.UXStyle> GetUXStyleList()
        {
            Debug.Log("Get list");
            if (Instance.uxStyleList.Count < 1)
            {
                return new List<UX.UXStyle> { new UX.UXStyle() };
            }
            else
            {
                return Instance.uxStyleList;
            }
        }

        public string[] GetUXTypeNames()
        {
            List<UX.UXStyle> typeList = uxStyleList;
            string[] typeNames = new string[typeList.Count];
            for (int i = 0; i < typeList.Count; i++)
            {
                typeNames[i] = typeList[i].name;
            }

            return typeNames;
        }

        public string GetUXTypeName(int index)
        {
            return uxStyleList[index].name;
        }

        public static UX.AnimConfig GetConfiguration(int index)
        {
            if (Instance == null)
            {
                Debug.LogError("Default");
                return UX.AnimConfig.Default;
            }
            else
            {
                return Instance.uxStyleList[index].configuration;
            }
        }

        public static Coroutine RunCoroutine(IEnumerator _coroutine)
        {
            if(Instance == null)
            {
                Debug.LogError("Make sure to run UXManager before the coroutine");
                return null;
            }
            else
            {
                return Instance.StartCoroutine(_coroutine);
            }
        }

        public static void StopCo(Coroutine _coroutine)
        {
            if (Instance == null)
            {
                Debug.Log("Make sure to run UXManager before the coroutine");
            }
            else
            {
                if(_coroutine != null)
                {
                    Instance.StopCoroutine(_coroutine);
                }
            }
        }

        public static Vector2 GetInputPosition()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            return Input.mousePosition;
#endif
            if(Input.touchCount > 0) { Debug.Log(">0"); };
            return (Input.touchCount > 0)? Input.GetTouch(0).position : (Vector2)Input.mousePosition;
        }

        public static bool GetClickUp()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            return Input.GetMouseButtonUp(0);
#endif
            return (Input.touchCount > 0) ? Input.GetTouch(0).phase.Equals(TouchPhase.Ended) : false;
        }

        public static bool GetClick()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            return Input.GetMouseButton(0);
#endif
            return (Input.touchCount > 0) ? Input.GetTouch(0).phase.Equals(TouchPhase.Moved) : false;
        }

        public static bool GetClickDown()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            return Input.GetMouseButtonDown(0);
#endif
            return (Input.touchCount > 0) ? Input.GetTouch(0).phase.Equals(TouchPhase.Began) : false;
        }

        public void ClearUnusedTrigger()
        {
            UXComponent[] uxcList = Resources.FindObjectsOfTypeAll<UXComponent>();
            Debug.Log("uxcList : " + uxcList.Length);
            for (int i = 0; i < uxcList.Length; i++)
            {
                Debug.Log(uxcList[i].name, uxcList[i]);
            }

            //Resources.FindObjectsOfTypeAll
        }

        //public void CloseAllPanel()
        //{
        //    for(int i = 0; i < uxParentList.Count; i++)
        //    {
        //        uxParentList.scene
        //    }
        //}

        public static void ScrollToNormalized(ScrollRect scrollRect, float normalizedPosition = 0, float scrollSpeed = -1)
        {
            scrollMode = ScrollMode.normalized;
            if (scrollCo != null)
            {
                UXManager.StopCo(scrollCo);
                scrollCo = null;
            }
            scrollCo = UXManager.RunCoroutine(ScrollDownCo(scrollRect, normalizedPosition, scrollSpeed));
        }

        public static void ScrollTo(ScrollRect scrollRect, float anchoredPosition = 0, float scrollSpeed = -1)
        {
            scrollMode = ScrollMode.anchoredPosition;
            if (scrollCo != null)
            {
                UXManager.StopCo(scrollCo);
                scrollCo = null;
            }
            scrollCo = UXManager.RunCoroutine(ScrollDownCo(scrollRect, anchoredPosition, scrollSpeed, false));
        }

        public static void CancelScroll()
        {
            if (UXManager.GetClickDown())
            {
                if (scrollCo != null)
                {
                    UXManager.StopCo(scrollCo);
                    scrollCo = null;
                    if (scrollMode == ScrollMode.normalized)
                    {
                        Instance.lastScrollRect.verticalNormalizedPosition = Instance.lastScrollTarget;
                    }
                    else if (scrollMode == ScrollMode.anchoredPosition)
                    {
                        Instance.lastScrollRect.content.anchoredPosition = new Vector2(Instance.lastScrollRect.content.anchoredPosition.x, Instance.lastScrollTarget);
                    }
                }
            }
        }

        private static IEnumerator ScrollDownCo(ScrollRect scrollRect, float position, float scrollSpeed = -1, bool normalized = true)
        {
            Instance.lastScrollRect = scrollRect;
            Instance.lastScrollTarget = position;
            float current = normalized ? scrollRect.verticalNormalizedPosition : scrollRect.content.anchoredPosition.y;
            if (current < position)
            {
                while (current < position)
                {
                    current =
                        Mathf.Lerp(current + scrollStopThreshold, position, scrollSpeed == -1 ? defaultScrollSpeed : scrollSpeed);
                    if (!normalized)
                    {
                        if (current < 0)
                        {
                            scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, 0);
                            yield break;
                        }
                        else if (current > scrollRect.content.rect.height - scrollRect.viewport.rect.height)
                        {
                            scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, scrollRect.content.rect.height - scrollRect.viewport.rect.height);
                            yield break;
                        }
                    }
                    if (normalized)
                    {
                        scrollRect.verticalNormalizedPosition = current;
                    }
                    else
                    {
                        scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, current);
                    }
                    yield return null;
                }
            }
            else
            {
                while (scrollRect && current > position)
                {
                    current =
                        Mathf.Lerp(current - scrollStopThreshold, position, scrollSpeed == -1 ? defaultScrollSpeed : scrollSpeed);
                    if (!normalized)
                    {
                        if (current < 0)
                        {
                            scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, 0);
                            yield break;
                        }
                        else if (current > scrollRect.content.rect.height - scrollRect.viewport.rect.height)
                        {
                            scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, scrollRect.content.rect.height - scrollRect.viewport.rect.height);
                            yield break;
                        }
                    }
                    if (normalized)
                    {
                        scrollRect.verticalNormalizedPosition = current;
                    }
                    else
                    {
                        scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, current);
                    }
                    yield return null;
                }
            }
            if (normalized)
            {
                scrollRect.verticalNormalizedPosition = position;
                scrollCo = null;
            }
            else
            {
                scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, position);
                scrollCo = null;
            }
        }
    }
}
