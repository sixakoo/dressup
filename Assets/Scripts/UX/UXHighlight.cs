﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UX;

public class UXHighlight : MonoBehaviour
{
    public UXAnimator anim;

    public void SetHighlight(Transform source)
    {
        transform.position = source.position;
        anim.Play();
    }
}
