﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace UX
{
    public class UXPopper : MonoBehaviour
    {

        public Button[] buttons;
        public Shadow[] shadows;

        public Vector2 distance = Vector2.one * -20;
        public Color shadowColor = Color.black;
        public float duration = 1;
        public AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1);

        public bool autoToggle = true;

        private float timer, animVal;
        private int lastSelected;
        private Vector2[]
            initialPosition,
            initialDistance;
        private bool play;
        private int index;

        private void Start()
        {
            SetupShadows();
        }

        private void Update()
        {
            if (play)
            {
                AnimateShadow();
            }
        }

        public void SetupShadows()
        {
            if (buttons.Length != shadows.Length)
            {
                Debug.LogError("Button count and shadow count does not match");
            }
            else
            {
                initialPosition = new Vector2[shadows.Length];
                initialDistance = new Vector2[shadows.Length];
                for (int i = 0; i < buttons.Length; i++)
                {
                    int j = i;
                    if (autoToggle)
                    {
                        buttons[i].onClick.AddListener(delegate { ToggleShadows(j); });
                    }

                    shadows[j].effectColor = Color.clear;
                    shadows[j].effectDistance = Vector2.zero;
                    initialPosition[j] = ((RectTransform)shadows[j].transform).anchoredPosition;
                    initialDistance[j] = shadows[j].effectDistance;
                }
            }
        }

        public void ToggleShadows(int i)
        {

            if (lastSelected != index)
            {
                lastSelected = index;
            }
            index = i;
            timer = 0;
            play = true;
        }

        public void AnimateShadow()
        {
            timer += Time.deltaTime / duration;
            animVal = Mathf.Lerp(0, 1, curve.Evaluate(timer));
            shadows[index].effectDistance = initialDistance[index] + ((distance - initialDistance[index]) * animVal);
            shadows[index].effectColor = shadowColor;
            Vector2 target = initialPosition[index] - distance;
            ((RectTransform)shadows[index].transform).anchoredPosition = initialPosition[index] + ((target - initialPosition[index]) * animVal);

            for (int i = 0; i < shadows.Length; i++)
            {
                if (i != index)
                {
                    target = initialPosition[i] - ((RectTransform)shadows[i].transform).anchoredPosition;
                    ((RectTransform)shadows[i].transform).anchoredPosition = ((RectTransform)shadows[i].transform).anchoredPosition + (target * animVal);
                    target = initialDistance[i] - shadows[i].effectDistance;
                    shadows[i].effectDistance = shadows[i].effectDistance + (target * animVal);
                }
            }

            if (timer > 1)
            {
                play = false;
            }
        }
    }
}