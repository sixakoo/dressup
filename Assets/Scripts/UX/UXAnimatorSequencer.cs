﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXAnimatorSequencer : MonoBehaviour
    {
        //[HideInInspector]
        public List<UXAnimator> animatorList;
        [HideInInspector]
        public List<UXCardFlip> flipList;
        [Range(0,1)]
        public float interval = 0.2f;
        public bool loop, playOnStart, overrideInitial;
        [Range(0, 5)]
        public float startDelay = 0.0f;
        Coroutine animSequenceCo, flipSequenceCo;
        [HideInInspector]
        public bool isDone;
        public int startAnimIndex = -1;

        private Coroutine startCo;

        public void Awake()
        {
            GetUXAnimators();
            GetUXCardFlip();
            if (playOnStart)
            {
                if (startCo != null)
                {
                    UXManager.StopCo(startCo);
                }
                startCo = UXManager.RunCoroutine(PlayOnStartEnumerator());
            }
        }

        public void OnEnable()
        {
            if (playOnStart)
            {
                if (startCo != null)
                {
                    UXManager.StopCo(startCo);
                }
                startCo = UXManager.RunCoroutine(PlayOnStartEnumerator());
            }
        }

        private IEnumerator PlayOnStartEnumerator()
        {
            yield return null;
            if(this == null)
            {
                yield break;
            }
            yield return new WaitForSeconds(startDelay);
            RunAnimatorSequence(false, startAnimIndex);
        }

        public void GetUXAnimators()
        {
            animatorList = new List<UXAnimator>();
            if (animSequenceCo != null)
            {
                UXManager.StopCo(animSequenceCo);
            }
            foreach (Transform t in transform)
            {
                if(t != transform)
                {
                    if (t.TryGetComponent(out UXAnimator uxa))
                    {
                        animatorList.Add(uxa);
                    }
                }
            }
        }

        public void GetUXCardFlip()
        {
            flipList = new List<UXCardFlip>();
            if (flipSequenceCo != null)
            {
                UXManager.StopCo(flipSequenceCo);
            }
            foreach (Transform t in transform)
            {
                if (t != transform)
                {
                    if (t.TryGetComponent(out UXCardFlip uxcf))
                    {
                        flipList.Add(uxcf);
                    }
                }
            }
        }

        public void CloseChildren()
        {
            foreach (UXAnimator uxc in animatorList)
            {
                uxc.gameObject.SetActive(false);
            }
            foreach (UXCardFlip uxcf in flipList)
            {
                uxcf.gameObject.SetActive(false);
            }
        }

        public void RunCardFlipSequence(bool back, bool refetchComponent = false)
        {
            if (refetchComponent)
            {
                GetUXAnimators();
            }
            if (flipSequenceCo != null)
            {
                UXManager.StopCo(flipSequenceCo);
            }
            flipSequenceCo = UXManager.RunCoroutine(CardFlipSequencer(back));
        }

        public void RunAnimatorSequence(bool refetchComponent = false, int index = -1)
        {
            if(this == null)
            {
                return;
            }
            if (refetchComponent || animatorList.Count == 0)
            {
                GetUXAnimators();
            }
            if (animSequenceCo != null)
            {
                UXManager.StopCo(animSequenceCo);
            }
            if(!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
            animSequenceCo = UXManager.RunCoroutine(AnimatorSequencer(UXAnimator.Direction.Preset, Vector2.zero, index, overrideInitial));
        }

        public void RunAnimatorSequence(UXAnimator.Direction direction, Vector2 target, int index)
        {
            if (animSequenceCo != null)
            {
                UXManager.StopCo(animSequenceCo);
            }
            animSequenceCo = UXManager.RunCoroutine(AnimatorSequencer(direction, target, index, overrideInitial));
        }

        public void StopAnimatorSequence()
        {
            if(animSequenceCo != null)
            {
                UXManager.StopCo(animSequenceCo);
            }
        }

        public void StopFlipCardSequence()
        {
            if (flipSequenceCo != null)
            {
                UXManager.StopCo(flipSequenceCo);
            }
        }

        public void ForceOnAllChildren(bool includeIgnore = true)
        {
            //Debug.LogError("Force on all children");
            GetUXAnimators();
            for (int i = 0; i < animatorList.Count; i++)
            {
                if(includeIgnore)
                {
                    animatorList[i].gameObject.SetActive(true);
                }
                else if(!animatorList[i].ignoreSequence)
                {
                    animatorList[i].gameObject.SetActive(true);
                }
            }
        }

        public void ShowAllChildren()
        {
            GetUXAnimators();
            for (int i = 0; i < animatorList.Count; i++)
            {
                if (animatorList[i].ignoreSequence)
                    continue;

                animatorList[i].gameObject.SetActive(true);
                animatorList[i].cg.alpha = 1;
            }
        }

        private IEnumerator AnimatorSequencer(UXAnimator.Direction direction, Vector2 target, int index, bool overrideInitial)
        {
            if (animatorList.Count <= 0)
            {
                yield break;
            }
            ResetAnimatorTriggers();
            isDone = false;
            repeat:
            float timer = 0;
            float floor = 0;
            int i = 0;
            while (timer < animatorList.Count * interval)
            {
                if (this == null)
                {
                    yield break;
                }
                timer += Time.deltaTime;
                floor = Mathf.Floor(timer / interval);
                if (i != floor)
                {
                    i = (int)floor;
                    if ((i - 1) < animatorList.Count)
                    {
                        for (int j = 1; j <= i; j++)
                        {
                            if (!animatorList[j - 1].ignoreSequence)
                            {
                                if (!animatorList[j - 1].sequenceList[index < 0? 0 : index].isTriggered)
                                {
                                    switch (direction)
                                    {
                                        case UXAnimator.Direction.Preset:
                                            animatorList[j - 1].Play(index, overrideInitial);
                                            break;
                                        case UXAnimator.Direction.ToTarget:
                                            animatorList[j - 1].MoveTo(target, index, overrideInitial);
                                            break;
                                        case UXAnimator.Direction.FromTarget:
                                            animatorList[j - 1].MoveFrom(target, index, overrideInitial);
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
                yield return null;
            }
            if(this == null)
            {
                yield break;
            }
            yield return new WaitForSeconds(animatorList[animatorList.Count -1].sequenceList[index != -1 ? index : 0].duration);
            //check for missing child
            if(this == null)
            {
                yield break;
            }
            for (int j = 0; j < animatorList.Count; j++)
            {
                if (animatorList[j].gameObject.activeSelf == false || !animatorList[j].sequenceList[index != -1 ? index : 0].isTriggered)
                {
                    if (!animatorList[j].ignoreSequence)
                    {
                        switch (direction)
                        {
                            case UXAnimator.Direction.Preset:
                                animatorList[j].Play(index, false);
                                break;
                            case UXAnimator.Direction.ToTarget:
                                animatorList[j].MoveTo(target, index, true);
                                break;
                            case UXAnimator.Direction.FromTarget:
                                animatorList[j].MoveFrom(target, index, true);
                                break;
                        }
                    }
                }
            }
            if(loop)
            {
                yield return null;
                goto repeat;
            }
            isDone = true;
        }

        private IEnumerator CardFlipSequencer(bool back)
        {
            ResetFlipTriggers();
            isDone = false;
            float timer = 0;
            float floor = 0;
            int i = 0;
            while (timer < flipList.Count * interval)
            {
                timer += Time.deltaTime;
                floor = Mathf.Floor(timer / interval);
                if (i != floor)
                {
                    i = (int)floor;
                    if ((i - 1) < flipList.Count)
                    {
                        for (int j = 1; j <= i; j++)
                        {
                            if (!flipList[j - 1].ignoreSequence)
                            {
                                if (!flipList[j - 1].isTriggered)
                                {
                                    flipList[j - 1].Flip(back);
                                }
                            }
                        }
                    }
                }
                yield return null;
            }

            if(flipList.Count > 0)
            {
                yield return new WaitForSeconds(flipList[0].duration);
            }

            for (int j = 0; j < flipList.Count; j++) //check for completed
            {
                if (!flipList[j].isTriggered)
                {
                    if (!flipList[j].ignoreSequence)
                    {
                        flipList[j].Flip(back);
                    }
                }
            }
            isDone = true;
        }

        private void ResetAnimatorTriggers()
        {
            foreach(UXAnimator uxa in animatorList)
            {
                foreach(UXAnimator.AnimationSequence uxas in uxa.sequenceList)
                {
                    uxas.isTriggered = false;
                }
            }
        }

        private void ResetFlipTriggers()
        {
            foreach(UXCardFlip uxc in flipList)
            {
                uxc.isTriggered = false;
            }
        }
    }
}