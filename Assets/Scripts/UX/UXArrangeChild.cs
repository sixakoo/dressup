﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UX
{
    public class UXArrangeChild : MonoBehaviour
    {

        public Direction direction;
        public float spacing;
        public List<RectTransform> children;

        public enum Direction
        {
            Horizontal,
            Vertical
        }

        public void ControlChildSize()
        {
            RectTransform thisRect = ((RectTransform)transform);
            switch (direction)
            {
                case Direction.Horizontal:
                    Vector2 childSize2 = new Vector2((thisRect.sizeDelta.x - ((children.Count - 1) * spacing)) / children.Count, thisRect.sizeDelta.y);
                    for (int i = 0; i < children.Count; i++)
                    {
                        RectTransform childRect = (RectTransform)children[i].transform;
                        childRect.anchorMin = Vector2.up;
                        childRect.anchorMax = Vector2.up;
                        childRect.sizeDelta = new Vector2(childSize2.x, childSize2.y);
                        childRect.anchoredPosition = new Vector2((childSize2.x * i) + (spacing * i) + GetPivotOffset(childRect).x, -GetPivotOffset(childRect).y);
                    }
                    break;
                case Direction.Vertical:
                    Vector2 childSize = new Vector2(thisRect.sizeDelta.x, (thisRect.sizeDelta.y - ((children.Count - 1) * spacing)) / children.Count);
                    for (int i = 0; i < children.Count; i++)
                    {
                        RectTransform childRect = (RectTransform)children[i].transform;
                        childRect.anchorMin = Vector2.up;
                        childRect.anchorMax = Vector2.up;
                        childRect.sizeDelta = new Vector2(childSize.x, childSize.y);
                        childRect.anchoredPosition = new Vector2(GetPivotOffset(childRect).x, ((childSize.y * -i) + (spacing * -i)) - GetPivotOffset(childRect).y);
                    }
                    break;
            }
        }

        private Vector2 GetPivotOffset(RectTransform rt)
        {
            //Vector2 offset = rt.sizeDelta * rt.pivot;
            Vector2 offset = new Vector2(rt.sizeDelta.x - (rt.pivot.x * rt.sizeDelta.x), rt.sizeDelta.y - (rt.pivot.y * rt.sizeDelta.y));
            return offset;
        }
    }
}