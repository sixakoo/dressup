﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    [RequireComponent (typeof(Text), typeof(UXAnimator))]
    public class UXValueChange : MonoBehaviour
    {
        private Text text;
        private UXAnimator uxanim;
        private string lastValue;

        private void Awake()
        {
            text = GetComponent<Text>();
            uxanim = GetComponent<UXAnimator>();
            lastValue = text.text;
        }

        private void Update()
        {
            if(lastValue != text.text)
            {
                TriggerAnimation();
                lastValue = text.text;
            }
        }

        private void TriggerAnimation()
        {
            uxanim.Play(true);
        }
    }
}