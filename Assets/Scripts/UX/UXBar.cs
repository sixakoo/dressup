﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXBar : MonoBehaviour
    {
        public Image
            slotRect,
            barRect;

        public void Start()
        {
            if (barRect == null)
                return;

            barRect.rectTransform.anchorMin = Vector2.up * 0.5f;
            barRect.rectTransform.anchorMax = Vector2.up * 0.5f;
            barRect.rectTransform.pivot = Vector2.up * 0.5f;
            //barRect.rectTransform.anchoredPosition = Vector2.zero;
            barRect.rectTransform.anchoredPosition = new Vector2(0, barRect.rectTransform.anchoredPosition.y);
        }

        public void SetBar(float min, float current, float max)
        {
            float val = (current - min) / (max - min);
            if(current<max)
            {
                barRect.rectTransform.sizeDelta = new Vector2(val * slotRect.rectTransform.rect.width, slotRect.rectTransform.rect.height);
            }
            else
            {
                barRect.rectTransform.sizeDelta = new Vector2(slotRect.rectTransform.rect.width, slotRect.rectTransform.rect.height);
            }
        }

        public void SetBar(float current)
        {
            barRect.rectTransform.sizeDelta = new Vector2(current * slotRect.rectTransform.rect.width, slotRect.rectTransform.rect.height);
        }
    }
}