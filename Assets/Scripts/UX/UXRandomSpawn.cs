﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXRandomSpawn : MonoBehaviour
    {
        public GameObject spawnObject;
        public float 
            duration = 5, 
            minInterval = 0, 
            maxInterval = 0.5f;
        public bool overrideScale;
        public Vector2
            minScale = Vector2.one,
            maxScale = Vector2.one;
        public bool runOnStart, runForever;
        private Coroutine co;

        private void OnEnable()
        {            
            if(runOnStart)
            {
                co = UXManager.RunCoroutine(Spawn());
            }
        }

        private void OnDisable()
        {
            if(co != null)
            {
                UXManager.StopCo(co);
            }
        }

        private IEnumerator Spawn()
        {
            RectTransform rt = (RectTransform)transform;
            float durationTimer = 0;
            float randomTimer = 0;
            float randomTime = 0;
            //rt.pivot = Vector2.zero;
            while (durationTimer < duration)
            {
                if(this == null)
                {
                    yield break;
                }
                if(!runForever)
                {
                    durationTimer += Time.deltaTime;
                }
                randomTimer += Time.deltaTime;
                if(randomTimer >= randomTime)
                {
                    randomTime = randomTimer + Random.Range(minInterval, maxInterval);
                    GameObject o = Instantiate(spawnObject, transform, false);
                    if (overrideScale)
                    {
                        o.transform.localScale = new Vector2(Random.Range(minScale.x, maxScale.x), Random.Range(minScale.y, maxScale.y));
                    }
                    RectTransform ot = (RectTransform)o.transform;
                    ot.anchorMin = Vector2.zero;
                    ot.anchorMax = Vector2.zero;
                    Vector2 randomPos = new Vector2(Random.Range(0, rt.rect.width), Random.Range(0, rt.rect.height));
                    ot.anchoredPosition = randomPos;
                }
                yield return null;
            }
        }
    }
}