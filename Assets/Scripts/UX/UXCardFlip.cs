﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXCardFlip : MonoBehaviour
    {
        [Range(0, 5)]
        public float
            duration = 1;
        public AnimationCurve
            animCurve = AnimationCurve.Linear(0,0,1,1);
        private Vector3
            rotate,
            ChangeThreshold = Vector3.up * 90f;
        [HideInInspector]
        public Sprite
            frontImg,
            backImg;
        public bool
            isBack { private set; get; }
        public bool
            isTriggered,
            ignoreSequence;
        public FlipDirection
            flipDirection;
        public bool isFlipped { private set; get; }
        private float
            val,
            rotVal;
        private Coroutine
            flipCo;
        private Vector3
            initialRot;

        public RectTransform
            frontContent,
            backContent;

        private bool
            endToResult;

        public enum FlipDirection
        {
            Horizontal,
            Vertical
        }

        public void Awake()
        {
            initialRot = transform.rotation.eulerAngles;
            isBack = false;
            SetState(false);
        }

        public void Update()
        {
            //test
            //if (Input.GetKeyDown(KeyCode.K))
            //{
            //    Flip(false);
            //}
            //if (Input.GetKeyDown(KeyCode.J))
            //{
            //    Flip(true);
            //}
        }

        public void OnDisable()
        {
            UX.externalAnimList.Remove(this);
            if (flipCo != null)
            {
                UXManager.StopCo(flipCo);
            }
        }

        public void OnDestroy()
        {
            UX.externalAnimList.Remove(this);
            if (flipCo != null)
            {
                UXManager.StopCo(flipCo);
            }
        }

        public void Flip(bool back)
        {
            isTriggered = true;
            if(flipCo != null)
            {
                UXManager.StopCo(flipCo);
            }
            flipCo = UXManager.RunCoroutine(AnimateFlip(back));
        }

        public IEnumerator AnimateFlip(bool back)
        {
            if (!UX.externalAnimList.Contains(this))
            {
                UX.externalAnimList.Add(this);
            }
            ((RectTransform)transform).eulerAngles = Vector3.zero;
            val = 0;
            rotVal = 0;
            endToResult = false;
            bool change = false;
            if(flipDirection == FlipDirection.Horizontal)
            {
                rotate = Vector3.up * 180;
                ChangeThreshold = Vector3.up * 90;
            }
            else
            {
                rotate = Vector3.right * 180;
                ChangeThreshold = Vector3.right * 90;
            }
            if (!back)
            {
                //cardImg.sprite = frontImg;
            }
            else
            {
                
                //cardImg.sprite = backImg;
            }
            while(val < 1)
            {
                if(endToResult)
                {
                    val = 1;
                }
                else
                {
                    val += Time.deltaTime / duration;
                }
                rotVal = animCurve.Evaluate(val);
                transform.eulerAngles = Rotate(rotVal, rotate, initialRot);
                if (flipDirection == FlipDirection.Horizontal)
                {
                    if (Quaternion.Angle(transform.rotation, Quaternion.Euler(rotate)) < ChangeThreshold.y)
                    {
                        change = true;
                    }
                }
                else
                {
                    if (Quaternion.Angle(transform.rotation, Quaternion.Euler(rotate)) < ChangeThreshold.x)
                    {
                        change = true;
                    }
                }
                if (change)
                {
                    if (!back)
                    {
                        isBack = true;
                        SetState(true);
                    }
                    else
                    {
                        isBack = false;
                        SetState(false);
                    }
                    if (flipDirection == FlipDirection.Horizontal)
                    {
                        frontContent.localScale = new Vector3(-1, 1, 1);
                        backContent.localScale = new Vector3(-1, 1, 1);
                    }
                    else
                    {
                        frontContent.localScale = new Vector3(1, -1, 1);
                        backContent.localScale = new Vector3(1, -1, 1);
                    }
                }
                yield return null;
            }
            transform.eulerAngles = Vector3.zero;
            frontContent.localScale = Vector3.one;
            backContent.localScale = Vector3.one;
            UX.externalAnimList.Remove(this);
            endToResult = false;
            isFlipped = true;
        }

        public void ShowEnd()
        {
            endToResult = true;
        }

        private static Vector3 Rotate(float x, Vector3 rotate, Vector3 initialRot)
        {
            return new Vector3(initialRot.x + (rotate.x * (x)), initialRot.y + (rotate.y * (x)), initialRot.z + (rotate.z * (x)));
        }

        public void CreateChild()
        {
            Rect tempRect = ((RectTransform)transform).rect;
        }

        public void SetState(bool back)
        {
            if (back)
            {
                isBack = true;
                backContent.gameObject.SetActive(true);
                frontContent.gameObject.SetActive(false);
            }
            else
            {
                isBack = false;
                backContent.gameObject.SetActive(false);
                frontContent.gameObject.SetActive(true);
                
            }
            //Debug.LogError("set back : " + name + " + " + back, this);
        }

        public void FlipReset()
        {
            if(flipCo != null)
            {
                UXManager.StopCo(flipCo);
            }
            val = 0;
            rotVal = 0;
            initialRot = transform.rotation.eulerAngles;
            isBack = false;
            SetState(false);
            isFlipped = false;
            isTriggered = false;
        }
    }
}