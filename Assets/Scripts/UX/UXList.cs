﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UX
{
    public class UXList : MonoBehaviour
    {
        public Text titleTxt;
        public RectTransform panel;
        public Button listEntryPrefab;
        public Button listBG;
        public int index;
        public UnityAction<int> onSelect;
        private const string
                LIST = "PieceGenerator/List";

        private void Start()
        {
            listBG.onClick.AddListener(Close);
        }

        public void Open()
        {
            UX.OpenAnimation(LIST);
        }

        public void Close()
        {
            UX.CloseAnimation(LIST);
        }

        public void SetList(string listTitle, List<string> list, UnityAction<int> action)
        {
            titleTxt.text = listTitle;
            Open();
            ClearList();
            onSelect = null;
            onSelect += action;
            for (int i = 0; i < list.Count; i++)
            {
                Button b = Instantiate(listEntryPrefab, panel, false);
                b.ChangeText(list[i]);
                int j = i;
                b.onClick.AddListener(()=> { onSelect?.Invoke(j); Close(); });
            }
        }

        private void ClearList()
        {
            foreach (Transform t in panel)
            {
                Destroy(t.gameObject);
            }
        }
    }
}