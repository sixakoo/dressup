﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UX {
    public class UXCoinScatter : MonoBehaviour {
        public RectTransform
            from,
            to;
        public float
            pauseTime = 0.5f;
        public int
            coinCount = 4;
        public Vector2
            scatteringRange = Vector2.one * 50f;
        public float
            intervalDifference = 0.1f;
        public UXAnimator
            coinSample;
        public bool IsDone { private set; get; }
        private List<UXAnimator>
            animatorList;
        public UnityAction onElementSpawn;
        public UnityAction onElementEnd;

        private Coroutine scatterCo;

        private void Start()
        {
            coinSample.gameObject.SetActive(false);
        }

        private void Update()
        {
            //if(Input.GetKeyDown(KeyCode.J))
            //{
            //    Play();
            //}
        }

        public void Play(bool from = false)
        {
            if (scatterCo != null)
            {
                UXManager.StopCo(scatterCo);
            }
            if (from)
            {
                scatterCo = UXManager.RunCoroutine(ScatterCoinsFrom());
            }
            else
            {
                scatterCo = UXManager.RunCoroutine(ScatterCoins());
            }
        }

        public void ForceDoneFalse()
        {
            IsDone = false;
        }

        private void Init()
        {
            IsDone = false;
            DestroyAllCoins();
            animatorList = new List<UXAnimator>();
            for (int i = 0; i < coinCount; i++)
            {
                animatorList.Add(Instantiate(coinSample, transform, false));
                RectTransform rt = ((RectTransform)animatorList[i].transform);
                Vector2 pos = GetRandomPos(rt, scatteringRange);
                rt.anchoredPosition = pos;
                animatorList[i].SetInitialPosition(pos);
                rt.gameObject.SetActive(false);
            }
        }

        private IEnumerator ScatterCoins()
        {
            Init();
            //move to
            for (int i = 0; i < animatorList.Count; i++)
            {
                yield return new WaitForSeconds(Random.Range(0, intervalDifference));
                animatorList[i].MoveTo(to.position, 0);
            }
            //wait for move to animation
            for (int i = 0; i < animatorList.Count; i++)
            {
                yield return new WaitUntil(() => animatorList[i].sequenceList[0].isDone);
            }
            DestroyAllCoins();
            IsDone = true;
        }

        private IEnumerator ScatterCoinsFrom()
        {
            Init();
            //move from
            for (int i = 0; i < animatorList.Count; i++)
            {
                yield return new WaitForSeconds(Random.Range(0, intervalDifference));
                onElementSpawn?.Invoke();
                StartCoroutine(ControlScatter(animatorList[i]));
            }
            for (int i = 0; i < animatorList.Count; i++)
            {
                yield return new WaitUntil(() => animatorList[i].sequenceList[1].isDone);
                onElementEnd?.Invoke();
            }
            DestroyAllCoins();
            IsDone = true;
        }

        private IEnumerator ControlScatter(UXAnimator coin)
        {
            
            coin.MoveFrom(from.position, 0);
            yield return new WaitUntil(() => coin.sequenceList[0].isDone);
            yield return new WaitForSeconds(pauseTime);
            coin.MoveTo(to.position, 1);
        }

        private void DestroyAllCoins()
        {
            if (animatorList != null)
            {
                for (int i = 0; i < animatorList.Count; i++)
                {
                    Destroy(animatorList[i].gameObject);
                }
            }
            animatorList = new List<UXAnimator>();
        }

        public void Stop()
        {
            UXManager.StopCo(scatterCo);
            if(this != null)
            {
                StopAllCoroutines();
                DestroyAllCoins();
            }
        }

        private Vector2 GetRandomPos(RectTransform rt, Vector2 range)
        {
            return new Vector2(Random.Range(rt.anchoredPosition.x - range.x / 2, rt.anchoredPosition.x + range.x / 2), Random.Range(rt.anchoredPosition.y - range.y / 2, rt.anchoredPosition.y + range.y / 2));
        }
    }
}