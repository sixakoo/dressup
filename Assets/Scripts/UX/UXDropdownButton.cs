﻿using UnityEngine;
//using unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UX
{
    [RequireComponent(typeof(Button))]
    public class UXDropdownButton : MonoBehaviour
    {
        public Button buttonPrefab;
        public RectTransform panel;
        public Vector2 cellSize = new Vector2(30f, 30f);
        public bool changeToSelected;
        public float spacing = 0;
        public Direction direction = Direction.Vertical;
        public Direction cellDirection = Direction.Horizontal;
        private List<Data> dataList;
        private List<Button> buttonList;
        private Button mainButton;
        private bool justClicked;
        private RectTransform rt;
        private int index;

        public enum Direction
        {
            Vertical,
            Horizontal
        }

        private void SetupDropdown()
        {
            panel.gameObject.SetActive(true);
            UXManager.Instance.StartCoroutine(SetupDirection(panel, direction));
            SetupPanel();
            mainButton = GetComponent<Button>();
            mainButton.onClick.RemoveAllListeners();
            mainButton.onClick.AddListener(delegate { ToggleChild(); });
            panel.gameObject.SetActive(false);
            justClicked = true;
            RectTransform bprt = buttonPrefab.GetComponent<RectTransform>();
            cellSize = new Vector2(cellSize.x == 0 ? bprt.sizeDelta.x : cellSize.x, cellSize.y == 0 ? bprt.sizeDelta.y : cellSize.y);
        }

        private void Start()
        {
            if (dataList == null)
            {
                dataList = new List<Data>();
            }
            rt = panel.GetComponent<RectTransform>();
            SetupDropdown();
        }

        public void SetDirection(Direction _direction, Direction _cellDirection)
        {
            direction = _direction;
            cellDirection = _cellDirection;
            SetupDropdown();
        }

        public void SetCellSize(Vector2 _size)
        {
            cellSize = _size;
            SetOptions(dataList);
        }

        public void SetSpacing(float _spacing)
        {
            spacing = _spacing;
            SetupDropdown();
        }

        private void Update()
        {
            CloseWhenClickOut();
        }

        private IEnumerator SetupDirection(Transform _target, Direction _direction)
        {
            _target.TryGetComponent(out VerticalLayoutGroup vlg);
            _target.TryGetComponent(out HorizontalLayoutGroup hlg);
            switch (_direction)
            {
                case Direction.Vertical:
                    if (hlg)
                    {
                        Destroy(hlg);
                    }
                    yield return new WaitUntil(() => hlg == null);

                    if (_target)
                    {
                        if (!_target.TryGetComponent(out vlg))
                        {
                            vlg = _target.gameObject.AddComponent<VerticalLayoutGroup>();
                        }
                        vlg.spacing = spacing;
                        vlg.childForceExpandHeight = false;
                        vlg.childForceExpandWidth = false;
                        vlg.childAlignment = TextAnchor.MiddleCenter;
                    }
                    break;

                case Direction.Horizontal:
                    if (vlg)
                    {
                        Destroy(vlg);
                    }
                    //yield return new WaitUntil(() => vlg == null);
                    if (_target)
                    {
                        if (!_target.TryGetComponent(out hlg))
                        {
                            hlg = _target.gameObject.AddComponent<HorizontalLayoutGroup>();
                        }
                        hlg.spacing = spacing;
                        hlg.childForceExpandHeight = false;
                        hlg.childForceExpandWidth = false;
                        hlg.childAlignment = TextAnchor.MiddleCenter;
                    }

                    break;
            }
            yield return null;
        }

        private void CloseWhenClickOut()
        {
            if (UXManager.GetClickUp() && panel.gameObject.activeSelf && EventSystem.current.currentSelectedGameObject != mainButton.gameObject && !justClicked)
            {
                UX.CloseAnimation(panel.name);
            }
            if (justClicked)
            {
                justClicked = false;
            }
        }

        private void SetupButton(Button _button, int num)
        {
            buttonList.Add(_button);
            _button.gameObject.SetActive(true);
            _button.onClick.RemoveAllListeners();
            if (!_button.TryGetComponent(out LayoutElement le))
            {
                le = _button.gameObject.AddComponent<LayoutElement>();
            }
            le.minWidth = cellSize.x;
            le.minHeight = cellSize.y;
            RectTransform rt = _button.GetComponent<RectTransform>();
            rt.sizeDelta = cellSize;

            _button.transform.SetParent(panel, false);
            _button.onClick.AddListener(delegate { dataList[num].func(); index = num; });

            if (changeToSelected)
            {
                _button.onClick.AddListener(delegate { SetButtonToSelected(num); });
            }

            if (!string.IsNullOrEmpty(dataList[num].text))
            {
                Text i = _button.OnText(true);
                if (i != null)
                {
                    i.text = dataList[num].text;
                }
            }
            else
            {
                _button.OnText(false);
            }

            if (dataList[num].img != null)
            {
                RawImage ri = _button.OnRawImage(true);
                ri.texture = dataList[num].img;
                if (!ri.TryGetComponent(out LayoutElement imgLe))
                {
                    imgLe = ri.gameObject.AddComponent<LayoutElement>();
                }
                if (cellSize.x > cellSize.y)
                {
                    imgLe.minWidth = cellSize.y;
                    imgLe.minHeight = cellSize.y;
                }
                else
                {
                    imgLe.minWidth = cellSize.x;
                    imgLe.minHeight = cellSize.x;
                }
                SetupChildAlignment(_button.transform, TextAnchor.MiddleCenter);
            }
            else
            {
                _button.OnRawImage(false);
                SetupChildAlignment(_button.transform, TextAnchor.MiddleCenter);
            }
            if (_button.IsActive())
            {
                StartCoroutine(SetupDirection(_button.transform, cellDirection));
            }
        }

        private IEnumerator SetupChildAlignment(Transform _target, TextAnchor _textAnchor)
        {
            _target.TryGetComponent(out HorizontalOrVerticalLayoutGroup hvlg);
            yield return new WaitUntil(() => hvlg);  
            hvlg.childAlignment = _textAnchor;
        }

        public void SetOptions(List<Data> _data)
        {
            dataList = new List<Data>(_data);
            buttonList = new List<Button>();
            Clear();
            SetupPanel();
            panel.gameObject.SetActive(true);
            for (int i = 0; i < dataList.Count; i++)
            {
                SetupButton(Instantiate(buttonPrefab), i);
            }
            panel.gameObject.SetActive(false);
        }

        public void SetupPanel()
        {
            Vector2 pos;
            pos = panel.localPosition;
            if (dataList != null)
            {
                panel.sizeDelta = (direction == Direction.Vertical) ? new Vector2(cellSize.x, (cellSize.y + spacing) * dataList.Count) : new Vector2((cellSize.x + spacing) * dataList.Count, cellSize.y);
            }
            panel.localPosition = pos;
            panel.gameObject.SetActive(false);
        }

        public void SetButtonToSelected(int num)
        {
            index = num;
            Text t = mainButton.GetComponentInChildren<Text>();
            if (t != null)
            {
                t.text = buttonList[num].GetComponentInChildren<Text>().text;
            }

            RawImage ri = mainButton.GetComponentInChildren<RawImage>();
            if (ri != null)
            {
                ri.texture = buttonList[num].GetComponentInChildren<RawImage>().texture;
            }
        }

        public int GetIndex()
        {
            return index;
        }

        public class Data
        {
            public string text;
            public Texture img;
            public DelFunc func;

            public Data(string _text, DelFunc _func)
            {
                text = _text;
                func = _func;
            }
            public Data(string _text, Texture _img, DelFunc _func)
            {
                text = _text;
                img = _img;
                func = _func;
            }
        }

        private void Clear()
        {
            foreach (Transform t in panel)
            {
                Destroy(t.gameObject);
            }
        }

        private void ToggleChild()
        {
            if (panel.gameObject.activeSelf)
            {
                UX.CloseAnimation(panel.name);
            }
            else
            {
                UX.OpenAnimation(panel.name);
            }
        }

        public void SetTitle(string _title)
        {
            mainButton.ChangeText(_title);
        }

        public void Show()
        {
            panel.gameObject.SetActive(true);
            justClicked = true;
        }

        public void GotoMousePosition()
        {
            rt.position = UXManager.GetInputPosition();
        }
    }
    public delegate void DelFunc();
}