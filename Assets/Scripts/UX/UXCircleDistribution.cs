﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXCircleDistribution : MonoBehaviour
    {
        public float
            radius = 500,
            angleOffset = 0;
        public Vector2 radiusScale = Vector2.one;
        public bool 
            includesInactiveChild,
            counterClockWise = false,
            constantUpdate = false;
        private List<RectTransform> childList;      

        private void Start()
        {
            Distribute();
        }

        private void Update()
        {
            if(constantUpdate)
            {
                Distribute();
            }
        }

        private void OnEnable()
        {
            Distribute();
        }

        public void Distribute()
        {
            List<RectTransform> rtList = new List<RectTransform>();
            foreach(Transform t in transform)
            {
                if(!includesInactiveChild)
                {
                    if (t.gameObject.activeSelf)
                    {
                        rtList.Add(t as RectTransform);
                    }
                }
                else
                {
                    rtList.Add(t as RectTransform);
                }
            }
            int i = 0;
            if (!counterClockWise)
            {
                i = rtList.Count - 1;
            }
            
            foreach (RectTransform rt in rtList)
            {
                rt.anchoredPosition = CircleSpawnPos(i, rtList.Count, radius, angleOffset, radiusScale.x, radiusScale.y);
                if(counterClockWise)
                {
                    i++;
                }
                else
                {
                    i--;
                }
            }
        }

        public static Vector2 CircleSpawnPos(int i, int NumOfUnit, float _radius, float _angleOffset, float ratioX = 1, float ratioY = 1)
        {
            if (NumOfUnit > 1)
            {
                float __angle = (_angleOffset / 360 * Mathf.PI * 2) + (i * Mathf.PI * 2 / NumOfUnit);
                Vector2 __Pos = new Vector2(Mathf.Cos(__angle), Mathf.Sin(__angle)) * _radius;
                __Pos = new Vector2(__Pos.x * ratioX, __Pos.y * ratioY);
                return __Pos;
            }
            return Vector2.zero;
        }
    }
}