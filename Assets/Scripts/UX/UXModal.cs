﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Events;

namespace UX
{
    public class UXModal : MonoBehaviour
    {
        public static UXModal Instance { private set; get; }

        public Button
            leftBtn,
            rightBtn,
            loadingBtn;

        public Transform
            buttonPanel,
            blockPanel;

        public Image
            loadingImage;
        public Text
            title,
            text,
            blockMessage,
            loadingBtnLabel;

        public Canvas canvas;
        public List<RectTransform> textOffsetList;

        private const string
            MODAL = "UI_Modal/Modal_Panel",
            BLOCK = "UI_Modal/Block_Panel",
            BUTTON = "UI_Modal/LoadinBtn";

        public UXPopMessage popMessagePrefab;
        private UXPopMessage lastMessage;
        private Color popPanelColor;
        private Vector2 popPos;

        private void Awake()
        {
            Instance = this;
            popPos = new Vector2(Screen.width / 2, Screen.height / 2);
        }

        /// <summary>
        /// No button window
        /// </summary>
        /// <param name="_title"></param>
        /// <param name="description"></param>
        public static void SetModalWindow(string _title, string description)
        {
            SetModalWindowMain(_title, description, "Okay", null, null, null);
        }

        public static void SetModalWindow(string _title, string description, string buttonText)
        {
            SetModalWindowMain(_title, description, buttonText, null, null, null);
        }

        /// <summary>
        /// 1 button window
        /// </summary>
        /// <param name="_title"></param>
        /// <param name="description"></param>
        /// <param name="btnText"></param>
        /// <param name="btnAction"></param>
        public static void SetModalWindow(string _title, string description, string btnText, UnityAction btnAction)
        {
            SetModalWindowMain(_title, description, btnText, btnAction, null, null);
        }

        /// <summary>
        /// 2 button window
        /// </summary>
        /// <param name="_title"></param>
        /// <param name="description"></param>
        /// <param name="leftBtnText"></param>
        /// <param name="leftBtnAction"></param>
        /// <param name="rightBtnText"></param>
        /// <param name="rightBtnAction"></param>
        public static void SetModalWindow(string _title, string description, string leftBtnText, UnityAction leftBtnAction, string rightBtnText, UnityAction rightBtnAction)
        {
            SetModalWindowMain(_title, description, leftBtnText, leftBtnAction, rightBtnText, rightBtnAction);
        }

        public static void SetModalWindowMain(string _title, string description, string leftBtnText, UnityAction leftBtnAction, string rightBtnText, UnityAction rightBtnAction)
        {
            Instance.title.text = _title;
            if(!string.IsNullOrEmpty(description))
            {
                Instance.text.text = description;
                Instance.text.gameObject.SetActive(true);
            }
            else
            {
                Instance.text.gameObject.SetActive(false);
            }
            Instance.buttonPanel.gameObject.SetActive(false);
            if (!string.IsNullOrEmpty(leftBtnText))
            {
                Instance.buttonPanel.gameObject.SetActive(true);
                Instance.leftBtn.gameObject.SetActive(true);
                Instance.leftBtn.ChangeText(leftBtnText);
                Instance.leftBtn.onClick.RemoveAllListeners();
                if (leftBtnAction != null)
                {
                    Instance.leftBtn.onClick.AddListener(leftBtnAction);
                }
                Instance.leftBtn.onClick.AddListener(delegate { Activate(false); });
            }
            else
            {
                Instance.leftBtn.gameObject.SetActive(false);
            }
            if (!string.IsNullOrEmpty(rightBtnText))
            { 
                Instance.buttonPanel.gameObject.SetActive(true);
                Instance.rightBtn.gameObject.SetActive(true);
                Instance.rightBtn.ChangeText(rightBtnText);
                Instance.rightBtn.onClick.RemoveAllListeners();
                if (rightBtnAction != null)
                {
                    Instance.rightBtn.onClick.AddListener(rightBtnAction);
                    
                }
                Instance.rightBtn.onClick.AddListener(delegate { Activate(false); });
            }
            else
            {
                Instance.rightBtn.gameObject.SetActive(false);
            }
            Activate(true);
        }

        public static void Activate(bool active)
        {
            if (active)
            {
                LoadingPage(false);
                UX.OpenAnimation(MODAL);
            }
            else
            {
                UX.CloseAnimation(MODAL);
            }
        }

        public static void LoadingPage(bool activate, string message = "", bool image = false, Sprite sprite = null, UnityEngine.Events.UnityAction buttonAction = null, string buttonLabel = "CANCEL")
        {
            if (activate)
            {
                Instance.blockMessage.text = message;
                UX.OpenAnimation(BLOCK);
                if (!image)
                {
                    Instance.loadingImage.gameObject.SetActive(false);
                    Instance.blockMessage.rectTransform.position = Instance.textOffsetList[0].position;
                }
                else
                {
                    Instance.loadingImage.gameObject.SetActive(true);
                    if (sprite != null)
                    {
                        Instance.loadingImage.sprite = sprite;
                    }
                    Instance.blockMessage.rectTransform.position = Instance.textOffsetList[2].position;
                }
                if(buttonAction != null)
                {
                    Instance.loadingBtn.onClick.RemoveAllListeners();
                    Instance.loadingBtn.onClick.AddListener(buttonAction);
                    UX.OpenAnimation(BUTTON);
                    //Instance.loadingBtn.gameObject.SetActive(true);
                    Instance.loadingBtnLabel.text = buttonLabel;
                }
                else
                {
                    UX.CloseAnimation(BUTTON);
                    //Instance.loadingBtn.gameObject.SetActive(false);
                }
            }
            else
            {
                UX.CloseAnimation(BLOCK);
            }
        }

        public static UXPopMessage PopMessage(string message, Vector2 position, float duration = 2, int fontSize = 50)
        {
            if (Instance != null)
            {
                if (Instance.lastMessage != null)
                {
                    Instance.lastMessage.Interupt();
                }
            }
            else
            {
                Debug.LogError("Instance is null");
            }
            UXPopMessage temp = Instantiate(Instance.popMessagePrefab, Instance.transform, false);
            temp.PopMessage(message, position, duration, fontSize);
            temp.SetPanelColor(Instance.popPanelColor);
            Instance.lastMessage = temp;
            return temp;
        }

        public static void SetPopMessageColor(Color color)
        {
            Instance.popPanelColor = color;
        }

        public static void SetPopPos(Vector2 pos)
        {
            Instance.popPos = pos;
        }

        public static UXPopMessage PopMessage(string message, float duration = 2, int fontSize = 50)
        {
            return PopMessage(message, Instance.popPos, duration, fontSize);
        }

        public static void PopCancel()
        {
            if(Instance.lastMessage)
            {
                Instance.lastMessage.ForceClose();
            }
        }
    }
}