﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UX
{
    public class UIElement : MonoBehaviour
    {
        public RectTransform RectTransform { private set { } get { return (RectTransform)this.transform; } }
        public TextMeshProUGUI[] texts;
        public Image[] images;
        public Button[] buttons;
        public UXAnimator[] animators;
        public RectTransform[] rectTransforms;
    }
}