﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UX
{
    public class UXParent : MonoBehaviour {

        //public string scene;
        public List<UXComponent> uxComponentList;
        public List<UXTrigger> uxTriggerList;

        private void Awake()
        {
            GetChildTrigger();
            GetChildUX();
            //UXManager.UpdateUXParentList(this);
        }

        public void GetChildTrigger()
        {
            uxTriggerList = new List<UXTrigger>();
            UXTrigger[] arr = GetComponentsInChildren<UXTrigger>(true);
            for (int i = 0; i < arr.Length; i++)
            {
                if (CheckUXTriggerListID(arr[i].ID))
                {
                    Debug.LogError("UXTrigger of the same ID existed : " + arr[i].ID, arr[i]);
                    Debug.Log("Duplicated trigger : " + arr[i].ID, GetUXTrigger(arr[i].ID));
                }
                else
                {
                    uxTriggerList.Add(arr[i]);
                }
            }
        }

        public void GetChildUX()
        {
            uxComponentList = new List<UXComponent>();
            UXComponent[] arr = GetComponentsInChildren<UXComponent>(true);
            for (int i = 0; i < arr.Length; i++)
            {
                if(CheckUXComponentListID(arr[i].ID))
                {
                    Debug.LogError("UXComponent of the same ID existed : " + arr[i].ID, arr[i]);
                }
                else
                {
                    uxComponentList.Add(arr[i]);
                }
            }
        }

        public UXComponent GetUXComponent(string id)
        {
            for(int i = 0; i < uxComponentList.Count; i++)
            {
                if(uxComponentList[i].ID == id)
                {
                    return uxComponentList[i];
                }
            }
            Debug.Log("Could not find matching component : " + id);
            return null;
        }

        public UXTrigger GetUXTrigger(string id)
        {
            for (int i = 0; i < uxTriggerList.Count; i++)
            {
                if (uxTriggerList[i] != null)
                {
                    if (uxTriggerList[i].ID == id)
                    {
                        return uxTriggerList[i];
                    }
                }
                else
                {
                    Debug.LogError("Cannot find UXTrigger : " + id);
                }
            }
            return null;
        }

        private bool CheckUXComponentListID(string id)
        {
            for(int i = 0; i < uxComponentList.Count; i++)
            {
                if(uxComponentList[i].ID == id)
                {
                    return true;
                }
            }
            return false;
        }

        public bool CheckUXTriggerListID(string id)
        {
            if(uxTriggerList == null)
            {
                GetChildTrigger();
            }
            for (int i = 0; i < uxTriggerList.Count; i++)
            {
                if (uxTriggerList[i].ID == id)
                {
                    return true;
                }
            }
            return false;
        }
    }
}