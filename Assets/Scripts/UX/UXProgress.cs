﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXProgress : MonoBehaviour
    {
        [Range(0, 5)]
        public float animationDuration = 1;
        public string prefix = "";
        public AnimationCurve animationCurve = AnimationCurve.Linear(0, 0, 1, 1);
        public Text
            progressTxt;
        public UXBar
            bar;
        public bool
            showLimit = true,
            showSign = false,
            showComma = true,
            hideZero = false,
            autoDisable = true;
        public float
            maxVal;
        public UXAnimator maxAnimator;
        public float test;
        private Coroutine changesCo;
        public float val;
        private readonly string per = "/";
        public UnityEngine.Events.UnityAction onLevelUp, onMax;
        public bool isMaxLevel, isDone;
        private string overrideText;
        private bool forceTerminate;

        private void Start()
        {
            
        }

        private void Update()
        {
            //if (Input.GetKeyDown(KeyCode.K))
            //{
            //    //UpdateChanges(test);
            //    //SetCurrentProgress(0,4);
            //}
        }

        public void AnimateProgression(float addValue)
        {
            float oldVal = val;
            val = addValue;
            if (changesCo != null)
            {
                UXManager.StopCo(changesCo);
            }
            changesCo = UXManager.RunCoroutine(AnimateChanges(addValue, oldVal));
        }

        public void StopAnimation()
        {
            if(changesCo != null)
            {
                UXManager.StopCo(changesCo);
            }
            forceTerminate = true;
        }

        private IEnumerator AnimateChanges(float newVal, float oldVal)
        {
            forceTerminate = false;
            isDone = false;
            float timer = 0;
            float animVal;
            while (timer < 1)
            {
                if(forceTerminate)
                {
                    yield break;
                }
                timer += Time.deltaTime / animationDuration;
                animVal = animationCurve.Evaluate(timer);
                val = newVal * animVal + oldVal;
                int n = Mathf.RoundToInt(val);
                if (bar != null)
                {
                    bar.SetBar(0, val, maxVal);
                    if (newVal > 0)
                    {
                        bar.barRect.gameObject.SetActive(true);
                    }
                }
                if (progressTxt != null)
                {                    
                    string text = "";
                    if (showLimit)
                    {
                        if (showComma)
                        {
                            text = n.AddComma() + per + maxVal.ToString().AddComma();
                        }
                        else
                        {
                            text = n + per + maxVal;
                        }
                    }
                    else
                    {
                        if (showComma)
                        {
                            text = n.AddComma();
                        }
                        else
                        {
                            text = n.ToString();
                        }
                    }
                    progressTxt.text = prefix + ((showSign) ? GetOperator(n) : "") + text;
                    if (hideZero && n == 0)
                    {
                        progressTxt.text = "";
                    }
                }
                if (val >= maxVal)
                {
                    if(onMax != null)
                    {
                        onMax();
                    }
                    if (onLevelUp != null)
                    {
                        newVal -= (maxVal - oldVal);
                        oldVal = 0;
                        timer = 0;
                        onLevelUp();
                        if (maxVal < 1)
                        {
                            break;
                        }
                    }
                }
                yield return null;
            }
            isDone = true;
        }

        public void SetCurrentProgress(float current, float max)
        {
            UXManager.RunCoroutine(SetCurrentProgressCo(current, max)); //wait for 2 frames
        }

        public void SetCurrentProgress(int current)
        {
            UXManager.RunCoroutine(SetCurrentProgressCo(current, current)); //wait for 2 frames
        }

        private IEnumerator SetCurrentProgressCo(float current, float max)
        {
            string text = "";            
            val = current;
            maxVal = max;
            int n = Mathf.RoundToInt(val);
            if (progressTxt != null)
            {
                if (showLimit)
                {
                    if(showComma)
                    {
                        text = n.AddComma() + (per + maxVal.ToString().AddComma());
                    }
                    else
                    {
                        text = n + per + maxVal;
                    }
                }
                else
                {
                    if(showComma)
                    {
                        text = n.ToString().AddComma();
                        
                    }
                    else
                    {
                        text = n.ToString();
                    }
                }
                progressTxt.text = prefix + ((showSign) ? GetOperator(Mathf.RoundToInt(val)) : "") + text;
                if (hideZero && n == 0)
                {
                    progressTxt.text = "";
                }
            }
            if (bar != null)
            {
                bar.SetBar(0, val, maxVal);
                if(autoDisable)
                {
                    if (val <= 0)
                    {
                        bar.barRect.gameObject.SetActive(false);
                    }
                    else
                    {
                        bar.barRect.gameObject.SetActive(true);
                    }
                }
            }
            //wait for ui layout paint;
            yield return null;
            yield return null;
            if (progressTxt != null)
            {
                progressTxt.text = prefix + ((showSign) ? GetOperator(Mathf.RoundToInt(val)) : "") + text;
            }
            if (hideZero && n == 0)
            {
                progressTxt.text = "";
            }
            if (bar != null)
            {
                bar.SetBar(0, val, maxVal);
            }
            if(val >= maxVal)
            {
                if(onMax != null)
                {
                    onMax();
                }
            }
            if (!string.IsNullOrEmpty(overrideText) && progressTxt != null)
            {
                progressTxt.text = overrideText;
                //overrideText = null;
            }
        }

        /// <summary>
        /// Put null or empty to turn off override
        /// </summary>
        /// <param name="text"></param>
        public void OverrideText(string text)
        {
            overrideText = text;
        }

        public void SetProgressionRequirement(float newMax)
        {
            maxVal = newMax;
        }

        public void SetMax(string maxText = "Max", bool isMax = true)
        {
            isMaxLevel = isMax;
            if (isMax)
            {
                if (progressTxt != null)
                {
                    progressTxt.text = maxText;
                    bar.SetBar(0, 1, 1);
                    
                }
            }
            if (onMax != null && isMax)
            {
                onMax();
            }
        }

        private string GetOperator(int count)
        {
            if (count > 0)
            {
                return " +";
            }
            else
            {
                return " ";
            }
        }
    }
}