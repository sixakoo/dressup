﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawArea : MonoBehaviour {

    public static DrawArea Instance { private set; get; }
    public static RectTransform rectTransform;

    private void Awake()
    {
        Instance = this;
        rectTransform = transform as RectTransform;
    }
}
