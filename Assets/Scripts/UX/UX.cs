﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif
using UnityEngine.SceneManagement;

namespace UX
{
    public class UX
    {
        public const string tag = "#";//the escape character for the gameObject name; characters after this tag will not be process
        public const string scenePath = "Assets/Scenes";
        public const string sceneExtension = ".unity";
        public enum AnimationType
        {
            Open,
            Close,
            Toggle,
            Repeat
        }

        [System.Serializable]
        public class UXStyle
        {
            public string name;
            public UX.AnimConfig configuration = UX.AnimConfig.Default;

            public UXStyle()
            {
                name = "Empty";
                configuration = UX.AnimConfig.Default;
            }
        }

        [System.Serializable]
        public class AnimConfig
        {
            [Range(0, 5), Tooltip("duration of the animation")]
            public float duration = 1;
            [Tooltip("how much shrinking will be applied; proportional to the object (0 - 1)")]
            public Vector2 scale = Vector2.one * -0.2f;
            [Tooltip("if the value is less than 1, object moves proportionally to the screen space; if the value is more than 1, object moves by pixel")]
            public Vector2 move = Vector2.zero;
            [Tooltip("how much rotation to be applied to a specific axis")]
            public Vector3 rotate = Vector3.zero;
            [Tooltip("override the pivot of the RectTransform. Put the values of '-1, -1' to cancel the override & use the value in the inspector")]
            public Vector2 pivot = Vector2.one * -1f;
            [Tooltip("determines the alpha value when the object is deactivated")]
            public float alpha = 0.0f;
            [Tooltip("play animation in one way or both ways")]
            public bool oneWay = false;
            [Tooltip("set to true for the gameobject to never be disabled")]
            public bool alwaysOn = false;
            [Tooltip("mirror the animation curve for closing animation, turning the animation to have ping pong like effect")]
            public bool mirror = false;
            [Tooltip("determines the state of the UI at the start of the game")]
            public bool beginActive = false;
            [Tooltip("repeat the animation again when called")]
            public bool persistent = false;
            [HideInInspector]
            public bool isAnimating { set; get; }
            [Tooltip("set the animation curve. 0 must be set only at the beginning & 1 only at the end")]
            public AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1);

            public static AnimConfig Default
            {
                get
                {
                    return new UX.AnimConfig(5f, Vector2.one * 0.2f, new Vector2(0, 0.2f), Vector3.zero, Vector2.one * -1f, 0.0f, false, false, false, false, false, AnimationCurve.Linear(0, 0, 1, 1));
                }
            }

            public AnimConfig()
            {
                duration = 1f;
                scale = Vector2.one * 0.2f;
                move = Vector2.zero;
                pivot = Vector2.one * -1;
                alpha = 0f;
                oneWay = false;
                alwaysOn = false;
                mirror = false;
                beginActive = false;
                persistent = false;
                curve = AnimationCurve.EaseInOut(0, 0, 1, 1);
            }

            /// <summary>
            /// Configure the panel animation sequence
            /// </summary>
            /// <param name="_duration">duration of the animation</param>
            /// <param name="_shrink">how much shrinking will be applied; proportional to the object (0 - 1)</param>
            /// <param name="_move">if the value is less than 1, object moves proportionally to the screen space; if the value is more than 1, object moves by pixel</param>
            /// <param name="_rotate">how much rotation to be applied to a specific axis</param>
            /// <param name="_pivot">override the pivot of the RectTransform. Put the values of '-1, -1' to cancel the override & use the value in the inspector</param>
            /// <param name="_alphaStart">determines the alpha value when the object is deactivated</param>
            /// <param name="_oneWay">play animation in one way or both ways</param>
            /// <param name="_alwaysOn">set to true for the gameobject to never be disabled</param>
            /// <param name="_mirror">mirror the animation curve for closing animation, turning the animation to have ping pong like effect</param>
            /// /// <param name="_beginActive">mirror the animation curve for closing animation, turning the animation to have ping pong like effect</param>
            /// <param name="_curve">set the animation curve. 0 must be set only at the beginning & 1 only at the end</param>
            public AnimConfig(float _duration, Vector2 _shrink, Vector2 _move, Vector3 _rotate, Vector2 _pivot, float _alphaStart, bool _oneWay, bool _alwaysOn, bool _mirror, bool _beginActive, bool _persistent, AnimationCurve _curve)
            {
                duration = _duration;
                scale = _shrink;
                move = _move;
                rotate = _rotate;
                pivot = _pivot;
                alpha = _alphaStart;
                oneWay = _oneWay;
                alwaysOn = _alwaysOn;
                mirror = _mirror;
                beginActive = _beginActive;
                persistent = _persistent;
                curve = _curve;
            }
            public AnimConfig(AnimConfig config)
            {
                duration = config.duration;
                scale = config.scale;
                move = config.move;
                pivot = config.pivot;
                alpha = config.alpha;
                oneWay = config.oneWay;
                isAnimating = config.isAnimating;
                alwaysOn = config.alwaysOn;
                mirror = config.mirror;
                beginActive = config.beginActive;
                curve = config.curve;
            }
        }

        public class UXObject
        {
            public string name;
            public float timer;
            public float animValue;
            public float curveVal;
            public CanvasGroup cg;
            public RectTransform rt;
            public bool active;
            public bool lastState;
            public AnimConfig animConfig;
            public Vector2 initialPos;
            public Vector3 initialRot;
            public bool isAnimating, isDone;
            public bool cancel;

            public UXObject()
            {
                name = "";
                cg = null;
                rt = null;
                animValue = 0.0f;
                active = false;
                animConfig = new AnimConfig();
                initialPos = Vector2.zero;
                isAnimating = false;
                cancel = false;
            }

            public UXObject(RectTransform _rt, AnimConfig _animConfig)
            {
                animConfig = _animConfig;
                name = _rt.gameObject.name;
                if (!_rt.TryGetComponent(out cg))
                {
                    cg = _rt.gameObject.AddComponent<CanvasGroup>();
                }
                rt = cg.GetComponent<RectTransform>();
                animValue = 0;
                active = false;
                initialPos = rt.anchoredPosition;
                isAnimating = false;
                cancel = false;
                SetState(_animConfig.beginActive);
            }

            //To transfer the state of old object to new
            public UXObject(RectTransform _rt, AnimConfig _animConfig, UXObject _oldInstance)
            {
                name = _rt.gameObject.name;
                if (!_rt.TryGetComponent(out cg))
                {
                    cg = _rt.gameObject.AddComponent<CanvasGroup>();
                }
                rt = cg.GetComponent<RectTransform>();
                if (_oldInstance != null)
                {
                    animValue = _oldInstance.animValue;
                    active = _oldInstance.active;
                    isAnimating = _oldInstance.isAnimating;
                    cancel = _oldInstance.cancel;
                }
                else
                {
                    animValue = 0;
                    active = false;
                    isAnimating = false;
                    cancel = false;
                }
                animConfig = _animConfig;
                initialPos = rt.anchoredPosition;
                SetState(_animConfig.beginActive);
            }

            public void SetState(bool on)
            {
                if (on)
                {
                    active = true;
                    animValue = 1;
                    cg.alpha = 1;
                    curveVal = 1;
                    timer = 1;
                    curveVal = 1;
                    lastState = true;
                }
                else
                {
                    active = false;
                    animValue = 0;
                    cg.alpha = 0;
                    curveVal = 0;
                    timer = 0;
                    curveVal = 0;
                    lastState = false;
                }
                UpdateState();
            }

            public void UpdateState()
            {
                rt.localScale = ConvertShrink(animValue, animConfig.scale);
                rt.anchoredPosition = ConvertMove(animValue, animConfig.move, initialPos);
                rt.eulerAngles = ConvertRotate(animValue, animConfig.rotate, initialRot);
                if (animConfig.pivot != Vector2.one * -1)
                {
                    rt.pivot = animConfig.pivot;
                }
                cg.alpha = ConvertStartFade(animValue, animConfig.alpha);
                cg.gameObject.SetActive(active);
            }

            public void Dump()
            {
                Debug.Log
                    (
                        "name :" + name + "\r\n" +
                        "cg :" + cg + "\r\n" +
                        "rt :" + rt + "\r\n" +
                        "animValue :" + animValue + "\r\n" +
                        "active :" + active + "\r\n" +
                        "timer :" + timer + "\r\n" +
                        "initialPos :" + initialPos + "\r\n" +
                        "isAnimating :" + isAnimating + "\r\n" +
                        "cancel :" + cancel + "\r\n"
                        );
            }
        }
        public static List<UXObject> animationList = new List<UXObject>();
        private static List<UXObject> removeList = new List<UXObject>();
        public static List<Component> externalAnimList = new List<Component>();
        private static Dictionary<string, UXTrigger> uxTriggerDict;
        private static Dictionary<string, UXComponent> uxComponentDict;

        /// <summary>
        /// Put this in Update()
        /// </summary>  
        public static void AnimationListControl()
        {
            for (int i = 0; i < animationList.Count; i++)
            {
                FadeAnimation(animationList[i]);
            }

            for (int i = 0; i < removeList.Count; i++)
            {
                removeList[i].isDone = true;
                animationList.Remove(removeList[i]);
            }
            removeList.Clear();
        }

        public static void DebugUXComponentDict()
        {
            foreach (var kvp in uxComponentDict)
            {
                Debug.Log("Key : " + kvp.Key + " | Value : " + kvp.Value.ID, kvp.Value);
            }
        }

        public static void AddTriggerListener(string trigger, UnityEngine.Events.UnityAction action)
        {
            if (UX.uxTriggerDict.ContainsKey(trigger))
            {
                UX.uxTriggerDict[trigger].button.onClick.AddListener(action);
            }
            else
            {
                Debug.LogWarning("Trigger not found : " + trigger + " | " + trigger);
            }
        }

        public static void UXDictAdd(UXComponent uxComponent)
        {
            Debug.Log(uxComponent.gameObject, uxComponent);
            if (uxComponentDict == null)
            {
                uxComponentDict = new Dictionary<string, UXComponent>();
            }
            if (!uxComponentDict.ContainsKey(uxComponent.ID))
            {
                Debug.Log("Added : " + uxComponent.ID, uxComponent);
                uxComponentDict.Add(uxComponent.ID, uxComponent);
            }
        }

        public static void FadeAnimation(UX.UXObject panel)
        {
            if(panel.cg == null)
            {
                removeList.Add(panel);
                return;
            }
            if (panel.active)
            {
                panel.timer += Time.deltaTime / panel.animConfig.duration;
            }
            else
            {
                panel.timer -= Time.deltaTime / panel.animConfig.duration;
            }

            if (panel.active) //activate
            {
                if (!panel.cg.gameObject.activeSelf)
                {
                    panel.cg.gameObject.SetActive(true);
                    panel.isAnimating = true;
                }
                if (panel.timer >= 1)
                {
                    panel.curveVal = 0;
                    removeList.Add(panel);
                    panel.isAnimating = false;
                    panel.cancel = false;
                    panel.isDone = true;
                    ForceActivate(panel);
                }
                else
                {
                    if (panel.lastState != panel.active)
                    {
                        if (panel.curveVal > 0)
                        {
                            if (panel.animConfig.mirror)
                            {
                                panel.curveVal = 1 - panel.curveVal;
                            }
                            else
                            {
                                panel.curveVal = 1 - panel.animConfig.curve.Evaluate(panel.curveVal);
                            }
                            if (panel.curveVal < 0)
                            {
                                panel.curveVal = 0;
                            }
                            panel.animValue = panel.curveVal;
                            if (panel.cancel)
                            {
                                panel.cancel = false;
                            }
                            else
                            {
                                panel.cancel = true;
                            }
                        }
                    }
                    panel.cg.alpha = ConvertStartFade(panel.animValue, panel.animConfig.alpha);
                    panel.rt.localScale = ConvertShrink(panel.animValue, panel.animConfig.scale);
                    if (panel.animConfig.oneWay && panel.cancel)
                    {
                        panel.rt.anchoredPosition = ConvertMoveOpposite(panel.animValue, panel.animConfig.move, panel.initialPos);
                        panel.rt.eulerAngles = ConvertRotateOpoosite(panel.animValue, panel.animConfig.rotate, panel.initialRot);
                    }
                    else
                    {
                        panel.rt.anchoredPosition = ConvertMove(panel.animValue, panel.animConfig.move, panel.initialPos);
                        panel.rt.eulerAngles = ConvertRotate(panel.animValue, panel.animConfig.rotate, panel.initialRot);
                    }

                    if (panel.animConfig.pivot != Vector2.one * -1)
                    {
                        panel.rt.pivot = panel.animConfig.pivot;
                    }
                    panel.curveVal += Time.deltaTime / panel.animConfig.duration;
                    panel.animValue = panel.animConfig.curve.Evaluate(panel.curveVal);
                }
            }
            else //deactivate
            {
                if (panel.timer <= 0)
                {
                    panel.curveVal = 0;
                    if (!panel.animConfig.alwaysOn)
                    {
                        panel.cg.gameObject.SetActive(false);
                    }
                    removeList.Add(panel);
                    panel.isAnimating = false;
                    panel.cancel = false;
                    panel.isDone = true;
                    ForceDeactivate(panel);
                }
                else
                {
                    if (panel.lastState != panel.active)
                    {
                        panel.isAnimating = true;
                        if (panel.curveVal > 0)
                        {
                            if (panel.animConfig.mirror)
                            {
                                panel.curveVal = 1 - panel.curveVal;
                            }
                            else
                            {
                                panel.curveVal = 1 - panel.animConfig.curve.Evaluate(panel.curveVal);
                            }
                            if (panel.curveVal < 0)
                            {
                                panel.curveVal = 0;
                            }
                            panel.animValue = panel.curveVal;
                            if (panel.cancel)
                            {
                                panel.cancel = false;
                            }
                            else
                            {
                                panel.cancel = true;
                            }
                        }
                    }
                    panel.cg.alpha = ConvertStartFade(panel.animValue, panel.animConfig.alpha);
                    panel.rt.localScale = ConvertShrink(panel.animValue, panel.animConfig.scale);
                    if (panel.animConfig.pivot != Vector2.one * -1)
                    {
                        panel.rt.pivot = panel.animConfig.pivot;
                    }

                    if (panel.animConfig.oneWay && !panel.cancel)
                    {
                        panel.rt.anchoredPosition = ConvertMoveOpposite(panel.animValue, panel.animConfig.move, panel.initialPos);
                        panel.rt.eulerAngles = ConvertRotateOpoosite(panel.animValue, panel.animConfig.rotate, panel.initialRot);
                    }
                    else
                    {
                        panel.rt.anchoredPosition = ConvertMove(panel.animValue, panel.animConfig.move, panel.initialPos);
                        panel.rt.eulerAngles = ConvertRotate(panel.animValue, panel.animConfig.rotate, panel.initialRot);
                    }
                    panel.curveVal += Time.deltaTime / panel.animConfig.duration;
                    if (panel.animConfig.mirror)
                    {
                        panel.animValue = panel.animConfig.curve.Evaluate(1 - panel.curveVal);
                    }
                    else
                    {
                        panel.animValue = 1 - panel.animConfig.curve.Evaluate(panel.curveVal);
                    }
                }
            }
            panel.lastState = panel.active;
        }

        private static bool IsAnimating(UX.UXObject panel)
        {
            foreach (UX.UXObject i in animationList)
            {
                if (i == panel)
                {
                    return true;
                }
            }
            return false;
        }

        private static void ForceActivate(UXObject uxObj)
        {
            uxObj.cg.alpha = 1;
            uxObj.rt.localScale = Vector3.one;
            uxObj.rt.anchoredPosition = uxObj.initialPos;
            uxObj.rt.eulerAngles = uxObj.initialRot;

            if (uxObj.animConfig.pivot != Vector2.one * -1)
            {
                uxObj.rt.pivot = uxObj.animConfig.pivot;
            }
        }

        private static void ForceDeactivate(UXObject uxObj)
        {
            uxObj.cg.alpha = 1 - uxObj.cg.alpha;
            uxObj.rt.localScale = Vector2.one - uxObj.animConfig.scale;
            Vector2 move = uxObj.animConfig.move;
            if (move.x > 1 || move.x < -1 || move.y > 1 || move.y < -1)
            {
                uxObj.rt.anchoredPosition = uxObj.initialPos + uxObj.animConfig.move;
            }
            else
            {
                uxObj.rt.anchoredPosition = uxObj.initialPos + (uxObj.initialPos - uxObj.animConfig.move);
            }
            uxObj.rt.eulerAngles = uxObj.initialRot;

            if (uxObj.animConfig.pivot != Vector2.one * -1)
            {
                uxObj.rt.pivot = uxObj.animConfig.pivot;
            }
        }

        public static void OpenAnimation(string id)
        {
            UXManager.RunCoroutine(Animate(id, AnimationType.Open));
        }

        public static void RepeatAnimation(string id)
        {
            UXManager.RunCoroutine(Animate(id, AnimationType.Repeat));
        }

        public static void CloseAnimation(string id)
        {
            UXManager.RunCoroutine(Animate(id, AnimationType.Close));
        }

        public static void ToggleAnimation(string id)
        {
            UXManager.RunCoroutine(Animate(id, AnimationType.Toggle));
        }

        public static IEnumerator Animate(string id, AnimationType animType)
        {
            string sceneName = id.Split('/')[0];
            AsyncOperation ao = null;
            Scene scene = SceneManager.GetSceneByName(sceneName);
            if (string.IsNullOrEmpty(scene.name) && animType != AnimationType.Close) // Check if Loaded
            {
                ao = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
                if (ao != null) //wait for sceneload
                {
                    yield return new WaitUntil(() => ao.isDone);
                    scene = SceneManager.GetSceneByName(sceneName);
                }
            }

            if (string.IsNullOrEmpty(scene.name)) //Check if not exist
            {
                if(animType != AnimationType.Close)
                {
                    Debug.LogError("Scene doest not exist : " + sceneName);
                }
            }
            else // animate Component
            {   
                yield return new WaitUntil(() => scene.isLoaded);
                if(!scene.IsValid())
                {
                    yield break;
                }
                GameObject[] goArr = scene.GetRootGameObjects();
                UXComponent uxc = null;

                for (int i = 0; i < goArr.Length; i++)
                {
                    if (goArr[i].TryGetComponent(out UXParent uxp))
                    {
                        uxc = uxp.GetUXComponent(id);
                        if (uxc != null)
                        {
                            break;
                        }
                    }
                }
                if (uxc != null)
                {
                    if (uxc.coroutine != null)
                    {
                        UXManager.Instance.StopCoroutine(uxc.coroutine);
                    }
                    uxc.coroutine = UXManager.RunCoroutine(AddToAnimationList(uxc, animType));
                }
                else
                {
                    Debug.LogError("UXComponent ID not recognised : " + id);
                }
                yield return null;
            }
        }

        public static UXComponent GetUXComponent(string id)
        {
            string sceneName = id.Split('/')[0];
            Scene scene = SceneManager.GetSceneByName(sceneName);
            if(!scene.IsValid())
            {
                return null;
            }
            else
            {
                GameObject[] goArr = scene.GetRootGameObjects();
                UXComponent uxc = null;

                for (int i = 0; i < goArr.Length; i++)
                {
                    if (goArr[i].TryGetComponent(out UXParent uxp))
                    {
                        uxc = uxp.GetUXComponent(id);
                        if (uxc != null)
                        {
                            break;
                        }
                    }
                }
                if (uxc != null)
                {
                    return uxc;
                }
                else
                {
                    Debug.LogError("UXComponent ID not recognised : " + id);
                    return null;
                }
            }
        }

        public static IEnumerator AddToAnimationList(UXComponent uxComponent, AnimationType animType)
        {
            if (uxComponent.uxObject == null)
            {
                uxComponent.Setup();
                uxComponent.uxObject.isDone = false;
            }
            switch(animType)
            {
                case AnimationType.Open:
                    uxComponent.uxObject.active = true;
                    if (!IsAnimating(uxComponent.uxObject))
                    {
                        uxComponent.uxObject.isDone = false;
                        if(uxComponent.uxObject.animConfig.persistent)
                        {
                            uxComponent.uxObject.timer = 0;
                        }
                        UX.animationList.Add(uxComponent.uxObject);
                    }
                    break;
                case AnimationType.Close:
                    uxComponent.uxObject.active = false;
                    if (!IsAnimating(uxComponent.uxObject))
                    {
                        uxComponent.uxObject.isDone = false;
                        if (uxComponent.uxObject.animConfig.persistent)
                        {
                            uxComponent.uxObject.timer = 1;
                        }
                        UX.animationList.Add(uxComponent.uxObject);
                    }
                    if (uxComponent.removeScene)
                    {
                        if (uxComponent.gameObject.activeSelf)
                        {
                            yield return new WaitUntil(() => uxComponent.uxObject.isDone); //wait for animation to end
                        }
                        if (uxComponent != null)
                        {
                            SceneManager.UnloadSceneAsync(uxComponent.gameObject.scene);
                        }
                    }
                    break;
                case AnimationType.Toggle: //Deprecated
                    uxComponent.uxObject.active = !uxComponent.uxObject.active;
                    if (!IsAnimating(uxComponent.uxObject))
                    {
                        uxComponent.uxObject.isDone = false;
                        UX.animationList.Add(uxComponent.uxObject);
                    }
                    if (uxComponent.removeScene && !uxComponent.uxObject.active)
                    {
                        Debug.Log(uxComponent.uxObject.isAnimating, uxComponent);
                        if (!uxComponent.uxObject.isAnimating)
                        {
                            yield return new WaitUntil(() => uxComponent.uxObject.isAnimating); //wait for animation to start
                        }
                        yield return new WaitUntil(() => !uxComponent.uxObject.isAnimating); //wait for animation to end
                        Debug.Log("Removing scene");

                        SceneManager.UnloadSceneAsync(uxComponent.gameObject.scene);
                    }
                    break;
                case AnimationType.Repeat: //Deprecated
                    uxComponent.Off();
                    uxComponent.uxObject.active = true;
                    if (!IsAnimating(uxComponent.uxObject))
                    {
                        uxComponent.uxObject.isDone = false;
                        UX.animationList.Add(uxComponent.uxObject);
                    }
                    else
                    {
                        //UX.animationList.Remove(uxComponent.uxObject);
                        //yield return null;
                        //UX.animationList.Add(uxComponent.uxObject);
                        //uxComponent.uxObject.Dump();
                    }
                    break;
                default:
                    Debug.Log("Why???");
                    break;
            }
            yield return null;
        }

        public static void CloseAnimation(UX.UXObject _panel)
        {
            _panel.active = false;
            if (!IsAnimating(_panel))
            {
                _panel.isDone = false;
                UX.animationList.Add(_panel);
            }
        }

        public static IEnumerator AnimateRectTransform(RectTransform rectTransform, float duration, AnimationCurve ac, Vector2 move, Vector2 scale, Vector3 rotate)
        {
            float val = 0;
            float animVal = 0;
            Vector2 initialPos = rectTransform.anchoredPosition;
            Vector3 initialRot = rectTransform.eulerAngles;
            while (val < 1)
            {
                val += Time.deltaTime / duration;
                animVal = ac.Evaluate(val);
                rectTransform.anchoredPosition = ConvertMove(animVal, move, initialPos);
                rectTransform.localScale = ConvertShrink(1 + animVal, scale);
                rectTransform.eulerAngles = ConvertRotate(animVal, rotate, initialRot);
                yield return null;
            }
        }

        public static void SwitchAnimation(string _panelName, MonoBehaviour _instance)
        {
            _instance.StartCoroutine(SwitchAnimationFunc(_panelName, null));
        }

        //might need to update later
        public static void SwitchAndUpadateAnimation(string _panelName, Dictionary<string, UXObject> _panelDict, MonoBehaviour _instance, UnityEngine.Events.UnityAction _action, MonoBehaviour coroutineInstance)
        {
            coroutineInstance.StartCoroutine(SwitchAnimationFunc(_panelName, _action));
        }
        //might need to update later
        private static IEnumerator SwitchAnimationFunc(string _panelName, UnityEngine.Events.UnityAction _action)
        {

            if (uxComponentDict.ContainsKey(_panelName))
            {
                UX.UXObject custom = uxComponentDict[_panelName].uxObject;
                custom.active = false;
                if (!IsAnimating(custom))
                {
                    UX.animationList.Add(custom);
                    custom.isAnimating = true;
                }
                yield return new WaitUntil(() => custom.isAnimating == false);
                if (_action != null)
                {
                    _action();
                }
                custom.animValue = 0;
                custom.active = true;
                if (!IsAnimating(custom))
                {
                    custom.isDone = false;
                    UX.animationList.Add(custom);
                }
            }
            else
            {
                Debug.LogError("key not found : " + _panelName);
            }
        }

        /// <summary>
        /// This will only toggle one UXObject and closes all of the other siblings
        /// </summary>
        public static void RadioToggle(string _panelName)
        {
            UX.UXObject sub = uxComponentDict[_panelName].uxObject;
            sub.active = true;

            UX.UXObject currentSubWindow = null;
            foreach (Transform t in sub.cg.transform.parent)
            {
                if (t.gameObject.activeSelf && t != sub.cg.transform)// Close other SubWindows
                {
                    foreach (KeyValuePair<string, UXComponent> kvp in uxComponentDict)
                    {
                        if (kvp.Key == t.name)
                        {
                            currentSubWindow = kvp.Value.uxObject;
                            currentSubWindow.active = false;
                            break;
                        }
                    }
                }
            }
            if (!IsAnimating(sub))
            {
                if (currentSubWindow != null)
                {
                    currentSubWindow.isDone = false;
                    UX.animationList.Add(currentSubWindow);

                }
                sub.isDone = false;
                UX.animationList.Add(sub);
            }
        }

        public static Vector3 ConvertShrink(float x, Vector3 shrink)
        {
            return (x * shrink) + (new Vector3(1 - shrink.x, 1 - shrink.y, 1));
        }

        public static float ConvertStartFade(float x, float startFade)
        {
            return (startFade + (x * (1 - startFade)));
        }

        public static Vector2 ConvertMove(float x, Vector2 move, Vector2 initialPos)
        {
            if (move.x > 1 || move.x < -1 || move.y > 1 || move.y < -1)
            {
                return new Vector2(move.x * (1f - x) + initialPos.x, move.y * (1f - x) + initialPos.y);
            }
            else
            {
                return new Vector2((Screen.width * move.x) * (1f - x) + initialPos.x, (Screen.height * move.y) * (1f - x) + initialPos.y);
            }
        }

        public static Vector2 ConvertMoveOpposite(float x, Vector2 move, Vector2 initialPos)
        {
            if (move.x > 1 || move.x < -1 || move.y > 1 || move.y < -1)
            {
                return new Vector2((move.x * -1f) * (1f - x) + initialPos.x, (move.y * -1f) * (1f - x) + initialPos.y);
            }
            else
            {
                return new Vector2((Screen.width * move.x * -1f) * (1f - x) + initialPos.x, (Screen.height * move.y * -1f) * (1f - x) + initialPos.y);
            }
        }

        public static Vector3 ConvertRotate(float x, Vector3 rotate, Vector3 initialRot)
        {
            return new Vector3((initialRot.x - rotate.x) + (rotate.x * (x)), (initialRot.y - rotate.y) + (rotate.y * (x)), (initialRot.z - rotate.z) + (rotate.z * (x)));
        }

        public  static Vector3 ConvertRotateOpoosite(float x, Vector3 rotate, Vector3 initialRot)
        {
            return new Vector3((initialRot.x ) + (rotate.x * (1 - x)), (initialRot.y) + (rotate.y * (1 - x)), (initialRot.z) + (rotate.z * (1 - x)));
        }

        public static string ColorToHex(Color32 color)
        {// Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.		
            string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
            return hex;
        }

        public static Color HexToColor(string hex)
        {
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            byte a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
            return new Color32(r, g, b, a);
        }

        public static void ShowUXComponentReference(string id)
        {
    #if UNITY_EDITOR
            string sceneName = id.Split('/')[0];
            Scene scene = SceneManager.GetSceneByName(sceneName);
            if (string.IsNullOrEmpty(scene.name)) // Check if Loaded
            {
                Debug.Log("Loading scene : " + sceneName);
                EditorSceneManager.OpenScene(scenePath + sceneName + sceneExtension, OpenSceneMode.Additive);
                scene = SceneManager.GetSceneByName(sceneName);
                Debug.Log("Scene loaded : " + sceneName);
            }
            if (string.IsNullOrEmpty(scene.name)) //Check if not exist
            {
                Debug.LogError("Scene doest not exist");
            }
            else
            {
                GameObject[] goArr = scene.GetRootGameObjects();
                UXComponent uxc = null;
                for(int i = 0; i < goArr.Length; i++)
                {
                    if(goArr[i].TryGetComponent(out UXParent uxp))
                    {
                        uxc = uxp.GetUXComponent(id);
                    }
                }
                if(uxc != null)
                {
                    Debug.Log(uxc.ID, uxc);
                }
                else
                {
                    Debug.Log("Cannot find UXComponent reference");
                }
            }
#endif
        }

        public static void ShowUXTriggerReference(string id)
        {
            #if UNITY_EDITOR
            string sceneName = id.Split('/')[0];
            //AsyncOperation ao = null;
            Scene scene = EditorSceneManager.GetSceneByName(sceneName);
            if (string.IsNullOrEmpty(scene.name)) // Check if Loaded
            {
                Debug.Log("Loading scene : " + sceneName);
                EditorSceneManager.OpenScene(scenePath + sceneName + sceneExtension, OpenSceneMode.Additive);
                scene = EditorSceneManager.GetSceneByName(sceneName);
                Debug.Log("Scene loaded : " + sceneName);
            }
            if (string.IsNullOrEmpty(scene.name)) //Check if not exist
            {
                Debug.LogError("Scene doest not exist");
            }
            else
            {
                GameObject[] goArr = scene.GetRootGameObjects();
                UXTrigger uxt = null;
                for (int i = 0; i < goArr.Length; i++)
                {
                    if(goArr[i].TryGetComponent(out UXParent uxp))
                    {
                        uxt = uxp.GetUXTrigger(id);
                    }
                }
                if (uxt != null)
                {
                    Debug.Log(uxt.ID, uxt);
                }
                else
                {
                    Debug.Log("Cannot find UXTrigger reference : " + uxt.ID);
                }
            }
#endif
        }

        public static void RemoveUXTriggerComponent(string id)
        {
            #if UNITY_EDITOR
            string sceneName = id.Split('/')[0];
            //AsyncOperation ao = null;
            Scene scene = EditorSceneManager.GetSceneByName(sceneName);
            if (string.IsNullOrEmpty(scene.name)) // Check if Loaded
            {
                Debug.Log("Loading scene : " + sceneName);
                EditorSceneManager.OpenScene(scenePath + sceneName + sceneExtension, OpenSceneMode.Additive);
                scene = EditorSceneManager.GetSceneByName(sceneName);
                Debug.Log("Scene loaded : " + sceneName);
            }
            if (string.IsNullOrEmpty(scene.name)) //Check if not exist
            {
                Debug.LogError("Scene doest not exist");
            }
            else
            {
                GameObject[] goArr = scene.GetRootGameObjects();
                UXTrigger uxt = null;
                for (int i = 0; i < goArr.Length; i++)
                {
                    if (goArr[i].TryGetComponent(out UXParent uxp))
                    {
                        uxt = uxp.GetUXTrigger(id);
                    }
                }
                if (uxt != null)
                {
                    GameObject go = uxt.gameObject;
                    GameObject.DestroyImmediate(uxt);
                    Debug.Log("Removed Trigger : " + id, go);
                }
                else
                {
                    Debug.Log("Cannot find UXTrigger reference : " + id);
                }
            }
#endif
        }

        public static void RemoveUXTriggerID(string uxTriggerID, string uxComponentID, bool close)
        {
            #if UNITY_EDITOR
            string sceneName = uxTriggerID.Split('/')[0];
            //AsyncOperation ao = null;
            Scene scene = EditorSceneManager.GetSceneByName(sceneName);
            
            if (string.IsNullOrEmpty(scene.name)) //Check if not exist
            {
                Debug.LogError("Could not find scene, might have been removed");
            }
            else
            {
                Debug.Log("Loading scene : " + sceneName);
                if (scene != null)
                {
                    EditorSceneManager.OpenScene(scenePath + sceneName + sceneExtension, OpenSceneMode.Additive);
                    Debug.Log("Scene loaded : " + sceneName);
                    GameObject[] goArr = scene.GetRootGameObjects();
                    UXTrigger uxt = null;
                    for (int i = 0; i < goArr.Length; i++)
                    {
                        if(goArr[i].TryGetComponent(out UXParent uxp))
                        {
                            uxt = uxp.GetUXTrigger(uxTriggerID);
                        }
                    }
                    if (uxt != null)
                    {
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(uxt.gameObject.scene);
                        uxt.RemoveEntry(uxComponentID, close);
                    }
                    else
                    {
                        Debug.Log("Cannot find UXTrigger reference : " + uxTriggerID);
                    }
                }
                else
                {
                    Debug.Log("Scene is null");
                }
            }
#endif
        }
    }
}