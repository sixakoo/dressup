﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UX
{
    public class UXPopMessage : MonoBehaviour
    {
        public UXAnimator uxAnim;
        public Image panel;
        public TextMeshProUGUI textMessage;
        private Vector2 pos;
        private float lifeTime;
        private Coroutine popCo;

        public void Start()
        {
            popCo = UXManager.RunCoroutine(Sequence());
        }

        private IEnumerator Sequence()
        {
            transform.position = pos;
            uxAnim.overrideInitialPos = true;
            uxAnim.Play(0);
            yield return new WaitForSeconds(lifeTime - uxAnim.sequenceList[1].duration);
            uxAnim.Play(1);
            yield return new WaitForSeconds(uxAnim.sequenceList[1].duration);
            if(this != null)
            {
                Destroy(gameObject);
            }
        }

        public void PopMessage(string message, Vector2 position, float duration = 2, int fontSize = 50)
        {
            textMessage.fontSize = fontSize;
            textMessage.text = message;
            pos = position;
            lifeTime = duration;
        }

        public void PopMessage(string message, float duration = 2, int fontSize = 50)
        {
            PopMessage(message, pos, duration, fontSize);
        }

        public void SetPanelColor(Color color)
        {
            panel.color = color;
        }

        public void Interupt()
        {
            if(popCo != null)
            {
                UXManager.StopCo(popCo);
            }
            UXManager.RunCoroutine(InteruptCo());
        }

        private IEnumerator InteruptCo()
        {
            uxAnim.Play(1);
            yield return new WaitForSeconds(uxAnim.sequenceList[1].duration);
            Destroy(gameObject);
        }

        public void ForceClose()
        {
            if (popCo != null)
            {
                UXManager.StopCo(popCo);
            }
            if(this != null)
            {
                Destroy(gameObject);
            }
        }
    }
}