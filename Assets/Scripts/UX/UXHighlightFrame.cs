﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UX
{
    public class UXHighlightFrame : MonoBehaviour
    {
        public UIElement prefab;
        private UIElement highlight;

        public void Activate(RectTransform target, bool flipped = false)
        {
            if (highlight == null)
            {
                highlight = Instantiate(prefab, transform, false);
            }
            highlight.gameObject.SetActive(true);
            highlight.rectTransforms[0].localScale = new Vector3(flipped ? -1 : 1, 1, 1);
            RectTransform rt = highlight.transform as RectTransform;
            rt.sizeDelta = target.sizeDelta;
            rt.position = target.position;
            highlight.animators[0].Play();
        }

        public void Hide()
        {
            if(highlight != null)
            {
                highlight.gameObject.SetActive(false);
            }
        }
    }
}