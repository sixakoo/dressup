﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    public class UXScrollRefresher : MonoBehaviour
    {
        public ScrollRect scroller;
        public float
            triggerThreshold,
            refreshIconOffset,
            refreshDuration;
        public RectTransform refreshPrefab;
        public UnityEngine.Events.UnityAction triggerFunction;
        private float scrollPos;
        private RectTransform icon;
        private bool
            isTriggered,
            isRefreshing;
        private Coroutine refreshCo;

        public void Reset()
        {
            triggerThreshold = -150;
            refreshIconOffset = 200;
            refreshDuration = 2;
        }

        public void Update()
        {
            float pos = scroller.content.anchoredPosition.y;
            if (scrollPos != pos)
            {
                scrollPos = pos;
                if (pos < -1f)
                {
                    if (pos + refreshIconOffset < triggerThreshold)
                    {
                        isTriggered = true;
                        if (icon)
                        {
                            icon.anchoredPosition = new Vector2(0, triggerThreshold);
                            icon.eulerAngles = new Vector3(0, 0, 180);
                        }
                    }
                    else
                    {
                        isTriggered = false;
                        if (!isRefreshing)
                        {
                            RefreshIcon();
                        }
                    }
                }
            }

            if (UXManager.GetClickUp() && isTriggered && !isRefreshing)
            {
                isRefreshing = true;
                triggerFunction?.Invoke();
                if (refreshCo != null)
                {
                    StopCoroutine(refreshCo);
                }
                refreshCo = StartCoroutine(RefreshCountdown());
            }
        }

        private void OnDisable()
        {
            isRefreshing = false;
            isTriggered = false;
            if (icon)
            {
                Destroy(icon.gameObject);
            }
        }

        private void RefreshIcon()
        {
            float pos = scroller.content.anchoredPosition.y;
            if (!icon)
            {
                icon = Instantiate(refreshPrefab, scroller.viewport, false);
                icon.anchorMin = new Vector2(0.5f, 1);
                icon.anchorMax = new Vector2(0.5f, 1);
                icon.pivot = new Vector2(0.5f, 0.5f);
            }
            icon.anchoredPosition = new Vector2(0, scroller.content.anchoredPosition.y + refreshIconOffset);
            float rot = (-180 * ((pos + refreshIconOffset) / triggerThreshold));
            icon.eulerAngles = new Vector3(0, 0, rot);
        }

        private IEnumerator RefreshCountdown()
        {
            if (icon.TryGetComponent(out UXAnimator uxa))
            {
                uxa.Play();
            }
            yield return new WaitForSeconds(refreshDuration);
            isRefreshing = false;
            isTriggered = false;
            if (icon)
            {
                Destroy(icon.gameObject);
            }
        }
    }
}