﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UX
{
    [RequireComponent (typeof(UXAnimator))]
    public class UXValueChangeTMP : MonoBehaviour
    {
        public TextMeshProUGUI text;
        public UXAnimator uxanim;
        private string lastValue;

        private void Reset()
        {
            text = GetComponent<TextMeshProUGUI>();
            uxanim = GetComponent<UXAnimator>();
        }

        private void Awake()
        {
            lastValue = text.text;
        }

        private void Update()
        {
            if (lastValue != text.text)
            {
                TriggerAnimation();
                lastValue = text.text;
            }
        }

        private void TriggerAnimation()
        {
            uxanim.Play(true);
        }
    }
}