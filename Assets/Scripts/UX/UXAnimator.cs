﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    [RequireComponent(typeof(RectTransform))]
    public class UXAnimator : MonoBehaviour
    {
        [System.Serializable]
        public class AnimationSequence
        {
            public string sequenceName;
            public bool
                hasMove,
                hasScale,
                hasRotate,
                hasSize,
                hasAlpha;
            public float
                duration = 1;
            public AnimationCurve
                moveCurve,
                scaleCurve,
                rotateCurve,
                sizeCurve,
                alphaCurve;
            public Vector2
                move,
                scale,
                size,
                pivot;
            public Vector3
                rotate;
            public bool isDone, isTriggered, isAnimating;
            public float baseDuration;
            public AnimationSequence()
            {
                sequenceName = "Sequence";
                hasMove = false;
                hasScale = false;
                hasRotate = false;
                isDone = false;
                isTriggered = false;
                move = Vector2.zero;
                scale = Vector2.zero;
                rotate = Vector3.zero;
                pivot = Vector2.one * -1;
                moveCurve = AnimationCurve.Linear(0, 0, 1, 1);
                scaleCurve = AnimationCurve.Linear(0, 0, 1, 1);
                rotateCurve = AnimationCurve.Linear(0, 0, 1, 1);
                alphaCurve = AnimationCurve.Linear(0, 0, 1, 1);
            }
            public static AnimationSequence Default()
            {
                AnimationSequence animSeq = new AnimationSequence();
                animSeq.sequenceName = "Sequence";
                animSeq.hasMove = false;
                animSeq.hasScale = false;
                animSeq.hasRotate = false;
                animSeq.isDone = false;
                animSeq.move = Vector2.zero;
                animSeq.scale = Vector2.zero;
                animSeq.rotate = Vector3.zero;
                animSeq.pivot = Vector2.one * -1;
                animSeq.moveCurve = AnimationCurve.Linear(0, 0, 1, 1);
                animSeq.scaleCurve = AnimationCurve.Linear(0, 0, 1, 1);
                animSeq.rotateCurve = AnimationCurve.Linear(0, 0, 1, 1);
                animSeq.alphaCurve = AnimationCurve.Linear(0, 0, 1, 1);
                return animSeq;
            }
            public void SetBaseDuration()
            {
                baseDuration = duration;
            }
        }

        public List<AnimationSequence> sequenceList = new List<AnimationSequence>();
        public Coroutine animationCo, sequenceCo, closeCo;
        public bool
            playOnStart,
            startIsResult = true,
            loop,
            disableOnStart,
            ignoreSequence,
            destroyOnEnd,
            overrideInitialPos,
            hideBeforePlay,
            startAtRandom,
            firstPlay = true,
            removeComponentOnEnd,
            isDebug;
        [Range(0, 10)]
        public float
            playOnStartDelay,
            loopInterval,
            durationDeviation,
            playDelay;
        public CanvasGroup cg;
        private bool
            isInit,
            isRunning,
            endToResult,
            markedDisabled;
        private Vector2
            initialPos,
            initialScale,
            initialSize;
        private Vector3
            initialRotation;
        private float initialAlpha;
        private Canvas canvas;
        private bool forceStop;
        private Coroutine startCo;

        public enum OnEnd
        {
            Nothing,
            Loop,
            Disable,
            Destroy,
        }

        public Direction direction;

        public enum Direction
        {
            Preset,
            ToTarget,
            FromTarget
        }

        private void Awake()
        {
            if(!isInit)
            {
                Init();
            }
        }

        public Vector2 GetInitialPos()
        {
            return initialPos;
        }

        private void Init()
        {
            if (cg == null)
            {
                if (!gameObject.TryGetComponent(out cg))
                {
                    cg = gameObject.AddComponent<CanvasGroup>();
                }
            }
            RectTransform rt = (RectTransform)transform;
            initialPos = rt.anchoredPosition;
            initialScale = rt.localScale;
            initialRotation = rt.localEulerAngles;
            initialSize = new Vector2(rt.rect.width, rt.rect.height);
            initialAlpha = cg.alpha;
            canvas = GetComponentInParent<Canvas>();
            isInit = true;
            if(hideBeforePlay)
            {
                cg.alpha = 0;
            }
            for(int i = 0; i < sequenceList.Count; i++)
            {
                sequenceList[i].SetBaseDuration();
                sequenceList[i].isDone = false;
            }
        }

        public void SetInitialScale(Vector3 scale)
        {
            initialScale = scale;
        }

        public void SetInitialPosition(Vector2 pos)
        {
            initialPos = pos;
        }

        public void SetInitialRotation(Vector3 rot)
        {
            initialRotation = rot;
        }

        private void OnEnable()
        {
            markedDisabled = false;
            if (disableOnStart && !isRunning)
            {
                gameObject.SetActive(false);
            }
            if (hideBeforePlay)
            {
                cg.alpha = 0;
            }
            if (playOnStart)
            {
                if(startCo != null)
                {
                    UXManager.StopCo(startCo);
                }
                startCo = UXManager.RunCoroutine(PlayOnStartEnumerator());
            }
        }

        private IEnumerator PlayOnStartEnumerator()
        {
            //yield return new WaitForFixedUpdate();
            yield return null;
            if (!this) { yield break; }
            if (playOnStart && playOnStartDelay > 0)
            {
                if(hideBeforePlay)
                {
                    cg.alpha = 0;
                }
            }
            if (!this) { yield break; }
            if(!markedDisabled)
            {
                Play(overrideInitialPos);
            }
            markedDisabled = false;
        }

        private void OnDestroy()
        {
            Stop();
        }

        private void OnDisable()
        {
            markedDisabled = true;
            UX.externalAnimList.Remove(this);
            isRunning = false;
            Stop();
        }

        private IEnumerator Animate(Direction direction, Vector2 targetPos, AnimationSequence animSequence, int index, bool playAll)
        {
            animSequence.isTriggered = true;
            animSequence.isAnimating = true;
            if (animSequence.duration == 0)
            {
                animSequence.duration = 0.001f;
            }
            if (isDebug)
            {
                Debug.Log("Start animate : " + name, this);
            }
            if (durationDeviation != 0)
            {
                float fluctuation = (durationDeviation / 2);
                animSequence.duration = Random.Range(animSequence.baseDuration - fluctuation, animSequence.baseDuration + fluctuation);
            }
            else
            {
                animSequence.duration = animSequence.baseDuration;
            }
            if (!isInit)
            {
                Init();
            }
            if (!UX.externalAnimList.Contains(this))
            {
                //Application.targetFrameRate = 60;
                UX.externalAnimList.Add(this);
            }
            float val = 0;
            float moveVal = 0,
                scaleVal = 0,
                rotateVal = 0,
                alphaVal = 0;
            if(startAtRandom && firstPlay)
            {
                val = Random.Range(0f, 1f);
            }
            firstPlay = false;
            RectTransform rectTransform = transform as RectTransform;
            Vector2 startPos = initialPos;
            Vector2 startScale = initialScale;
            Vector3 startRot = initialRotation;
            Vector2 diff = targetPos - (Vector2)rectTransform.position;
            if(canvas != null)
            {
                diff = diff / canvas.scaleFactor;
            }

            if (startIsResult)
            {
                startScale -= animSequence.scale;
                if (playAll)
                {
                    for (int i = sequenceList.Count; i > index + 1; i--)
                    {
                        startPos -= sequenceList[i - 1].hasMove? sequenceList[i - 1].move : Vector2.zero;
                        startScale -= sequenceList[i - 1].hasScale ? sequenceList[i - 1].scale : Vector2.zero;
                        startRot -= sequenceList[i - 1].hasRotate ? sequenceList[i - 1].rotate : Vector3.zero;
                    }
                }
            }
            else
            {
                startPos = rectTransform.anchoredPosition;
                startScale = rectTransform.localScale;
                startRot = rectTransform.rotation.eulerAngles;
            }

            while (val < 1)
            {
                if (forceStop)
                {
                    break;
                }
                if (rectTransform == null)
                {
                    break;
                }
                if(endToResult)
                {
                    val = 1;
                    endToResult = false;
                }
                else
                {
                    val += Time.deltaTime / animSequence.duration;
                }
                
                if (animSequence.hasMove)
                {
                    moveVal = animSequence.moveCurve.Evaluate(val);

                    switch(direction)
                    {
                        case Direction.Preset:
                            if (startIsResult)
                            {
                                rectTransform.anchoredPosition = ConvertMove(moveVal, animSequence.move, startPos);
                            }
                            else
                            {
                                rectTransform.anchoredPosition = Move(moveVal, animSequence.move, startPos);
                            }
                            break;
                        case Direction.ToTarget:
                            rectTransform.anchoredPosition = Move(moveVal, diff, startPos);
                            break;
                        case Direction.FromTarget:
                            rectTransform.anchoredPosition = Move(moveVal, diff * -1, startPos + diff);
                            break;
                    }
                }
                else
                {
                    rectTransform.anchoredPosition = initialPos;
                }
                if (animSequence.hasScale)
                {
                    scaleVal = animSequence.scaleCurve.Evaluate(val);
                    if (startIsResult)
                    {
                        if(!float.IsNaN(scaleVal))
                        {
                            rectTransform.localScale = ConvertScale(scaleVal, animSequence.scale, startScale);
                        }
                    }
                    else
                    {
                        rectTransform.localScale = Scale(scaleVal, animSequence.scale, startScale);
                    }
                }
                if (animSequence.hasRotate)
                {
                    rotateVal = animSequence.rotateCurve.Evaluate(val);
                    if (startIsResult)
                    {
                        rectTransform.eulerAngles = ConvertRotate(rotateVal, animSequence.rotate, startRot);
                    }
                    else
                    {
                        rectTransform.eulerAngles = Rotate(rotateVal, animSequence.rotate, startRot);
                    }
                }
                else
                {
                    rectTransform.localEulerAngles = initialRotation;
                }

                if(animSequence.hasSize)
                {
                    if (startIsResult)
                    {
                        Vector2 nextSize = playAll ? index >= sequenceList.Count  - 1? initialSize : sequenceList[index + 1].size : initialSize; 
                        rectTransform.sizeDelta = Vector2.Lerp(animSequence.size, nextSize, animSequence.sizeCurve.Evaluate(val));
                    }
                    else
                    {
                        Vector2 previousSize = playAll ? index <= 0 ? initialSize : sequenceList[index - 1].size : initialSize;
                        rectTransform.sizeDelta = Vector2.Lerp(previousSize, animSequence.size, animSequence.sizeCurve.Evaluate(val));
                    }
                }
                
                if (animSequence.hasAlpha && cg != null)
                {
                    alphaVal = animSequence.alphaCurve.Evaluate(val);
                    cg.alpha = alphaVal;
                }
                else if(hideBeforePlay)
                {
                    cg.alpha = 1;
                }
                //yield return new WaitForFixedUpdate();
                yield return null;
            }
            if (!this) { yield break; }
            animSequence.isDone = true;
            animSequence.isAnimating = false;
            endToResult = false;
            UX.externalAnimList.Remove(this);
        }

        /// <summary>
        /// Play all sequence
        /// </summary>
        public void Play()
        {
            if (this == null)
            {
                return;
            }
            if (overrideInitialPos)
            {
                SetInitialPosition((transform as RectTransform).anchoredPosition);
                SetInitialRotation((transform as RectTransform).localEulerAngles);
                //SetInitialScale((transform as RectTransform).localScale);
            }
            Play(-1);
        }

        /// <summary>
        /// Play all sequence
        /// </summary>
        public void Play(bool _overrideInitialPos)
        {
            if(this == null)
            {
                return;
            }
            if(_overrideInitialPos)
            {
                SetInitialPosition((transform as RectTransform).anchoredPosition);
                SetInitialRotation((transform as RectTransform).localEulerAngles);
                //SetInitialScale((transform as RectTransform).localScale);
            }
            Play();
        }

        public void Play(int index, bool _overrideInitialPos)
        {
            if (this == null)
            {
                return;
            }
            if (_overrideInitialPos)
            {
                SetInitialPosition((transform as RectTransform).anchoredPosition);
                SetInitialRotation((transform as RectTransform).localEulerAngles);
                //SetInitialScale((transform as RectTransform).localScale);
            }
            Play(index);
        }

        public void ShowEnd(bool end = true)
        {
            endToResult = end;
        }

        private void GetComponentCheck()
        {
            if (canvas == null)
            {
                canvas = GetComponentInParent<Canvas>();
            }
            if (cg == null)
            {
                if (!gameObject.TryGetComponent(out cg))
                {
                    cg = gameObject.AddComponent<CanvasGroup>();
                }
            }
        }

        public void Play(int index)
        {
            ClearIsAnimating();
            if (this == null)
            {
                return;
            }
            if (animationCo != null)
            {
                UXManager.StopCo(animationCo);
            }
            if (sequenceCo != null)
            {
                UXManager.StopCo(sequenceCo);
            }
            if (closeCo != null)
            {
                UXManager.StopCo(closeCo);
            }
            if (isDebug)
            {
                Debug.Log("PLAY" + name, this);
            }
            if (overrideInitialPos)
            {
                //Debug.LogError("Set init");
                SetInitialPosition((transform as RectTransform).anchoredPosition);
                SetInitialRotation((transform as RectTransform).localEulerAngles);
                //SetInitialScale((transform as RectTransform).localScale);
            }
            animationCo = UXManager.RunCoroutine(RunSequence(Direction.Preset, Vector2.zero, index, (index == -1) ? true : false));
        }

        public void PlayThenClose(int index = 0)
        {
            if(closeCo != null)
            {
                UXManager.StopCo(closeCo);
            }
            closeCo = UXManager.RunCoroutine(PlayThenCloseCo(index));
        }

        public void PlayThenDestroy(int index = 0)
        {
            if (closeCo != null)
            {
                UXManager.StopCo(closeCo);
            }
            closeCo = UXManager.RunCoroutine(PlayThenDestroyCo(index));
        }

        private IEnumerator PlayThenCloseCo(int index = 0)
        {
            Play(index);
            yield return new WaitUntil(() => sequenceList[index].isDone);
            if (!this) { yield break; }
            gameObject.SetActive(false);
        }

        private IEnumerator PlayThenDestroyCo(int index = 0)
        {
            Play(index);
            yield return new WaitUntil(() => sequenceList[index].isDone);
            if (!this) { yield break; }
            Destroy(gameObject);
        }

        public void Stop()
        {
            ClearIsAnimating();
            if (animationCo != null)
            {
                UXManager.StopCo(animationCo);
            }
            if(sequenceCo != null)
            {
                UXManager.StopCo(sequenceCo);
            }
            forceStop = true;
            UX.externalAnimList.Remove(this);
            if(isDebug)
            {
                Debug.Log("Stop : " + name, this);
            }
        }

        public void StopAndDisable()
        {
            UXManager.RunCoroutine(StopAndDisableCo());
        }

        private IEnumerator StopAndDisableCo()
        {
            ClearIsAnimating();
            if (animationCo != null)
            {
                UXManager.StopCo(animationCo);
            }
            if (sequenceCo != null)
            {
                UXManager.StopCo(sequenceCo);
            }
            if (isDebug)
            {
                Debug.Log("Force stop : " + name, this);
            }
            forceStop = true;
            UX.externalAnimList.Remove(this);
            yield return null;
            if (!this) { yield break; }
            gameObject.SetActive(false);
        }

        public void MoveTo(Vector2 targetPos, int index, bool overridePos = false)
        {
            if (overridePos)
            {
                SetInitialPosition((transform as RectTransform).anchoredPosition);
                //SetInitialRotation((transform as RectTransform).localEulerAngles);
                //SetInitialScale((transform as RectTransform).localScale);
            }
            if(animationCo != null)
            {
                UXManager.StopCo(animationCo);
            }
            if (isDebug)
            {
                Debug.Log("Move : " + name, this);
            }
            animationCo = UXManager.RunCoroutine(RunSequence(Direction.ToTarget, targetPos, index, false));
        }

        public void MoveFrom(Vector2 targetPos, int index, bool overridePos = false)
        {
            if (overrideInitialPos)
            {
                SetInitialPosition((transform as RectTransform).anchoredPosition);
                //SetInitialRotation((transform as RectTransform).localEulerAngles);
                //SetInitialScale((transform as RectTransform).localScale);
            }
            if (animationCo != null)
            {
                UXManager.StopCo(animationCo);
            }
            if (isDebug)
            {
                Debug.Log("Move : " + name, this);
            }
            animationCo = UXManager.RunCoroutine(RunSequence(Direction.FromTarget, targetPos, index, false));
        }

        //public void MoveToSequence(Vector2 targetPos, int index)
        //{
        //    Reset();
        //    if (sequenceCo != null)
        //    {
        //        StopCoroutine(sequenceCo);
        //    }
        //    sequenceCo = StartCoroutine(Animate(Direction.ToTarget, targetPos, sequenceList[index], index, false));
        //}

        private void ClearIsAnimating()
        {
            foreach (var seq in sequenceList)
            {
                seq.isAnimating = false;
            }
        }

        public IEnumerator RunSequence(Direction direction, Vector2 target, int index, bool playAll)
        {
            if(sequenceList.Count == 0)
            {
                yield break;
            }
            firstPlay = true;
            if(playOnStart && playOnStartDelay > 0)
            {
                yield return new WaitForSeconds(playOnStartDelay);
            }
            if(playDelay > 0)
            {
                for (int i = 0; i < playDelay; i++)
                {
                    yield return null;
                }
            }
            if (overrideInitialPos)
            {
                SetInitialPosition((transform as RectTransform).anchoredPosition);
                SetInitialRotation((transform as RectTransform).localEulerAngles);
            }

            if (isDebug)
            {
                Debug.Log("Run sequence : " + name, this);
            }
            
            forceStop = false;            
            Repeat:
            if (!this) { yield break; }
            if (!gameObject.activeSelf)
            {
                isRunning = true;
                gameObject.SetActive(true);
            }
            if(isDebug)
            {
                Debug.Log("Force stop : " + name, this);
            }
            if (forceStop)
            {
                yield break;
            }
            AnimatorReset();
            if (sequenceCo != null)
            {
                UXManager.StopCo(sequenceCo);
            }

            switch (direction)
            {
                case Direction.Preset:
                    if (playAll)
                    {
                        for (int i = 0; i < sequenceList.Count; i++)
                        {
                            sequenceCo = UXManager.RunCoroutine(Animate(Direction.Preset, Vector2.zero, sequenceList[i], i, true));
                            yield return new WaitUntil(() => sequenceList[i].isDone);
                        }
                    }
                    else
                    {
                        sequenceCo = UXManager.RunCoroutine(Animate(Direction.Preset, Vector2.zero, sequenceList[index], index, false));
                        yield return new WaitUntil(() => sequenceList[index].isDone);
                    }
                    break;
                case Direction.ToTarget:
                    sequenceCo = UXManager.RunCoroutine(Animate(Direction.ToTarget, target, sequenceList[index], index, false));
                    yield return new WaitUntil(() => sequenceList[index].isDone);
                    break;
                case Direction.FromTarget:
                    sequenceCo = UXManager.RunCoroutine(Animate(Direction.FromTarget, target, sequenceList[index], index, false));
                    yield return new WaitUntil(() => sequenceList[index].isDone);
                    break;
            }
            if(loop)
            {
                if(isDebug)
                {
                    Debug.Log("loop", this);
                }
                yield return new WaitForSeconds(loopInterval);
                goto Repeat;
            }
            if (destroyOnEnd)
            {
                if (!this) { yield break; }
                Destroy(gameObject);
            }
            if(removeComponentOnEnd)
            {
                if (!this) { yield break; }
                Destroy(cg);
                Destroy(this);
            }
        }

        public void AnimatorReset()
        {
            if (!this) { return; }
            if (!isInit)
            {
                Init();
            }
            ClearIsAnimating();
            for (int i = 0; i < sequenceList.Count; i++)
            {
                sequenceList[i].isDone = false;
            }
            RectTransform rectTransform = transform as RectTransform;
            rectTransform.anchoredPosition = initialPos;
            rectTransform.localScale = initialScale;
            rectTransform.rotation = Quaternion.Euler(initialRotation);
            ShowEnd(false);
        }
        
        private static Vector2 ConvertMove(float x, Vector2 move, Vector2 initialPos)
        {
            if (move.x > 1 || move.x < -1 || move.y > 1 || move.y < -1)
            {
                return new Vector2((move.x * (1f - x) - initialPos.x) * -1, (move.y * (1f - x) - initialPos.y) * -1);
            }
            else
            {
                //change later
                return new Vector2((Screen.width * move.x) * (1f - x) + initialPos.x, (Screen.height * move.y) * (1f - x) + initialPos.y);
            }
        }

        private static Vector2 Move(float x, Vector2 move, Vector2 initialPos)
        {
            if (move.x > 1 || move.x < -1 || move.y > 1 || move.y < -1)
            {
                return new Vector2(move.x * x + initialPos.x, move.y * x + initialPos.y);
            }
            else
            {
                //change later
                return new Vector2((Screen.width * move.x) * (1f - x) + initialPos.x, (Screen.height * move.y) * (1f - x) + initialPos.y);
            }
        }

        private static Vector2 ConvertScale(float x, Vector2 shrink, Vector2 initialScale)
        {
            return (new Vector2(shrink.x * x + initialScale.x, shrink.y * x + initialScale.y));
        }

        private static Vector2 Scale(float x, Vector2 shrink, Vector2 initialScale)
        {
            return (new Vector2(shrink.x * x + initialScale.x, shrink.y * x + initialScale.y));
        }

        private static Vector3 ConvertRotate(float x, Vector3 rotate, Vector3 initialRot)
        {
            return new Vector3((initialRot.x - rotate.x) + (rotate.x * (x)), (initialRot.y - rotate.y) + (rotate.y * (x)), (initialRot.z - rotate.z) + (rotate.z * (x)));
        }

        private static Vector3 Rotate(float x, Vector3 rotate, Vector3 initialRot)
        {
            return new Vector3(initialRot.x + (rotate.x * (x)), initialRot.y + (rotate.y * (x)), initialRot.z + (rotate.z * (x)));
        }
    }
}