﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UX
{
    [System.Serializable]
    public class RunOnEnabled : MonoBehaviour
    {
        [SerializeField]
        public List<UXComponent> memberList;

        private void OnEnable()
        {
            if(memberList != null)
            {
                for (int i = 0; i < memberList.Count; i++)
                {
                    UX.OpenAnimation(memberList[i].ID);
                }
            }
        }

        private void OnDisable()
        {
            if(memberList != null)
            {
                for (int i = 0; i < memberList.Count; i++)
                {
                    UX.CloseAnimation(memberList[i].ID);
                }
            }
        }
    }
}