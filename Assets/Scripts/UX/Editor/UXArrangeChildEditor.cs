﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UX
{
    [CustomEditor (typeof(UXArrangeChild))]
    public class UXArrangeChildEditor : Editor
    {
        public void OnEnable()
        {
            UXArrangeChild uxa = target as UXArrangeChild;
            uxa.children = new List<RectTransform>();
            foreach (Transform rt in uxa.transform)
            {
                
                uxa.children.Add(rt as RectTransform);
            }
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            ((UXArrangeChild)target).ControlChildSize();
        }
    }
}
