﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace UX
{
    [CustomEditor(typeof(UXComponent)), CanEditMultipleObjects]
    public class UXComponentEditor : Editor
    {
        public SerializedProperty styleIndex;
        public SerializedProperty typeIndex;
        public SerializedProperty removeScene;
        public SerializedProperty parent;
        public SerializedProperty configuration;
        public SerializedProperty customConfig;
        public string id = "";
        public Button obj;
        private int
            delOpen = -1,
            delClose = -1;

        private void OnEnable()
        {
            
            LinkParentReference();
            var component = target as UXComponent;
            styleIndex = serializedObject.FindProperty("index");
            typeIndex = serializedObject.FindProperty("triggerType");
            removeScene = serializedObject.FindProperty("removeScene");
            parent = serializedObject.FindProperty("uxparent");
            configuration = serializedObject.FindProperty("configuration");
            customConfig = serializedObject.FindProperty("customConfig");
            if (!Application.isPlaying)
            {
                GetParent();
            }
            if (string.IsNullOrEmpty(component.ID))
            {
                component.ID = component.gameObject.name;
            }
        }

        private void GetParent()
        {
            UXParent uxP = parent.objectReferenceValue as UXParent;
            if (uxP == null)
            {
                Debug.Log("Get Parent");
                var component = target as UXComponent;
                Canvas c = component.GetComponentInParent<Canvas>();
                if (c == null)
                {
                    Debug.LogError("UXComponent must be a child of canvas or enabled", component.transform);
                }
                else
                {
                    uxP = c.GetComponent<UXParent>();
                    if (uxP == null)
                    {
                        uxP = c.gameObject.AddComponent<UXParent>();
                    }
                    parent.objectReferenceValue = uxP;
                    Debug.Log("Assign parent");
                }
            }
            uxP.GetChildUX();

            serializedObject.ApplyModifiedProperties();
        }

        private void CheckTriggerType()
        {
            var component = target as UXComponent;
            if (component.typeTracker != component.triggerType)
            {
                component.typeTracker = component.triggerType;
                component.RemoveTriggersInList(ref component.openTriggerList);
                component.RemoveTriggersInList(ref component.closeTriggerList);
                component.RemoveTriggersInList(ref component.toggleTriggerList);
            }
        }

        public void LinkParentReference()
        {
            UXComponent uxc = target as UXComponent;
            uxc.uxparent = uxc.GetComponentInParent<UXParent>();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            var component = target as UXComponent;
            if (!EditorApplication.isPlaying)
            {
                if (!serializedObject.isEditingMultipleObjects)
                {
                    id = component.ID;
                    id = EditorGUILayout.TextField("ID", id);
                    component.ID = id;
                    if (!component.ID.Contains(component.gameObject.scene.name + "/"))
                    {
                        component.ID = component.gameObject.scene.name + "/" + id;
                    }
                    CheckTriggerType();
                }
            }
            else
            {
                EditorGUILayout.TextField("ID", component.ID);
            }

            if (!EditorApplication.isPlaying)
            {
                //TriggerType Dropdown
                EditorGUILayout.PropertyField(typeIndex);
                //if(component.triggerType == UXComponent.TriggerType.Sequence)
                EditorGUILayout.PropertyField(removeScene);

                //Configuration Dropdown
                EditorGUILayout.PropertyField(configuration);
            }

            switch (component.configuration)
            {
                case UXComponent.Configuration.Preset: //Preset Style Dropdown
                    if (UXEditor.uxManager != null)
                    {
                        if (!EditorApplication.isPlaying)
                        {
                            styleIndex.intValue = EditorGUILayout.Popup("UX Style", styleIndex.intValue, UXEditor.uxManager.GetUXTypeNames());
                        }
                        else
                        {
                            GUILayout.BeginHorizontal();
                            GUILayout.Label("UX Style : ");
                            GUILayout.Box(UXEditor.uxManager.GetUXTypeName(styleIndex.intValue), GUILayout.ExpandWidth(true));
                            GUILayout.EndHorizontal();
                        }
                    }
                    else
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("UX Style");
                        GUILayout.Box("Add UXManager to the scene to enabled preset configuration");
                        GUILayout.EndHorizontal();
                    }
                    break;
                case UXComponent.Configuration.Custom: //Custom Configuration
                    if (!serializedObject.isEditingMultipleObjects)
                    {
                        EditorGUILayout.PropertyField(customConfig);
                    }
                    break;
            }

            if (!serializedObject.isEditingMultipleObjects)
            {
                GUI.color = Color.white;
                switch (component.triggerType)
                {
                    case UXComponent.TriggerType.Normal:
                        GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));
                        GUILayout.BeginHorizontal();
                        GUILayout.BeginVertical();
                        EditorGUILayout.LabelField("Open Trigger", GUILayout.Width(150));
                        if (!EditorApplication.isPlaying)
                        {
                            obj = EditorGUILayout.ObjectField(obj, typeof(Button), true, GUILayout.Width(150)) as Button;
                        }
                        if (component.openTriggerList != null) //Show TriggerList UI
                        {
                            for (int i = 0; i < component.openTriggerList.Count; i++)
                            {
                                GUILayout.BeginHorizontal();
                                if (GUILayout.Button(component.openTriggerList[i], GUILayout.Width(120)))
                                {
                                    UX.ShowUXTriggerReference(component.openTriggerList[i]);
                                }

                                if (!EditorApplication.isPlaying) //Show Remove Button
                                {
                                    GUI.color = Color.red;
                                    if (GUILayout.Button("X", GUILayout.Width(30)))
                                    {
                                        delOpen = i;
                                    }
                                    GUI.color = Color.white;
                                }
                                GUILayout.EndHorizontal();
                            }
                            if (delOpen != -1)
                            {
                                UX.RemoveUXTriggerID(component.openTriggerList[delOpen], component.ID, false);
                                component.openTriggerList.RemoveAt(delOpen);
                                delOpen = -1;
                            }
                        }
                        if (obj != null)
                        {
                            if (component.openTriggerList == null)
                            {
                                component.openTriggerList = new List<string>();
                            }

                            //Get or add trigger component
                            UXTrigger trigger = obj.GetComponent<UXTrigger>();
                            if (trigger == null)
                            {
                                trigger = obj.gameObject.AddComponent<UXTrigger>();
                                trigger.Setup();
                            }
                            //Check if already exist
                            if (!component.openTriggerList.Contains(trigger.ID))
                            {
                                trigger.AddObjectToList(component, UX.AnimationType.Open);
                                component.openTriggerList.Add(trigger.ID);
                            }
                            else
                            {
                                Debug.Log("Already exist");
                            }
                        }
                        obj = null;

                        GUILayout.EndVertical();
                        GUILayout.Box("", GUILayout.ExpandHeight(true), GUILayout.Width(2));
                        GUILayout.BeginVertical();
                        EditorGUILayout.LabelField("Close Trigger", GUILayout.Width(150));
                        if (!EditorApplication.isPlaying)
                        {
                            obj = EditorGUILayout.ObjectField(obj, typeof(Button), true, GUILayout.Width(150)) as Button;
                        }
                        if (component.closeTriggerList != null)
                        {
                            for (int i = 0; i < component.closeTriggerList.Count; i++)
                            {
                                GUILayout.BeginHorizontal();
                                if (GUILayout.Button(component.closeTriggerList[i], GUILayout.Width(120)))
                                {
                                    UX.ShowUXTriggerReference(component.closeTriggerList[i]);
                                }

                                if (!EditorApplication.isPlaying)
                                {
                                    GUI.color = Color.red;
                                    if (GUILayout.Button("X", GUILayout.Width(30)))
                                    {
                                        delClose = i;
                                    }
                                    GUI.color = Color.white;
                                }

                                GUILayout.EndHorizontal();
                            }
                            if (delClose != -1)
                            {
                                UX.RemoveUXTriggerID(component.closeTriggerList[delClose], component.ID, true);
                                component.closeTriggerList.RemoveAt(delClose);
                                delClose = -1;
                            }
                        }

                        if (obj != null)
                        {
                            if (component.closeTriggerList == null)
                            {
                                component.closeTriggerList = new List<string>();
                            }
                            //Get or add trigger component
                            UXTrigger trigger = obj.GetComponent<UXTrigger>();
                            if (trigger == null)
                            {
                                trigger = obj.gameObject.AddComponent<UXTrigger>();
                                trigger.Setup();
                            }
                            //Check if already exist
                            if (!component.closeTriggerList.Contains(trigger.ID))
                            {
                                trigger.AddObjectToList(component, UX.AnimationType.Close);
                                component.closeTriggerList.Add(trigger.ID);
                            }
                            else
                            {
                                Debug.Log("Already exist");
                            }
                        }
                        obj = null;
                        GUILayout.EndVertical();
                        GUILayout.EndHorizontal();
                        break;

                    case UXComponent.TriggerType.Toggle:
                        GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(3));
                        EditorGUILayout.LabelField("Toggle Trigger");
                        if (!EditorApplication.isPlaying)
                        {
                            obj = EditorGUILayout.ObjectField(obj, typeof(Button), true) as Button;
                        }
                        if (component.toggleTriggerList != null)
                        {
                            for (int i = 0; i < component.toggleTriggerList.Count; i++)
                            {
                                GUILayout.BeginHorizontal();
                                if (GUILayout.Button(component.toggleTriggerList[i]))
                                {
                                    UX.ShowUXTriggerReference(component.toggleTriggerList[i]);
                                }

                                if (!EditorApplication.isPlaying)
                                {
                                    GUI.color = Color.red;
                                    if (GUILayout.Button("X", GUILayout.Width(30)))
                                    {
                                        delOpen = i;
                                    }
                                    GUI.color = Color.white;
                                }
                                GUILayout.EndHorizontal();
                            }

                            if (delOpen != -1)
                            {
                                UX.RemoveUXTriggerID(component.toggleTriggerList[delOpen], component.ID, false);
                                component.toggleTriggerList.RemoveAt(delOpen);
                                delOpen = -1;
                            }
                        }
                        if (obj != null)
                        {
                            if (component.toggleTriggerList == null)
                            {
                                component.toggleTriggerList = new List<string>();
                            }
                            //Get or add trigger component
                            UXTrigger trigger = obj.GetComponent<UXTrigger>();
                            if (trigger == null)
                            {
                                trigger = obj.gameObject.AddComponent<UXTrigger>();
                                trigger.Setup();
                            }
                            //Check if already exist
                            if (!component.toggleTriggerList.Contains(trigger.ID))
                            {
                                trigger.AddObjectToList(component, UX.AnimationType.Toggle);
                                component.toggleTriggerList.Add(trigger.ID);
                            }
                            else
                            {
                                Debug.Log("Already exist");
                            }
                        }
                        obj = null;
                        break;
                }
            }

            if (EditorApplication.isPlaying)
            {
                //EditorGUILayout.PropertyField(configuration);
                GUILayout.BeginHorizontal();
                if(GUILayout.Button("Test activation"))
                {
                    TestAnimation(true);
                }
                if (GUILayout.Button("Test deactivation"))
                {
                    TestAnimation(false);
                }
                GUILayout.EndHorizontal();
                
            }
            serializedObject.ApplyModifiedProperties();
            Undo.RecordObject(component, "component index");
        }

        private Coroutine testCo;

        private void TestAnimation(bool state)
        {
            UXComponent uxC = target as UXComponent;
            uxC.uxObject.active = state;
            if(testCo != null)
            {
                uxC.StopCoroutine(testCo);
            }
            testCo = UXManager.RunCoroutine(RunTest(state, uxC));
        }

        private IEnumerator RunTest(bool state, UXComponent uxComponent)
        {
            bool initialState = uxComponent.uxObject.active;
            if(state)
            {
                uxComponent.Off();
                uxComponent.uxObject.active = true;
                while (!uxComponent.uxObject.isDone)
                {
                    UX.FadeAnimation(uxComponent.uxObject);
                    yield return null;
                }
            }
            else
            {
                uxComponent.On();
                uxComponent.uxObject.active = false;
                while (!uxComponent.uxObject.isDone)
                {
                    UX.FadeAnimation(uxComponent.uxObject);
                    yield return null;
                }
            }
            uxComponent.uxObject.active = initialState;
        }
    }
}
#endif