﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UX;

namespace UX
{
    [CustomEditor(typeof(UXSequencer)), CanEditMultipleObjects]
    public class UXSequencerEditor : Editor
    {
        UXComponent obj;
        int del = -1;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            GUI.color = Color.white;
            DrawDefaultInspector();
            if (!serializedObject.isEditingMultipleObjects)
            {
                UXSequencer uxs = (UXSequencer)target;
                if(uxs.arrangeChild != UXSequencer.ArrangeChild.None && uxs.isActiveAndEnabled)
                {
                    uxs.spacing = (int)EditorGUILayout.FloatField("Spacing", uxs.spacing);
                    uxs.ControlChildSize();
                }
                GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));
                obj = EditorGUILayout.ObjectField(obj, typeof(UXComponent), true) as UXComponent;
                int removeIndex = -1;
                if (uxs.childUX != null)
                {
                    for (int i = 0; i < uxs.childUX.Count; i++)
                    {
                        if(uxs.childUX[i] == null) //check if removed by other means
                        {
                            removeIndex = i;
                        }
                        else
                        {
                            GUILayout.BeginHorizontal();
                            GUILayout.Label((i + 1).ToString() + ".", GUILayout.Width(20));
                            if (GUILayout.Button(uxs.childUX[i].ID))
                            {
                                Debug.Log(uxs.childUX[i].name, uxs.childUX[i]);
                            }
                            GUI.color = Color.red;

                            if (GUILayout.Button("X", GUILayout.Width(30)))
                            {
                                del = i;
                            }
                            GUI.color = Color.white;
                            GUILayout.EndHorizontal();
                        }
                        
                    }
                    if(removeIndex != -1)
                    {
                        uxs.childUX.RemoveAt(removeIndex);
                        removeIndex = -1;
                    }
                    if (del != -1)
                    {
                        uxs.childUX.RemoveAt(del);
                        del = -1;
                    }
                    GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));

                    if (obj != null)
                    {
                        if (obj.triggerType == UXComponent.TriggerType.Sequence)
                        {
                            uxs.childUX.Add(obj);
                        }
                    }
                    obj = null;
                }
                if (GUILayout.Button("Get child sequence"))
                {
                    uxs.GetChildUX();
                    Undo.RecordObject(uxs, "component index");
                }
                serializedObject.ApplyModifiedProperties();
                Undo.RecordObject(uxs, "component index");
            }
        }
    }
}