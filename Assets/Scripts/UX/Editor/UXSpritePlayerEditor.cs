﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace UX
{
    [CustomEditor(typeof(UXSpritePlayer))]
    public class UXSpritePlayerEditor : Editor
    {
        private UXSpritePlayer component;
        private int pickerIndex; //remove later
        private bool selectSprite = false;
        private Vector2 gridCheck;
        private Rect lastSize;
        private bool preview;

        private void OnEnable()
        {
            component = (UXSpritePlayer)target;
            if(component.spriteTransform == null || component.mask == null)
            {
                component.AssignComponents();
            }
        }

        public override void OnInspectorGUI()
        {
            
            //GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            
            if (component.image.sprite != null)
            {
                //EditorGUI.DrawPreviewTexture(new Rect(0, 0, 500, 500), atlas.texture);

                GUILayout.Box(component.image.sprite.texture, GUILayout.Width(100), GUILayout.Height(100));
            }
            else
            {
                GUILayout.Box("NONE" , GUILayout.Width(100), GUILayout.Height(100));
            }

            GUILayout.BeginVertical();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("grid"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("totalFrames"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("fps"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("preserveAspect"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("playOnStart"));
            if(component.playOnStart)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("playOnStartDelay"));
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("loop"));
            if(component.loop)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("loopInterval"));
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            //GUILayout.Label("",GUILayout.ExpandHeight(true));
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Select atlas", GUILayout.Width(100)))
            {
                selectSprite = true;
#if UNITY_EDITOR
                EditorGUIUtility.ShowObjectPicker<Sprite>(component, false, "", pickerIndex);
#endif
            }

            if (Event.current.commandName == "ObjectSelectorClosed")
            {
                selectSprite = false;
            }

            if (Event.current.commandName == "ObjectSelectorUpdated" && selectSprite)
            {
#if UNITY_EDITOR
                component.image.sprite = EditorGUIUtility.GetObjectPickerObject() as Sprite;
#endif
                //component.spriteID = (EditorGUIUtility.GetObjectPickerObject() as Sprite).name;
                component.SetupImage();
                selectSprite = false;
                serializedObject.ApplyModifiedProperties();
                Undo.RegisterCompleteObjectUndo(component, "Atlas Select");
            }

            //Maybe later
            //
            //if (!preview)
            //{
            if(EditorApplication.isPlaying)
            {
                if (GUILayout.Button("Preview"))
                {
                    //preview = true;
                    component.Play();
                }
            }
            
            //}
            //else
            //{
            //    if (GUILayout.Button("Stop"))
            //    {
            //        preview = false;
            //    }
            //}

            GUILayout.EndHorizontal();

            //Debug.Log(gridCheck);
            if(gridCheck != component.grid)
            {
                gridCheck = component.grid;
                component.SetupImage();
            }

            if(((RectTransform)component.transform).rect != lastSize)
            {

                lastSize = ((RectTransform)component.transform).rect;
                component.SetupImage();
            }

            if(component.preserveAspect != component.image.preserveAspect)
            {
                component.image.preserveAspect = component.preserveAspect;
                component.SetupImage();
            }
            serializedObject.ApplyModifiedProperties();
            Undo.RecordObject(component, "UXSpritePlayer");
        }
    }
}