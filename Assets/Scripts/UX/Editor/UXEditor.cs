﻿using UnityEngine;
using System.Collections;
using UnityEditor;
//using UXAudio;
namespace UX
{
    [InitializeOnLoad] //Refresh when open
    public class UXEditor : Editor {

        public static UXManager uxManager;
        //public static UIAudioManager uiAudioManager;

        static UXEditor()
        {
            EditorApplication.hierarchyChanged -= WindowChange;
            EditorApplication.hierarchyChanged += WindowChange;
            WindowChange();
        }

        private static void WindowChange()
        {
            uxManager = FindObjectOfType<UXManager>();
            //uiAudioManager = FindObjectOfType<UIAudioManager>();
        }
    }
}