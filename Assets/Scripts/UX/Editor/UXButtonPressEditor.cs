﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UX
{
    [CustomEditor (typeof(UXButtonPress)), CanEditMultipleObjects]
    public class UXButtonPressEditor : Editor
    {
        public SerializedProperty configuration;
        public SerializedProperty customConfig;
        public SerializedProperty index;

        public void OnEnable()
        {
            configuration = serializedObject.FindProperty("configuration");
            customConfig = serializedObject.FindProperty("config");
            index = serializedObject.FindProperty("index");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            UXButtonPress uxb = target as UXButtonPress;
            EditorGUILayout.PropertyField(configuration);
            switch (uxb.configuration)
            {
                case UXButtonPress.Configuration.Custom:
                    EditorGUILayout.PropertyField(customConfig);
                    break;
                case UXButtonPress.Configuration.Preset:
                    if (!EditorApplication.isPlaying)
                    {
                        index.intValue = EditorGUILayout.Popup("UX Style", index.intValue, UXEditor.uxManager.GetUXTypeNames());
                    }
                    break;
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}