﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UX
{
    [CustomEditor(typeof(UXAnimator))]
    [CanEditMultipleObjects]
    public class UXAnimatorEditor : Editor
    {
        private int sequenceIndex = -1;

        public override void OnInspectorGUI()
        {
            var component = target as UXAnimator;
            serializedObject.Update();
            int removeSeqIndex = -1;
            for(int i = 0; i < component.sequenceList.Count; i++)
            {
                GUILayout.BeginHorizontal();
                if(sequenceIndex == i)
                {
                    GUI.color = Color.gray;
                }
                if(GUILayout.Button(component.sequenceList[i].sequenceName))
                {
                    if(sequenceIndex == i)
                    {
                        sequenceIndex = -1;
                    }
                    else
                    {
                        sequenceIndex = i;
                    }
                }
                GUI.color = Color.white;
                GUI.color = Color.red;
                if (GUILayout.Button("X", GUILayout.Width(30)))
                {
                    removeSeqIndex = i;
                }
                GUI.color = Color.white;
                GUILayout.EndHorizontal();

                if(sequenceIndex == i)
                {
                    AnimationSequencePanel();
                }
            }
            if(removeSeqIndex != -1)
            {
                component.sequenceList.RemoveAt(removeSeqIndex);
            }
            if(GUILayout.Button("Add a sequence +"))
            {
                UXAnimator.AnimationSequence uxas = new UXAnimator.AnimationSequence();
                uxas.sequenceName = "Animation Sequence " + (component.sequenceList.Count + 1);
                component.sequenceList.Add(uxas);
            }
            component.startIsResult = GUILayout.Toggle(component.startIsResult, "Start is result");
            EditorGUILayout.PropertyField(serializedObject.FindProperty("durationDeviation"));
            component.playOnStart = GUILayout.Toggle(component.playOnStart, "Play on start");
            if (component.playOnStart)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("playOnStartDelay"));
            }
            component.overrideInitialPos = GUILayout.Toggle(component.overrideInitialPos, "Override initial position");
            component.loop = GUILayout.Toggle(component.loop, "Loop");
            if (component.loop)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("loopInterval"));
            }
            component.disableOnStart = GUILayout.Toggle(component.disableOnStart, "Disable on Start");
            component.destroyOnEnd = GUILayout.Toggle(component.destroyOnEnd, "DestroyOnEnd");
            component.hideBeforePlay = GUILayout.Toggle(component.hideBeforePlay, "Hide before Play");
            component.startAtRandom = GUILayout.Toggle(component.startAtRandom, "Start at random");
            component.removeComponentOnEnd = GUILayout.Toggle(component.removeComponentOnEnd, "Remove Component On End");
            component.isDebug = GUILayout.Toggle(component.isDebug, "isDebug");
            if (EditorApplication.isPlaying)
            {
                if (GUILayout.Button("Test"))
                {
                    component.Play(true);
                }
            }
            serializedObject.ApplyModifiedProperties();
            Undo.RecordObject(component, "AddSequence");
        }

        public void AnimationSequencePanel()
        {
            var component = target as UXAnimator;

            SerializedProperty sp = serializedObject.FindProperty("sequenceList");
            IEnumerator e = sp.GetEnumerator();

            int i = 0;
            
            while (e.MoveNext())
            {
                if (i == sequenceIndex)
                {
                    SerializedProperty s = e.Current as SerializedProperty;
                    EditorGUILayout.PropertyField(s);
                    //GUI.color = Color.clear;
                    //GUILayout.Box("", GUILayout.ExpandWidth(true) ,GUILayout.Height(200));
                    //GUI.color = Color.white;
                }
                i++;
            }
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));
        }
    }
}