﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UX;

[CustomEditor (typeof(UXSwipe))]
public class UXSwipeEditor : Editor{

    public void OnEnable()
    {
        if(!Application.isPlaying)
        {
            UXSwipe uxs = target as UXSwipe;
            uxs.UpdateContentSize();
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}
