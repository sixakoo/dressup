﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UX
{
    [CustomEditor(typeof(UXParent)), CanEditMultipleObjects]
    public class UXParentEditor : Editor
    {
        UXComponent obj;

        public void OnEnable()
        {
            UpdateList();
            UXManager.AddToList(target as UXParent);
        }

        private void UpdateList()
        {
            if(!Application.isPlaying)
            {
                var parent = target as UXParent;
                parent.GetChildTrigger();
                parent.GetChildUX();
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            GUI.color = Color.white;
            UXParent uxs = (UXParent)target;
            GUILayout.Label("UXTriggers");
            if (!serializedObject.isEditingMultipleObjects)
            {
                if(uxs.uxTriggerList != null)
                {
                    for(int i = 0; i < uxs.uxTriggerList.Count; i++)
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label((i + 1).ToString() + ".", GUILayout.Width(20));
                        if (GUILayout.Button(uxs.uxTriggerList[i].ID))
                        {
                            Debug.Log(uxs.uxTriggerList[i].ID, uxs.uxTriggerList[i]);
                        }
                        GUILayout.EndHorizontal();
                    }
                    GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));
                }

                GUILayout.Label("UXComponents");
                //GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));
                
                if (uxs.uxComponentList != null)
                {
                    for (int i = 0; i < uxs.uxComponentList.Count; i++)
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label((i + 1).ToString() + ".", GUILayout.Width(20));
                        if(GUILayout.Button(uxs.uxComponentList[i].ID))
                        {
                            Debug.Log(uxs.uxComponentList[i].name, uxs.uxComponentList[i]);
                        }
                        GUILayout.EndHorizontal();
                    }
                    GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));
                }
                if (GUILayout.Button("Update"))
                {
                    UpdateList();
                }
            }
            serializedObject.ApplyModifiedProperties();
            Undo.RecordObject(target, "Parent Update");
        }
    }
}