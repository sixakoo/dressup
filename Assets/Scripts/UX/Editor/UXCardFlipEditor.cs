﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UX
{
    [CustomEditor(typeof(UXCardFlip)), CanEditMultipleObjects]
    public class UXCardFlipEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            UXCardFlip component = target as UXCardFlip;
            base.OnInspectorGUI();
            if(Application.isPlaying)
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Flip Back"))
                {
                    component.Flip(true);
                }
                if (GUILayout.Button("Flip Front"))
                {
                    component.Flip(false);
                }
                GUILayout.EndHorizontal();
            }
        }
    }
}