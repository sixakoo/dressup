﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UX
{
    [CustomPropertyDrawer(typeof(UXAnimator.AnimationSequence))]
    public class UXAnimationSequenceEditor : PropertyDrawer
    {
        float cellHeight = 20;
        Vector2 padding = new Vector2(10, 10);
        int row = 18;
        int dynaRow = 0;
        Color darkerGray = new Color(0.4f, 0.4f, 0.4f, 1);

        public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
        {
            Rect box = new Rect(rect.x + padding.x, rect.y + padding.y, rect.width - (padding.x * 2), cellHeight);
            Rect[] rects = new Rect[row];
            Rect[] rects2 = new Rect[row];
            Rect[] rects3 = new Rect[row];
            Rect[] rects4 = new Rect[row];
            int autoI = 0;
            for (int i = 0; i < row; i++)
            {
                
                rects[i] = new Rect(box.x, box.y + cellHeight * i, box.width * 0.9f, cellHeight);
                rects2[i] = new Rect(rect.x + rects[i].width + (padding.x), box.y + cellHeight * i, box.width * 0.1f, cellHeight * 2);
                rects3[i] = new Rect(box.x, box.y + cellHeight * i, box.width, cellHeight);
                rects4[i] = new Rect(box.x, box.y + cellHeight * i, box.width, cellHeight * 2);

                GUI.color = Color.gray;
                GUI.Box(new Rect(rect.x, rect.y, rect.width, rect.height), "");
                GUI.color = Color.white;
            }
            EditorGUI.Slider(rects3[autoI], property.FindPropertyRelative("duration"), 0, 10, "Duration"); autoI++;
            property.FindPropertyRelative("baseDuration").floatValue = property.FindPropertyRelative("duration").floatValue;
            property.serializedObject.ApplyModifiedProperties();
            if (!property.FindPropertyRelative("hasMove").boolValue)
            {
                if (GUI.Button(rects3[autoI], "Move"))
                {
                    property.FindPropertyRelative("hasMove").boolValue = !property.FindPropertyRelative("hasMove").boolValue;
                }
                autoI++;
            }
            if (!property.FindPropertyRelative("hasScale").boolValue)
            {
                if (GUI.Button(rects3[autoI], "Scale"))
                {
                    property.FindPropertyRelative("hasScale").boolValue = !property.FindPropertyRelative("hasScale").boolValue;
                }
                autoI++;
            }
            if (!property.FindPropertyRelative("hasRotate").boolValue)
            {
                if (GUI.Button(rects3[autoI], "Rotate"))
                {
                    property.FindPropertyRelative("hasRotate").boolValue = !property.FindPropertyRelative("hasRotate").boolValue;
                }
                autoI++;
            }
            if (!property.FindPropertyRelative("hasSize").boolValue)
            {
                if (GUI.Button(rects3[autoI], "Size"))
                {
                    property.FindPropertyRelative("hasSize").boolValue = !property.FindPropertyRelative("hasSize").boolValue;
                }
                autoI++;
            }
            if (!property.FindPropertyRelative("hasAlpha").boolValue)
            {
                if (GUI.Button(rects3[autoI], "Alpha"))
                {
                    property.FindPropertyRelative("hasAlpha").boolValue = !property.FindPropertyRelative("hasAlpha").boolValue;
                }
                autoI++;
            }

            if (property.FindPropertyRelative("hasMove").boolValue)
            {
                GUI.color = darkerGray;
                GUI.Box(rects4[autoI], "");
                GUI.color = Color.white;

                GUI.color = Color.red;
                if (GUI.Button(rects2[autoI], "X"))
                {
                    property.FindPropertyRelative("hasMove").boolValue = !property.FindPropertyRelative("hasMove").boolValue;
                }
                GUI.color = Color.white;
                GUI.Box(rects[autoI], "Move"); autoI++;
                EditorGUI.PropertyField(rects[autoI], property.FindPropertyRelative("moveCurve"), new GUIContent("Move Curve")); autoI++;
                EditorGUI.PropertyField(rects[autoI], property.FindPropertyRelative("move"), new GUIContent("Move")); autoI++;
            }

            //Scale
                
            if (property.FindPropertyRelative("hasScale").boolValue)
            {
                GUI.color = darkerGray;
                GUI.Box(rects4[autoI], "");
                GUI.color = Color.white;

                GUI.color = Color.red;
                if (GUI.Button(rects2[autoI], "X"))
                {
                    property.FindPropertyRelative("hasScale").boolValue = !property.FindPropertyRelative("hasScale").boolValue;
                }
                GUI.color = Color.white;
                GUI.Box(rects[autoI], "Scale"); autoI++;
                EditorGUI.PropertyField(rects[autoI], property.FindPropertyRelative("scaleCurve"), new GUIContent("Scale Curve")); autoI++;
                EditorGUI.PropertyField(rects[autoI], property.FindPropertyRelative("scale"), new GUIContent("Scale")); autoI++;
            }

            //Rotate
                
            if (property.FindPropertyRelative("hasRotate").boolValue)
            {
                GUI.color = darkerGray;
                GUI.Box(rects4[autoI], "");
                GUI.color = Color.white;

                GUI.color = Color.red;
                if (GUI.Button(rects2[autoI], "X"))
                {
                    property.FindPropertyRelative("hasRotate").boolValue = !property.FindPropertyRelative("hasRotate").boolValue;
                }
                GUI.color = Color.white;
                GUI.Box(rects[autoI], "Rotate"); autoI++;
                EditorGUI.PropertyField(rects[autoI], property.FindPropertyRelative("rotateCurve"), new GUIContent("Rotate Curve")); autoI++;
                EditorGUI.PropertyField(rects[autoI], property.FindPropertyRelative("rotate"), new GUIContent("Rotate")); autoI++;
            }

            //Size
            if (property.FindPropertyRelative("hasSize").boolValue)
            {
                GUI.color = darkerGray;
                GUI.Box(rects4[autoI], "");
                GUI.color = Color.white;
                GUI.color = Color.red;
                if (GUI.Button(rects2[autoI], "X"))
                {
                    property.FindPropertyRelative("hasSize").boolValue = !property.FindPropertyRelative("hasSize").boolValue;
                }
                GUI.color = Color.white;
                GUI.Box(rects[autoI], "Size"); autoI++;
                EditorGUI.PropertyField(rects[autoI], property.FindPropertyRelative("sizeCurve"), new GUIContent("Size Curve")); autoI++;
                EditorGUI.PropertyField(rects[autoI], property.FindPropertyRelative("size"), new GUIContent("Size")); autoI++;
            }

            //Alpha

            if (property.FindPropertyRelative("hasAlpha").boolValue)
            {
                GUI.color = darkerGray;
                GUI.Box(rects4[autoI], "");
                GUI.color = Color.white;

                GUI.color = Color.red;
                if (GUI.Button(rects2[autoI], "X"))
                {
                    property.FindPropertyRelative("hasAlpha").boolValue = !property.FindPropertyRelative("hasAlpha").boolValue;
                }
                GUI.color = Color.white;
                GUI.Box(rects[autoI], "Alpha"); autoI++;
                EditorGUI.PropertyField(rects[autoI], property.FindPropertyRelative("alphaCurve"), new GUIContent("Alpha Curve")); autoI++;
                autoI++;
            }
            EditorGUI.PropertyField(rects3[autoI], property.FindPropertyRelative("pivot"), new GUIContent("Pivot")); autoI++;
            dynaRow = autoI;
            
            //base.OnGUI(position, property, label);
            //GUILayout.Box("", GUILayout.ExpandWidth(true));
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return (cellHeight * dynaRow) + (padding.y * 2);
        }
    }
}