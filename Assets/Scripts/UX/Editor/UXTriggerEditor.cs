﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UX;

namespace UX
{
    [CustomEditor(typeof(UXTrigger))]
    public class UXTriggerEditor : Editor
    {
        public SerializedProperty parent;
        public string id = "";

        public void OnEnable()
        {

            ((UXTrigger)target).Setup();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            var component = target as UXTrigger;
            if (!EditorApplication.isPlaying)
            {
                if (!serializedObject.isEditingMultipleObjects)
                {
                    id = component.ID;
                    id = EditorGUILayout.TextField("ID", id);
                    component.ID = id;
                    if (!component.ID.Contains(component.gameObject.scene.name + "/"))
                    {
                        component.ID = component.gameObject.scene.name + "/" + id;
                    }
                }
            }
            else
            {
                EditorGUILayout.LabelField("ID", component.ID);
            }

            if (component.GetOpenTriggerList() != null && component.GetOpenTriggerList().Count > 0)
            {
                GUILayout.Label("Open trigger");
                GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));
                int openIndex = -1;
                for (int i = 0; i < component.GetOpenTriggerList().Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label((i + 1).ToString() + ".", GUILayout.Width(20));
                    if (GUILayout.Button(component.GetOpenTriggerList()[i])) 
                    {
                        UX.ShowUXComponentReference(component.GetOpenTriggerList()[i]);
                    }
                    GUI.color = Color.red;
                    if (GUILayout.Button("x", GUILayout.Width(30)))
                    {
                        openIndex = i;
                    }
                    GUI.color = Color.white;
                    GUILayout.EndHorizontal();
                }
                if (openIndex != -1)
                {
                    component.OpenTriggerForceRemove(openIndex);
                }
            }
            if (component.GetCloseTriggerList() != null && component.GetCloseTriggerList().Count > 0)
            {
                GUILayout.Label("Close trigger");
                GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));
                int closeIndex = -1;
                for (int i = 0; i < component.GetCloseTriggerList().Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label((i + 1).ToString() + ".", GUILayout.Width(20));

                    if (GUILayout.Button(component.GetCloseTriggerList()[i]))
                    {
                        UX.ShowUXComponentReference(component.GetCloseTriggerList()[i]);
                    }
                    GUI.color = Color.red;
                    if (GUILayout.Button("x", GUILayout.Width(30)))
                    {
                        
                        closeIndex = i;
                        
                    }
                    GUI.color = Color.white;
                    GUILayout.EndHorizontal();
                }
                if(closeIndex != -1)
                {
                    component.CloseTriggerForceRemove(closeIndex);
                }
            }
            if (component.GetToggleTriggerList() != null && component.GetToggleTriggerList().Count > 0)
            {
                GUILayout.Label("Toggle trigger");
                GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(2));
                int toggleIndex = -1;
                for (int i = 0; i < component.GetToggleTriggerList().Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label((i + 1).ToString() + ".", GUILayout.Width(20));

                    if (GUILayout.Button(component.GetToggleTriggerList()[i]))
                    {
                        UX.ShowUXComponentReference(component.GetToggleTriggerList()[i]);
                    }
                    GUI.color = Color.red;
                    if (GUILayout.Button("x", GUILayout.Width(30)))
                    {
                        toggleIndex = i;
                    }
                    GUI.color = Color.white;
                    GUILayout.EndHorizontal();
                }
                if (toggleIndex != -1)
                {
                    component.ToggleTriggerForceRemove(toggleIndex);
                }
            }
            serializedObject.ApplyModifiedProperties();
            Undo.RecordObject(component, "trigger index");
        }

        public void UpdateTrigger()
        {
            serializedObject.ApplyModifiedProperties();
            Undo.RecordObject(target, "trigger list");
        }
    }
}