﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UX
{
    [CustomPropertyDrawer(typeof(UX.AnimConfig))]
    public class UXAnimConfigEditor : PropertyDrawer
    {
        int rectCount = 12;
        float cellHeight = 20;
        Vector2 padding = new Vector2(10, 10);

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Rect Box = new Rect(position.x + padding.x, position.y + padding.y, position.width - (padding.x * 2), cellHeight);
            Rect[] rects = new Rect[rectCount];
            for(int i = 0; i < rects.Length; i++)
            {
                rects[i] = new Rect(Box.x, Box.y + cellHeight * i, Box.width, cellHeight);
            }
            int j = 1;
            GUI.color = Color.gray;
            GUI.Box(new Rect(position.x, position.y, position.width, position.height),"");
            GUI.color = Color.white;
            EditorGUI.Slider(rects[0], property.FindPropertyRelative("duration"), 0, 5, "Duration");
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("scale"), new GUIContent("Scale") ); j++;
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("move"), new GUIContent("Move")); j++;
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("rotate"), new GUIContent("Rotate")); j++;
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("pivot"), new GUIContent("Pivot")); j++;
            EditorGUI.Slider(rects[j], property.FindPropertyRelative("alpha"), 0, 1, "Alpha"); j++;
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("oneWay"), new GUIContent("One Way")); j++;
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("alwaysOn"), new GUIContent("Always On")); j++;
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("mirror"), new GUIContent("Mirror")); j++;
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("beginActive"), new GUIContent("Begin Active")); j++;
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("persistent"), new GUIContent("Persistent")); j++;
            EditorGUI.PropertyField(rects[j], property.FindPropertyRelative("curve"), new GUIContent("Curve")); j++;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return (cellHeight * rectCount) + (padding.y * 2);
        }
    }
}