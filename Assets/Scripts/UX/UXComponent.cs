﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UX;

namespace UX
{
    [System.Serializable]
    public class UXComponent : MonoBehaviour
    {
        public Coroutine coroutine;
        [SerializeField, HideInInspector, Tooltip("The unique ID of the component")]
        public string ID;
        [SerializeField, HideInInspector]
        public UX.UXObject uxObject;
        private bool firstTrigger = true;
        [SerializeField, HideInInspector, Tooltip("Select the trigger type for the component")]
        public TriggerType triggerType;
        public TriggerType typeTracker;
        [SerializeField, HideInInspector, Tooltip("Remove the scene of this component when it is deactivated")]
        public bool removeScene;
        [SerializeField, HideInInspector, Tooltip("Select animation configuration between the presets done in UXManager or a custom configuration") ]
        public Configuration configuration;
        [SerializeField, HideInInspector]
        public UXParent uxparent;
        [SerializeField, HideInInspector]
        private int index;
        [SerializeField, HideInInspector]
        public int Index
        {
            set
            {
                if (index != value)
                {
                    uxObject = new UX.UXObject((RectTransform)transform, UXManager.GetConfiguration(value), uxObject);
                }
                index = value;
            }
            get
            {
                return index;
            }
        }
        [SerializeField]
        public UX.AnimConfig customConfig;
        [SerializeField, HideInInspector]
        //public string scene;

        public enum Configuration
        {
            Preset,
            Custom
        }

        public enum TriggerType
        {
            Normal,
            Toggle,
            Sequence
        }

        [SerializeField, HideInInspector]
        public List<string>
            openTriggerList,
            closeTriggerList,
            toggleTriggerList;

        private void Start()
        {
            Setup();
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.I))
            {

            }
        }

        private void Reset()
        {
            UXParent uxp = GetComponentInParent<UXParent>();
            if(uxp == null)
            {
                Canvas c = GetComponentInParent<Canvas>();
                if (c == null)
                {
                    Debug.LogError("UXComponent must be a child of canvas or enabled", transform);
                }
                else
                {
                    uxp = c.gameObject.AddComponent<UXParent>();
                    uxparent = uxp;
                }
            }
            ID = gameObject.scene.name + "/" + gameObject.name;
            int i = 1;
            checkAgain:
            if(uxp.CheckUXTriggerListID(ID))
            {
                ID = gameObject.scene.name + "/" + gameObject.name + i;
                i++;
                goto checkAgain; //Untested
            }
        }

        public void Setup()
        {
            if (firstTrigger)
            {
                uxObject = new UX.UXObject((RectTransform)transform, SelectConfiguration(), uxObject);
                firstTrigger = false;
                if (string.IsNullOrEmpty(ID))
                {
                    ID = gameObject.scene.name + "/" + gameObject.name;
                }
            }
        }

        public void Off()
        {
            if(uxObject == null)
            {
                Debug.LogError("No uxObject", this);
            }
            else
            {
                uxObject.isDone = false;
                gameObject.SetActive(false);
                uxObject.active = false;
                uxObject.timer = 0;
                uxObject.cg.alpha = 0;
                uxObject.animValue = 0;
                uxObject.curveVal = 0;
            }
        }

        public void On()
        {
            if (uxObject == null)
            {
                Debug.LogError("No uxObject", this);
            }
            else
            {
                uxObject.isDone = false;
                gameObject.SetActive(true);
                uxObject.active = true;
                uxObject.timer = 1;
                uxObject.cg.alpha = 1;
                uxObject.animValue = 1;
            }
        }

        public void AssignListeners()
        {
            switch (triggerType)
            {
                case TriggerType.Normal:
                    for (int i = 0; i < openTriggerList.Count; i++)
                    {
                        if(openTriggerList[i] != null)
                        {
                            UX.AddTriggerListener(openTriggerList[i], delegate { UX.OpenAnimation(ID); });
                        }
                    }

                    for (int i = 0; i < closeTriggerList.Count; i++)
                    {
                        if (closeTriggerList[i] != null)
                        {
                            UX.AddTriggerListener(closeTriggerList[i], delegate { UX.CloseAnimation(ID); });
                        }
                    }
                    break;

                case TriggerType.Toggle:
                    for (int i = 0; i < toggleTriggerList.Count; i++)
                    {
                        if (toggleTriggerList[i] != null)
                        {
                            UX.AddTriggerListener(toggleTriggerList[i], delegate { UX.ToggleAnimation(ID); });
                        }
                    }
                    break;
            }
        }

        private UX.AnimConfig SelectConfiguration()
        {
            switch(configuration)
            {
                case Configuration.Preset:
                    return UXManager.GetConfiguration(Index);
                case Configuration.Custom:
                    return customConfig;
                default:
                    return customConfig;
            }
        }

        public void RemoveTriggersInList(ref List<string> stringList)
        {
            for(int i = 0; i < stringList.Count; i++)
            {
                UX.RemoveUXTriggerComponent(stringList[i]);
            }
            stringList = new List<string>();
        }
    }
}
