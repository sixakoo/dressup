﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LayoutElement))]
public class UXMaxHeight : MonoBehaviour
{
    public LayoutElement layoutElement;
    public RectTransform contentRectTransform;
    public ContentSizeFitter contentSizeFitter;
    public Vector2 maxValue;
    private bool isMaxed;

    private void Reset()
    {
        layoutElement = GetComponent<LayoutElement>();
        HeightCheck();
    }

    private void Update()
    {
        HeightCheck();
    }

    private void HeightCheck()
    {
        if (contentRectTransform.hasChanged)
        {
            if (contentRectTransform.rect.height >= maxValue.y)
            {
                if (!isMaxed)
                {
                    SetMaxHeight();
                }
            }
            else
            {
                if (isMaxed)
                {
                    RemoveMaxHeight();
                }
            }
        }
    }

    private void SetMaxHeight()
    {
        isMaxed = true;
        contentSizeFitter.enabled = true;
        layoutElement.preferredHeight = maxValue.y;
    }

    private void RemoveMaxHeight()
    {
        isMaxed = false;
        contentSizeFitter.enabled = false;
        layoutElement.preferredHeight = -1;
    }
}
