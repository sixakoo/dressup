﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    [System.Serializable, RequireComponent(typeof(Button))]
    public class UXTrigger : MonoBehaviour
    {
        [HideInInspector]
        public Button button;
        [SerializeField, HideInInspector]
        private List<string>
            openTriggerList,
            closeTriggerList,
            toggleTriggerList;
        [SerializeField, HideInInspector]
        public string ID;
        [SerializeField, HideInInspector]
        public UXParent uxParent;

        public void Start()
        {
            SetButton();
            for (int i = 0; i < openTriggerList.Count; i++)
            {
                int k = i;
                button.onClick.AddListener(delegate { UX.OpenAnimation(openTriggerList[k]); });
                //Debug.Log("1", this);
            }
            for (int i = 0; i < closeTriggerList.Count; i++)
            {
                int k = i;
                button.onClick.AddListener(delegate { UX.CloseAnimation(closeTriggerList[k]); });
                //Debug.Log("2", this);
            }
            for (int i = 0; i < toggleTriggerList.Count; i++)
            {
                int k = i;
                button.onClick.AddListener(delegate { UX.ToggleAnimation(toggleTriggerList[k]); });
                //Debug.Log("3", this);
            }
        }

        public void Setup()
        {
            if (string.IsNullOrEmpty(ID))
            {
                ID = gameObject.scene.name + "/" + gameObject.name;
            }
            if (uxParent == null)
            {
                uxParent = GetComponentInParent<UXParent>();
            }

            if (uxParent == null)
            {
                Debug.Log("Get Parent");
                Canvas c = GetComponentInParent<Canvas>();
                if (c == null)
                {
                    Debug.LogError("UXTrigger must be a child of canvas or enabled", transform.parent);
                }
                else
                {
                    if (!c.TryGetComponent(out uxParent))
                    {
                        Debug.LogError("Failed to get parent");
                        uxParent = c.gameObject.AddComponent<UXParent>();
                    }
                    Debug.Log("Assign parent");
                }
            }
            uxParent.GetChildTrigger();
        }

        public void SetButton()
        {
            button = GetComponent<Button>();
        }

#if UNITY_EDITOR
        public void AddObjectToList(UXComponent uxc, UX.AnimationType animType)
        {
            switch (animType)
            {
                case UX.AnimationType.Open:
                    if (openTriggerList == null)
                    {
                        openTriggerList = new List<string>();
                    }
                    if (!openTriggerList.Contains(uxc.ID))
                    {
                        openTriggerList.Add(uxc.ID);
                    }
                    break;
                case UX.AnimationType.Close:
                    if (closeTriggerList == null)
                    {
                        closeTriggerList = new List<string>();
                    }
                    if (!closeTriggerList.Contains(uxc.ID))
                    {
                        closeTriggerList.Add(uxc.ID);
                    }
                    break;
                case UX.AnimationType.Toggle:
                    if (toggleTriggerList == null)
                    {
                        toggleTriggerList = new List<string>();
                    }
                    if (!toggleTriggerList.Contains(uxc.ID))
                    {
                        toggleTriggerList.Add(uxc.ID);
                    }
                    break;
            }
            UnityEditor.Undo.RecordObject(this, "trigger list update");
            UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(this.gameObject.scene);
        }
#endif

        public void RemoveTriggerObject(string id) // might remove soon
        {
            if (openTriggerList != null)
            {
                int index = -1;
                for(int i = 0; i < openTriggerList.Count; i++)
                {
                    if(openTriggerList[i] == id)
                    {
                        index = i;
                    }
                }
                if(index != -1)
                {
                    openTriggerList.RemoveAt(index);
                }
            }
            if (openTriggerList.Count < 1)
            {
                DestroyImmediate(this);
            }
        }

        public void OpenTriggerForceRemove(int index)
        {
            if (openTriggerList != null)
            {
                openTriggerList.RemoveAt(index);
            }
        }

        public void CloseTriggerForceRemove(int index)
        {
            if (closeTriggerList != null)
            {
                closeTriggerList.RemoveAt(index);
            }
        }

        public void ToggleTriggerForceRemove(int index)
        {
            if (toggleTriggerList != null)
            {
                toggleTriggerList.RemoveAt(index);
            }
        }

        public List<string> GetOpenTriggerList()
        {
            if(openTriggerList != null)
            {
                return new List<string>(openTriggerList);
            }
            else
            {
                return null;
            }
        }

        public List<string> GetCloseTriggerList()
        {
            if (closeTriggerList != null)
            {
                return new List<string>(closeTriggerList);
            }
            else
            {
                return null;
            }
        }

        public List<string> GetToggleTriggerList()
        {
            if (toggleTriggerList != null)
            {
                return new List<string>(toggleTriggerList);
            }
            else
            {
                return null;
            }
        }

        public void RemoveEntry(string id, bool close)
        {
            if(close)
            {
                if (closeTriggerList != null && closeTriggerList.Contains(id))
                {
                    if (closeTriggerList.Contains(id))
                    {
                        closeTriggerList.Remove(id);
                        Debug.Log("removed trigger : " + id);
                    }
                    else
                    {
                        Debug.Log("Did not find uxcomponent id : " + id);
                    }
                }
            }
            else
            {
                if (openTriggerList != null && openTriggerList.Contains(id))
                {
                    if (openTriggerList.Contains(id))
                    {
                        openTriggerList.Remove(id);
                        Debug.Log("removed trigger : " + id);
                    }
                    else
                    {
                        Debug.Log("Did not find uxcomponent id : " + id);
                    }
                }
            }
            CheckEmpty();
        }

        public void CheckEmpty()
        {
            int strike = 0;
            if(openTriggerList == null )
            {
                strike += 1;
            }
            else if(openTriggerList.Count < 1)
            {
                strike += 1;
            }
            if (closeTriggerList == null)
            {
                strike += 1;
            }
            else if (closeTriggerList.Count < 1)
            {
                strike += 1;
            }
            if(strike >= 2)
            {
                DestroyImmediate(this);
            }
        }
    }
}