﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UX
{
    public class UXSwipe : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public Direction direction;
        [HideInInspector]
        public int pageCount = 2;
        public RectTransform
            viewport,
            content,
            swipeArea;
        public float spacing = 0;
        public float triggerThreshold = 5f;
        public float nextPageTriggerVelocity = 5f;
        [Range(0,1)]
        public float nextPageTriggerLength;
        [Range(0, 2)]
        public float dragScaling = 1;
        public AnimationCurve moveCurve = AnimationCurve.Linear(0, 0, 1, 1);
        public float moveDuration = 1;
        public float maxMove = -1f;
        public float lastPageLimit = 500f;
        public float inertiaIntesity = 2f;
        public AnimationCurve inertiaCurve;
        public Vector2 maxSpeed;
        public int pageIndex;
        public bool enableSwiping = true;
        public bool useIDragHandler = true;
        //[HideInInspector]
        public bool isMoving = false;
        private Vector2 currentPos;
        private Vector2
            initialPoint,
            offset,
            initialPos;
        private Vector2
            velocity,
            dragLength;
        private Coroutine
            inertiaCo,
            moveCo;
        public PageChange onPageChange;
        public delegate void PageChange(int changeTo);
        public bool isDrag = false;
        public float blockThreshold = 0.5f;
        private Canvas canvas;
        private bool correctDirection, directionCheck, lockSwipe;
        private float thresholdOffset;
        private float tempMax;
        private float cellSize;

        public bool ForceDisable = false;

        public enum Direction // Update all function to include vertical direction
        {
            Horizontal,
            Vertical
        }

        private void Awake()
        {
            tempMax = maxMove;
        }

        private void Start()
        {
            UpdateContentSize();
            initialPos = content.anchoredPosition;
            if(direction == Direction.Horizontal)
            {
                cellSize = viewport.rect.width;
            }
            else
            {
                cellSize = viewport.rect.height;
            }
            canvas = GetComponentInParent<Canvas>();
        }

        public void UpdateContentSize()
        {
            //Debug.LogError("Update content size");
            pageCount = content.childCount;
            content.anchorMax = Vector2.zero;
            content.anchorMin = Vector2.zero;
            content.anchoredPosition = Vector2.zero;
            content.pivot = Vector2.zero;

            if (direction == Direction.Horizontal)
            {
                content.sizeDelta = new Vector2((viewport.rect.width * pageCount) + (spacing * (pageCount - 1)), viewport.rect.height);
                int i = 0;
                foreach (Transform t in content)
                {
                    if (t != content.transform)
                    {
                        RectTransform rt = (RectTransform)t.transform;
                        rt.sizeDelta = new Vector2(viewport.rect.width, viewport.rect.height);
                        //rt.anchorMin = Vector2.zero;
                        //rt.anchorMax = Vector2.zero;
                        //rt.pivot = Vector2.zero;
                        Vector2 offset = rt.sizeDelta * rt.pivot;
                        rt.anchoredPosition = new Vector2((viewport.rect.width * i + ((i == 0)? 0  : spacing * i)) + offset.x, 0 + offset.y);
                        i++;
                    }
                }
            }
            else
            {
                content.sizeDelta = new Vector2(viewport.rect.width, (viewport.rect.height * pageCount) + (spacing * (pageCount - 1)));
                int i = 0;
                foreach (Transform t in content)
                {
                    if (t != content.transform)
                    {
                        RectTransform rt = (RectTransform)t.transform;
                        rt.sizeDelta = new Vector2(viewport.rect.width, viewport.rect.height);
                        //rt.anchorMin = Vector2.zero;
                        //rt.anchorMax = Vector2.zero;
                        //rt.pivot = Vector2.zero;
                        Vector2 offset = rt.sizeDelta * rt.pivot;
                        rt.anchoredPosition = new Vector2(0 + offset.x, (viewport.rect.height * i) + ((i == 0) ? 0 : spacing * i) + offset.y);
                        i++;
                    }
                }
            }
        }

        private void Update()
        {
            if (!ForceDisable)
            {
                if (!useIDragHandler)
                {
                    ManualSwipe();
                }
                BoundCheck();
            }

            if (UXManager.lastSize.x != Screen.width || UXManager.lastSize.y != Screen.height)
            {
                UpdateContentSize();
                UXManager.lastSize = new Vector2(Screen.width, Screen.height);
            }
        }

        private void BoundCheck()
        {
            if(direction == Direction.Horizontal)
            {
                if (lastPageLimit != -1)
                {
                    if (content.anchoredPosition.x > 0 || content.anchoredPosition.x < (content.rect.width - viewport.rect.width) * -1)
                    {
                        maxMove = lastPageLimit;
                        if (content.anchoredPosition.x > 0 + lastPageLimit)
                        {
                            content.anchoredPosition = new Vector2(0, content.anchoredPosition.y);
                        }
                        if (content.anchoredPosition.x < ((content.rect.width - viewport.rect.width) * -1) - lastPageLimit)
                        {
                            content.anchoredPosition = new Vector2((content.rect.width - viewport.rect.width) * -1, content.anchoredPosition.y);
                        }
                    }
                    else
                    {
                        maxMove = tempMax;
                    }
                }
            }
            else
            {
                if (lastPageLimit != -1)
                {
                    if (content.anchoredPosition.y > 0 || content.anchoredPosition.y < (content.rect.width - viewport.rect.width) * -1)
                    {
                        maxMove = lastPageLimit;
                        if (content.anchoredPosition.y > 0 + lastPageLimit)
                        {
                            content.anchoredPosition = new Vector2(content.anchoredPosition.x, 0);
                        }
                        if (content.anchoredPosition.y < ((content.rect.height - viewport.rect.height) * -1) - lastPageLimit)
                        {
                            content.anchoredPosition = new Vector2(content.anchoredPosition.x, (content.rect.height - viewport.rect.height) * -1);
                        }
                    }
                    else
                    {
                        maxMove = tempMax;
                    }
                }
            }
        }

        public void OnBeginDrag(PointerEventData data)
        {
            if(useIDragHandler)
            {
                BeginSwipe(data.position);
            }
        }

        public void OnDrag(PointerEventData data)
        {
            if(useIDragHandler)
            {
                Swipe(data.position /** dragScaling*/);
            }
        }

        public void OnEndDrag(PointerEventData data)
        {
            if(useIDragHandler)
            {
                EndSwipe(data.position);
            }
        }

        private void BeginSwipe(Vector2 pointer)
        {
            if(enableSwiping && RectTransformUtility.RectangleContainsScreenPoint(swipeArea, UXManager.GetInputPosition()))
            {
                initialPoint = pointer;
                offset = (Vector2)content.anchoredPosition - pointer;
                directionCheck = true;
                if (moveCo != null)
                {
                    UXManager.StopCo(moveCo);
                }
                isMoving = false;
            }
        }

        private void Swipe(Vector2 pointer)
        {
            if (enableSwiping)
            {
                SwipeDirection(pointer);
                Vector2 lastPos = currentPos;
                currentPos = pointer;
                velocity = currentPos - lastPos;
                switch (direction)
                {
                    case Direction.Horizontal:
                        if (correctDirection)
                        {
                            if (maxMove > 0)
                            {
                                if (Mathf.Abs(pointer.x - initialPoint.x) < maxMove)
                                {
                                    content.anchoredPosition = new Vector2(pointer.x + offset.x - thresholdOffset, content.anchoredPosition.y);
                                }
                                else
                                {
                                    content.anchoredPosition = new Vector2(content.anchoredPosition.x, content.anchoredPosition.y);
                                }
                            }
                            else
                            {
                                content.anchoredPosition = new Vector2(pointer.x + offset.x - thresholdOffset, content.anchoredPosition.y);
                            }
                            isDrag = true;
                        }
                        break;
                    case Direction.Vertical: //Updated this later
                        if (correctDirection)
                        {
                            if (maxMove > 0)
                            {
                                if (Mathf.Abs(pointer.y - initialPoint.y) < maxMove)
                                {
                                    content.anchoredPosition = new Vector2(content.anchoredPosition.x, pointer.y + offset.y - thresholdOffset);
                                }
                                else
                                {
                                    content.anchoredPosition = new Vector2(content.anchoredPosition.x, content.anchoredPosition.y);
                                }
                            }
                            else
                            {
                                content.anchoredPosition = new Vector2(content.anchoredPosition.x, pointer.y + offset.y - thresholdOffset);
                            }
                            isDrag = true;
                        }
                        break;
                }
            }
        }

        private void EndSwipe(Vector2 pointer)
        {
            if(enableSwiping)
            {
                directionCheck = false;
                dragLength = pointer - initialPoint;
                //RunInertia();
                SwipeCheck();
                isDrag = false;
                correctDirection = false;
                lockSwipe = false;
                if (!RestCheck())
                {
                    GoToPage(pageIndex);
                }
            }
        }

        private void SwipeDirection(Vector2 pointer)
        {
            if (directionCheck == true)
            {
                if (direction == Direction.Horizontal)
                {
                    if (Mathf.Abs(pointer.x - initialPoint.x) > triggerThreshold && (Mathf.Abs(pointer.y - initialPoint.y) < triggerThreshold))
                    {
                        thresholdOffset = pointer.x - initialPoint.x;
                        if(!lockSwipe)
                        {
                            correctDirection = true;
                        }
                        directionCheck = false;
                        if (moveCo != null)
                        {
                            UXManager.StopCo(moveCo);
                        }
                        isMoving = false;
                    }
                    else if(Mathf.Abs(pointer.y - initialPoint.y) > triggerThreshold && (Mathf.Abs(pointer.x - initialPoint.x) < triggerThreshold))
                    {
                        lockSwipe = true;
                    }
                }
                else
                {
                    if (Mathf.Abs(pointer.y - initialPoint.y) > triggerThreshold && (Mathf.Abs(pointer.x - initialPoint.x) < triggerThreshold))
                    {
                        thresholdOffset = pointer.y - initialPoint.y;
                        if (!lockSwipe)
                        {
                            correctDirection = true;
                        }
                        directionCheck = false;
                        if (moveCo != null)
                        {
                            UXManager.StopCo(moveCo);
                        }
                        isMoving = false;
                    }
                    else if (Mathf.Abs(pointer.x - initialPoint.x) > triggerThreshold && (Mathf.Abs(pointer.y - initialPoint.y) < triggerThreshold))
                    {
                        lockSwipe = true;
                    }
                }
            }
        }

        public void ManualSwipe()
        {
            if (UXManager.GetClickDown())
            {
                BeginSwipe(RelativeMousePosition());
            }
            if (UXManager.GetClick())
            {
                Swipe(RelativeMousePosition());
            }
            if (UXManager.GetClickUp())
            {
                EndSwipe(RelativeMousePosition());
            }
            //if (Input.GetMouseButtonDown(0))
            //{
            //    BeginSwipe(RelativeMousePosition());
            //}
            //if (Input.GetMouseButton(0))
            //{
            //    Swipe(RelativeMousePosition());
            //}
            //if (Input.GetMouseButtonUp(0))
            //{
            //    EndSwipe(RelativeMousePosition());
            //}
        }

        public Vector2 RelativeMousePosition()
        {
            return (UXManager.GetInputPosition() / canvas.scaleFactor) * dragScaling;
        }

        public void RunInertia()
        {
            if (inertiaCo != null)
            {
                StopCoroutine(inertiaCo);
            }
            inertiaCo = StartCoroutine(SimulateInertia());
        }

        private IEnumerator SimulateInertia()
        {
            float duration = Mathf.Abs(velocity.x / maxSpeed.x);
            float val = 0;
            if (direction == Direction.Horizontal)
            {
                while (val < 1)
                {
                    val += Time.deltaTime / duration;
                    if (velocity.x / maxSpeed.x > 0)
                    {
                        transform.Translate(Vector2.right * inertiaCurve.Evaluate(val) * inertiaIntesity);
                    }
                    else
                    {
                        transform.Translate(Vector2.left * inertiaCurve.Evaluate(val) * inertiaIntesity);
                    }
                    yield return null;
                }
            }
            else
            {
                while (val < 1)
                {
                    val += Time.deltaTime / duration;
                    if (velocity.y / maxSpeed.y > 0)
                    {
                        transform.Translate(Vector2.up * inertiaCurve.Evaluate(val) * inertiaIntesity);
                    }
                    else
                    {
                        transform.Translate(Vector2.down* inertiaCurve.Evaluate(val) * inertiaIntesity);
                    }
                    yield return null;
                }
            }
        }

        public void GoToPage(int index)
        {
            if (moveCo != null)
            {
                UXManager.StopCo(moveCo);
            }
            moveCo = UXManager.RunCoroutine(MovePage(index));
            pageIndex = index;
        }

        public void SetToPage(int index)
        {
            pageIndex = index;
            if(direction == Direction.Horizontal)
            {
                content.anchoredPosition = Vector2.right * ((-cellSize * index) - ((spacing == 0) ? 0 : spacing * index));
            }
            else
            {
                content.anchoredPosition = Vector2.up * ((-cellSize * index) - ((spacing == 0) ? 0 : spacing * index));
            }
        }

        private IEnumerator MovePage(int index)
        {
            isMoving = true;
            float val = 0;
            if(content == null)
            {
                yield break;
            }
            Vector2 startPoint = content.anchoredPosition;
            Vector2 targetVector;
            if (direction == Direction.Horizontal)
            {
                targetVector = Vector2.right * ((-cellSize * index) - ((spacing == 0) ? 0 : spacing * index));
            }
            else
            {
                targetVector = Vector2.up * ((-cellSize * index) - ((spacing == 0) ? 0 : spacing * index));
            }
            Vector2 Diff = targetVector - startPoint;
            while (val < 1)
            {
                if(this == null)
                {
                    break;
                }
                val += Time.deltaTime / moveDuration;
                float animVal = moveCurve.Evaluate(val);
                content.anchoredPosition = Move(Diff, animVal, startPoint);
                if(isMoving && val > blockThreshold)
                {
                    isMoving = false;
                }
                yield return null;
            }
        }

        private Vector2 Move(Vector2 move, float x, Vector2 origin)
        {
            return new Vector2(move.x * x + origin.x, move.y * x + origin.y);
        }

        private bool RestCheck()
        {
            Vector2 targetVector;
            if (direction == Direction.Horizontal)
            {
                targetVector = Vector2.right * -cellSize * (pageIndex);
                if (Mathf.Abs(content.anchoredPosition.x - targetVector.x) < 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                targetVector = Vector2.up * -cellSize * (pageIndex);
                if (Mathf.Abs(content.anchoredPosition.y - targetVector.y) < 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private void SwipeCheck()
        {
            if (direction == Direction.Horizontal)
            {
                if (Mathf.Abs(velocity.x) > nextPageTriggerVelocity && correctDirection)
                {
                    if(velocity.x < 0)
                    {
                        if(pageIndex < pageCount - 1)
                        {
                            pageIndex++;
                            onPageChange?.Invoke(pageIndex);
                        }
                    }
                    else
                    {
                        if(pageIndex > 0)
                        {
                            pageIndex--;
                            onPageChange?.Invoke(pageIndex);
                        }
                    }
                    
                }
                else if(Mathf.Abs(dragLength.x / dragScaling) > nextPageTriggerLength * cellSize && correctDirection)
                {
                    if (dragLength.x < 0)
                    {
                        if (pageIndex < pageCount - 1)
                        {
                            pageIndex++;
                            onPageChange?.Invoke(pageIndex);
                        }
                    }
                    else
                    {
                        if (pageIndex > 0)
                        {
                            pageIndex--;
                            onPageChange?.Invoke(pageIndex);
                        }
                    }
                }
                if(isDrag)
                {
                    GoToPage(pageIndex);
                }
            }
            else
            {
                if (Mathf.Abs(velocity.y) > nextPageTriggerVelocity && correctDirection)
                {
                    if (velocity.y < 0)
                    {
                        if (pageIndex < pageCount - 1)
                        {
                            pageIndex++;
                            onPageChange?.Invoke(pageIndex);
                        }
                    }
                    else
                    {
                        if (pageIndex > 0)
                        {
                            pageIndex--;
                            onPageChange?.Invoke(pageIndex);
                        }
                    }
                }
                else if (Mathf.Abs(dragLength.y / dragScaling) > nextPageTriggerLength * cellSize && correctDirection)
                {
                    if (dragLength.y < 0)
                    {
                        if (pageIndex < pageCount - 1)
                        {
                            pageIndex++;
                            onPageChange?.Invoke(pageIndex);
                        }
                    }
                    else
                    {
                        if (pageIndex > 0)
                        {
                            pageIndex--;
                            onPageChange?.Invoke(pageIndex);
                        }
                    }
                }
                if (isDrag)
                {
                    GoToPage(pageIndex);
                }
            }
        }
    }
}