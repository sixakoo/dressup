﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace UX
{
    public class UI_Highlighter : MonoBehaviour
    {
        private List<Button> childrenList;
        public bool autoMove = true;
        public int siblingPosition;
        public float duration = 1;
        public RectTransform highlightPrefab;
        public float deviation = 25f;
        public AnimationCurve curve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        private RectTransform highlighter;
        private float animVal;
        private RectTransform target;
        private Vector2
            initialPos,
            initialAnchorMax,
            initialAnchorMin,
            initialPivot,
            initialSize;
        private float timer;
        public int index { private set; get; }

        private Vector2 targetPos;

        private void Start()
        {
            index = -1;
            GetChildren();
            if (highlightPrefab != null)
            {
                InstantiateHighlight();
            }
            else
            {
                AssignNormalHighlight();
            }
        }

        private void Update()
        {
            if (target != null)
            {
                MoveToTargetAnimation(target);
            }
        }

        private void GetChildren()
        {
            childrenList = new List<Button>();
            foreach (Transform t in transform)
            {
                if (t.TryGetComponent(out Button b))
                {
                    childrenList.Add(b);
                    if (highlightPrefab != null)
                    {
                        if (autoMove)
                        {
                            b.onClick.AddListener(delegate { MoveToTarget(b.transform as RectTransform); });
                        }
                    }
                }
            }
        }

        private void InstantiateHighlight()
        {
            highlighter = (RectTransform)Instantiate(highlightPrefab, transform);
            //highlighter.SetParent(transform, false);
            if (childrenList.Count > 0)
            {
                MoveTo(0);
            }
        }

        private void MoveToTarget(RectTransform _target)
        {
            target = _target;
            highlighter.SetSiblingIndex(siblingPosition);
            initialPos = highlighter.anchoredPosition;
            initialAnchorMax = highlighter.anchorMax;
            initialAnchorMin = highlighter.anchorMin;
            initialPivot = highlighter.pivot;
            initialSize = highlighter.sizeDelta;
            timer = 0f;
            animVal = 0;
            targetPos = new Vector2(Random.Range(_target.anchoredPosition.x - deviation, _target.anchoredPosition.x + (deviation * 2)), Random.Range(_target.anchoredPosition.y - deviation, _target.anchoredPosition.y + (deviation * 2)));
        }

        private void MoveToTargetAnimation(RectTransform _target)
        {
            timer += Time.deltaTime / duration;
            animVal = Mathf.Lerp(0, 1, curve.Evaluate(timer));
            highlighter.anchorMax = initialAnchorMax + ((_target.anchorMax - initialAnchorMax) * animVal);
            highlighter.anchorMin = initialAnchorMin + ((_target.anchorMin - initialAnchorMin) * animVal);
            highlighter.pivot = initialPivot + ((_target.pivot - initialPivot) * animVal);
            highlighter.anchoredPosition = initialPos + ((targetPos - initialPos) * animVal);
            highlighter.sizeDelta = initialSize + ((_target.sizeDelta - initialSize) * animVal);

            if (timer >= 1)
            {
                target = null;
            }
        }

        public void MoveTo(int _index)
        {
            MoveToTarget((RectTransform)childrenList[_index].transform);
        }

        public void Refresh()
        {
            GetChildren();
            AssignNormalHighlight();
        }

        public void SetIndex(int _index)
        {
            SetColor(_index);
        }

        private void AssignNormalHighlight()
        {
            for (int i = 0; i < childrenList.Count; i++)
            {
                int temp = i;
                childrenList[i].onClick.RemoveListener(delegate { SetColor(temp); });
                childrenList[i].onClick.AddListener(delegate { SetColor(temp); });
            }
        }

        private void SetColor(int _index)
        {
            index = _index;
            if (childrenList != null)
            {
                for (int i = 0; i < childrenList.Count; i++)
                {
                    if (childrenList[i].targetGraphic != null)
                    {
                        if (i == index)
                        {
                            childrenList[i].targetGraphic.color = Color.red; // Update later
                        }
                        else
                        {
                            childrenList[i].targetGraphic.color = Color.white;
                        }
                    }
                }
            }
            else
            {
                Refresh();
                SetColor(0);
            }
        }

        private void RefreshColor()
        {
            for (int i = 0; i < childrenList.Count; i++)
            {
                if (childrenList[i].targetGraphic != null)
                {
                    if (i == index)
                    {
                        childrenList[i].targetGraphic.color = Color.red;
                    }
                    else
                    {
                        childrenList[i].targetGraphic.color = Color.white;
                    }
                }
            }
        }
    }
}