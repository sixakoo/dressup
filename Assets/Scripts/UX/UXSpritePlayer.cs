﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UX
{
    [RequireComponent(typeof(RectMask2D))]
    public class UXSpritePlayer : MonoBehaviour
    {
        public Image image;
        public RectMask2D mask;
        public Vector2 grid;
        public int totalFrames;
        public float fps;
        public bool preserveAspect;
        public bool loop;
        public bool playOnStart;
        private Coroutine playerCo;
        public RectTransform spriteTransform;
        public const string childName = "Sprite";
        [Range(0, 5)]
        public float
            loopInterval,
            playOnStartDelay;
        [HideInInspector]
        public bool isPlaying;

        public void SetupImage()
        {
            if(image.sprite == null)
            {
                Debug.LogWarning("No Sprite");
                return;
            }
            //mask.GetComponent<Image>().sprite = null;
            image.rectTransform.pivot = Vector2.up;
            image.rectTransform.anchorMin = Vector2.up;
            image.rectTransform.anchorMax = Vector2.up;
            image.rectTransform.anchoredPosition = Vector2.zero;
            image.preserveAspect = preserveAspect;
            if (preserveAspect)
            {
                float aspect = (image.sprite.rect.width / grid.x) / (image.sprite.rect.height / grid.y);
                mask.rectTransform.sizeDelta = new Vector2(mask.rectTransform.rect.width, mask.rectTransform.rect.width / aspect );
            }

            if (grid.x < 1 || grid.y < 1)
            {
                image.rectTransform.sizeDelta = new Vector2(mask.rectTransform.rect.width, mask.rectTransform.rect.height);
            }
            else
            {
                image.rectTransform.sizeDelta = new Vector2(mask.rectTransform.rect.width * grid.x, mask.rectTransform.rect.height * grid.y);
            }
            //mask.GetComponent<Image>().color = new Color(0, 0, 0, 1);
            //mask.showMaskGraphic = false;
        }

        public void OnEnable()
        {
            if(playOnStart)
            {
                UXManager.RunCoroutine(PlayOnStartEnumerator());
            }
        }

        public IEnumerator PlayOnStartEnumerator()
        {
            yield return new WaitForSeconds(playOnStartDelay);
            Play();
        }

        public void OnDestroy()
        {
            Stop();
        }

        public void Update()
        {
            //Testing
            //if(Input.GetKeyDown(KeyCode.Space))
            //{
            //    Play();
            //}
        }

        public void AssignComponents()
        {
            bool found = false;
            foreach (Transform t in transform)
            {
                if (t != transform)
                {
                    if(t.name == childName)
                    {
                        spriteTransform = t as RectTransform;
                        found = true;
                        break;
                    }
                }
            }
            if(!found)
            {
                spriteTransform = new GameObject(childName, typeof(RectTransform)).GetComponent<RectTransform>() ;
                spriteTransform.SetParent(transform, false);
                
                image = spriteTransform.gameObject.AddComponent<Image>();

            }
            if(mask == null)
            {
                mask = GetComponent<RectMask2D>();
            }
            SetupImage();
        }

        public void DestroyChild()
        {
            foreach (Transform t in transform)
            {
                if (t != transform)
                {
                    Destroy(t);
                }
            }
        }

        public void Play()
        {
            Play(0, totalFrames);
        }

        public void Stop()
        {
            if (playerCo != null)
            {
                UXManager.StopCo(playerCo);
                UX.externalAnimList.Remove(this);
            }
            UX.externalAnimList.Remove(this);
            isPlaying = false;
        }

        public void OnDisable()
        {
            UX.externalAnimList.Remove(this);
            isPlaying = false;
        }

        public void Play(int startFrame, int endFrame)
        {
            if (playerCo != null)
            {
                UXManager.StopCo(playerCo);
            }
            playerCo = UXManager.RunCoroutine(Player(startFrame, endFrame));
        }

        private IEnumerator Player(int startFrame, int endFrame)
        {
            if (this == null)
            {
                yield break;
            }
            if (gameObject.activeSelf == false)
            {
                gameObject.SetActive(true);
            }
            isPlaying = true;
            if (!UX.externalAnimList.Contains(this))
            {
                UX.externalAnimList.Add(this);
            }
            SetupImage();
        repeat:
            float timer = 0;
            float interval = 1 / fps;
            int total = endFrame - startFrame;
            float endTime = interval * (total);
            timer = interval * startFrame;
            float lastval = -1;
            while (timer < endTime)
            {
                timer += Time.deltaTime;
                int i = Mathf.FloorToInt((timer / endTime) * total);
                if(lastval != i)
                {
                    lastval = i;
                }
                if (i < endFrame)
                {
                    image.rectTransform.anchoredPosition = new Vector2((i % grid.x) * mask.rectTransform.rect.width * -1, Mathf.FloorToInt((float)i / grid.x) * mask.rectTransform.rect.height);
                    yield return null;
                }
            }
            if (loop)
            {
                yield return new WaitForSeconds(loopInterval);
                goto repeat;
            }
            UX.externalAnimList.Add(this);
            isPlaying = false;
        }

        public float GetDuration()
        {
            return 1 / fps * totalFrames;
        }

        public void SetToFrame(int i)
        {
            image.rectTransform.anchoredPosition = new Vector2((i % grid.x) * mask.rectTransform.rect.width * -1, Mathf.FloorToInt((float)i / grid.x) * mask.rectTransform.rect.height);
        }

        public void Preview()
        {
            
        }
    }
}