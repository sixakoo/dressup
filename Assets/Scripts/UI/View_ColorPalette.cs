using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
using UX;

public class View_ColorPalette : MonoBehaviour
{
    public float triggerThreshold;
    public UIElement piePrefab;
    public UXAnimator anim;
    public AnimationCurve curve;
    public float animationDuration;
    private Coroutine animCo;
    private bool isOn;
    private Vector3 initialPos;
    private int optionLength;
    private int index;
    private int lastIndex;
    private List<UIElement> pieList;
    public Result ColorResult;
    public delegate void Result(int index);

    public void Update()
    {
        CheckIndex();
        if(Input.GetMouseButtonUp(0) && isOn)
        {
            HideColorPallete();
        }
    }

    private void ClearPallete()
    {
        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }
    }

    public void ShowColor(Color[] colors, Vector3 pos)
    {
        if(colors == null || colors.Length <= 0)
        {
            return;
        }
        pieList = new List<UIElement>();
        lastIndex = -2;
        index = -1;
        initialPos = Input.mousePosition;
        optionLength = colors.Length;
        isOn = true;
        RectTransform rt = ((RectTransform)transform);
        rt.anchoredPosition = (Vector2)pos;
        ClearPallete();
        anim.Play(0);
        float degree = -360f / colors.Length;
        int i = 0;
        foreach (Color color in colors)
        {
            UIElement p = Instantiate(piePrefab, transform, false);
            pieList.Add(p);            
            p.images[0].color = color;
            p.images[0].fillAmount = 0;
            p.images[0].rectTransform.eulerAngles = new Vector3(0, 0, degree * i);
            p.animators[0].SetInitialRotation(new Vector3(0, 0, degree * i));
            i++;
        }
        if(animCo != null)
        {
            StopCoroutine(animCo);
        }        
        animCo = StartCoroutine(AnimatePallete());
    }

    private IEnumerator AnimatePallete()
    {
        float timer = 0;
        float val = 0;
        float frac = 1f / optionLength;
        float pie = 0;
        int pieIndex = 0;
        float pieStart = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime / animationDuration;
            val = curve.Evaluate(timer);
            pie = val - pieStart;
            if (pie >= frac)
            {
                pieList[pieIndex].images[0].fillAmount = frac;
                pieIndex++;
                if(pieIndex >= optionLength)
                {
                    pieIndex = optionLength - 1;
                    break;
                }
                pieStart = frac * pieIndex;
                pie = 0;
            }
            pieList[pieIndex].images[0].fillAmount = pie;
            yield return null;
        }
        pieList[pieIndex].images[0].fillAmount = frac;
    }

    private void HideColorPallete()
    {
        if(index != -1)
        {
            ColorResult?.Invoke(ReturnResult());
        }
        anim.Play(1);
        isOn = false;
    }

    private int GetSelection()
    {
        Vector2 diff = Input.mousePosition - initialPos;
        float rad = Mathf.Atan2(diff.x, diff.y) * Mathf.Rad2Deg;
        float deg = rad < 0 ? rad + 360 : rad;
        float pie = 360 / optionLength;
        for (int i = 0; i < optionLength; i++)
        {
            if(deg < (i + 1) * pie)
            {
                return i;
            }
        }
        return -1;
    }

    public int ReturnResult()
    {
        return index;
    }

    private void CheckIndex()
    {
        if (!isOn || (Vector2.Distance(initialPos, Input.mousePosition) < triggerThreshold) || optionLength <= 0)
        {
            return;
        }
        index = GetSelection();
        if (lastIndex != index)
        {
            if(lastIndex >= 0)
            {
                pieList[lastIndex].animators[0].Play(1);
            }
            if (index > -1)
            {
                lastIndex = index;
                pieList[index].animators[0].Play(0);
            }
        }
    }
}
