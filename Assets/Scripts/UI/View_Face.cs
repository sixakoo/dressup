using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UX;

public class View_Face : MonoBehaviour
{
    [Header("Prefab")]
    public UIElement makeupColorPrefab;
    public UIElement itemPrefab;
    public UIElement iconBtnPrefab;
    public UIElement optionPrefab;
    public Sprite metallicSprite;
    public Sprite smoothness1Sprite;
    public Sprite smoothness2Sprite;

    [Header("Component")]
    public UIElement sliders;
    public UXAnimator anim;
    public ScrollRect mainPanel;
    public ScrollRect subPanel;
    public ScrollRect optionPanel;
    public static View_Face Instance;
    public Character.ColorizeParts highlightedFacePart;
    public AnimationCurve smooth1OpacityCurve, smooth2OpacityCurve;
    public FaceCategory currentCategory;
    public Slider[] slider;

    //variables
    private const string SEPARATOR = "_";
    private const string NONE = "None";
    private const string LAYOUT = "Layout";
    
    private bool isSliderUpdate;
    private bool isLoadSlider;

    public enum FaceCategory
    {
        Eyes,
        Nose,
        Mouth
    }

    private void Start()
    {
        GameMaster.controller.colorPalette.ColorResult += ChangeColor;
        LoadCategory();
        GameMaster.controller.customizationController.character.InitMakeup();
        AssignSliderListeners();
        GameMaster.skeleton.LoadFaceEdits();
    }

    private void Update()
    {
        RaycastFace();
        //if(sliders.gameObject.activeSelf)
        //{
        //    FaceSliderUpdate();
        //}
        if(isSliderUpdate && Input.GetMouseButtonUp(0))
        {
            isSliderUpdate = false;
            GameMaster.user.SaveFaceEdit();
        }
    }

    private void OnEnable()
    {
        if(GameMaster.skeleton)
        {
            GameMaster.skeleton.SetFaceCollidersActive(true);
        }
        mainPanel.horizontalNormalizedPosition = 0;
        subPanel.horizontalNormalizedPosition = 0;
        optionPanel.horizontalNormalizedPosition = 0;
    }

    private void OnDisable()
    {
        if (GameMaster.skeleton)
        {
            GameMaster.skeleton.SetFaceCollidersActive(false);
        }
    }

    private void ClearPanel(Transform panel)
    {
        foreach (Transform t in panel)
        {
            Destroy(t.gameObject);
        }
    }

    public void LoadCategory()
    {
        ClearPanel(optionPanel.content);
        ClearPanel(mainPanel.content);
        foreach (var category in System.Enum.GetValues(typeof(FaceCategory)))
        {
            UIElement uie = Instantiate(iconBtnPrefab, mainPanel.content, false);
            uie.texts[0].gameObject.SetActive(false);
            uie.images[0].sprite = SpriteManager.GetSprite(category.ToString());
            uie.buttons[0].onClick.AddListener(() => LoadParts((FaceCategory)category));
        }
        LoadHair(); //Manually load hair
        LoadMakeupCategory();
        LoadParts(FaceCategory.Eyes); //Load subPanel
    }

    public void LoadMakeupCategory()
    {
        foreach (var category in System.Enum.GetValues(typeof(MakeupData.Part)))
        {
            if((MakeupData.Part)category == MakeupData.Part.None)
            {
                continue;
            }
            UIElement uie = Instantiate(iconBtnPrefab, mainPanel.content, false);
            uie.texts[0].gameObject.SetActive(false);
            uie.images[0].sprite = SpriteManager.GetSprite(category.ToString());
            uie.buttons[0].onClick.AddListener(() => LoadMakeup((MakeupData.Part)category));
        }
    }

    public void LoadHair()
    {
        UIElement uie = Instantiate(iconBtnPrefab, mainPanel.content, false);
        uie.texts[0].gameObject.SetActive(false);
        uie.images[0].sprite = SpriteManager.GetSprite(OutfitData.OufitType.Head.ToString());
        uie.buttons[0].onClick.AddListener(() => { ClearPanel(optionPanel.content); HideFaceSliders(); GameMaster.controller.viewBar.viewOutfit.LoadSubPanel(OutfitData.OufitType.Head, subPanel.content); });
    }

    [System.Obsolete]
    public void LoadParts_Predicated(string category)
    {
        ClearPanel(subPanel.content);
        var parts = GameMaster.controller.facePart.ShapeList.Where(x => x.shapeCategory == category);
        foreach (var part in parts)
        {
            UIElement uie = Instantiate(itemPrefab, subPanel.content, false);
            uie.texts[0].text = part.shapeName;
            var sk = part.shapeKeys;
            uie.buttons[0].onClick.AddListener(() => GameMaster.controller.customizationController.character.SetFaceShapes(sk));
        }
    }

    public void LoadParts(FaceCategory category)
    {
        ShowFaceSliders(category);
        ClearPanel(optionPanel.content);
        ClearPanel(subPanel.content);
        var parts = Character.CreateFaceParts(category.ToString());
        int i = 1;
        foreach (var part in parts)
        {
            UIElement uie = Instantiate(itemPrefab, subPanel.content, false);
            uie.texts[0].text = part.name + i;
            uie.texts[0].gameObject.SetActive(false);
            uie.images[0].gameObject.SetActive(true);
            uie.images[0].sprite = SpriteManager.GetSprite(part.name + i);
            uie.buttons[0].onClick.AddListener(() => 
            GameMaster.controller.customizationController.character.ToggleFaceShape(part.key, part.shape, part.name));
            i++;
        }
    }

    public void LoadMakeup(MakeupData.Part category)
    {
        HideFaceSliders();
        ClearPanel(optionPanel.content);
        ClearPanel(subPanel.content);

        //Layout Options
        List<Texture2D> layout = null;
        switch (category)
        {
            case MakeupData.Part.EyeMakeup:
                layout = GameMaster.controller.itemManager.eyemakeUpLayout;
                break;
            case MakeupData.Part.Blusher:
                layout = GameMaster.controller.itemManager.blushLayout;
                break;
            case MakeupData.Part.Lipstick:
                layout = GameMaster.controller.itemManager.lipstickLayout;
                break;
            default:
                break;
        }
        for (int i = 0; i < layout.Count; i++)
        {
            UIElement uieLayout = Instantiate(optionPrefab, optionPanel.content, false);
            uieLayout.texts[0].text = LAYOUT + " " + (i + 1);
            uieLayout.texts[0].gameObject.SetActive(false);
            uieLayout.images[0].sprite = SpriteManager.GetSprite(category.ToString() + (i + 1));
            uieLayout.images[0].gameObject.SetActive(true);
            int ii = i;
            uieLayout.buttons[0].onClick.AddListener(() =>
            {
                GameMaster.controller.customizationController.character.ChangeLayout(category, ii);
                GameMaster.controller.customizationController.character.SaveMakeupLayout();
            });
        }

        //None
        UIElement uieNone = Instantiate(itemPrefab, subPanel.content, false);
        uieNone.texts[0].text = NONE;
        uieNone.buttons[0].onClick.AddListener(() =>
        {
            GameMaster.controller.customizationController.character.ApplyMakeup(null, category);
            GameMaster.controller.customizationController.character.SaveMakeupItems();
            GameMaster.controller.customizationController.character.SaveMakeupLayout();
        });

        //Makeup colors
        var parts = GameMaster.controller.itemManager.makeupDatas;
        int j = 0;
        foreach (var part in parts)
        {
            j++;
            int ii = j;
            if (part.part != category || part.part == MakeupData.Part.None)
            {
                continue;
            }
            UIElement uie = Instantiate(makeupColorPrefab, subPanel.content, false);
            uie.images[0].color = part.color;
            uie.images[1].sprite = metallicSprite;
            uie.images[1].color = new Color(Color.white.r, Color.white.g, Color.white.b, part.metallic);
            uie.images[2].sprite = smoothness1Sprite;
            uie.images[2].color = new Color(Color.white.r, Color.white.g, Color.white.b, smooth1OpacityCurve.Evaluate(part.smoothness));
            uie.images[3].sprite = smoothness2Sprite;
            uie.images[3].color = new Color(Color.white.r, Color.white.g, Color.white.b, smooth2OpacityCurve.Evaluate(part.smoothness));
            var d = part;
            uie.buttons[0].onClick.AddListener(() => 
            {
#if UNITY_EDITOR
                UnityEditor.EditorGUIUtility.PingObject(d);
#endif
                GameMaster.controller.customizationController.character.ApplyMakeup(d);
                GameMaster.controller.customizationController.character.SaveMakeupItems();
            });

        }
    }

    public void RaycastFace()
    {
        if (Input.GetMouseButtonDown(0) && !CanvasController.IsOnUI())
        {
            //LayerMask layer = LayerMask.GetMask(new string[] { "Face" });
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
            {
                Skeleton skeleton = hit.collider.gameObject.GetComponentInParent<Skeleton>();
                if (skeleton)
                {
                    highlightedFacePart = skeleton.GetPart(hit.collider);
                    GameMaster.controller.colorPalette.ShowColor(skeleton.GetColor(hit.collider), CanvasController.MouseToUIPos());
                }
            }
        }
    }

    //public ShapeKey[] CreateFaceParts(string category)
    //{
    //    Mesh face = GameMaster.controller.customizationController.character.face.sharedMesh;
    //    List<ShapeKey> shapeList = new List<ShapeKey>();
    //    shapeList.Add(new ShapeKey(-1, 0, category));
    //    for (int i = 0; i < face.blendShapeCount; i++)
    //    {
    //        //create default 
    //        string s = face.GetBlendShapeName(i);
    //        if(s.Contains(category))
    //        {
    //            shapeList.Add(new ShapeKey(i, 100, s.Split(SEPARATOR.ToCharArray())[0]));
    //        }
    //    }
    //    return shapeList.ToArray();
    //}

    private void ChangeColor(int colorIndex)
    {
        if(View_Bar.currentPage == View_Bar.Page.Customize)
        {
            GameMaster.controller.customizationController.character.ChangeColor(highlightedFacePart, colorIndex);
        }
    }

    private void ShowFaceSliders(FaceCategory category)
    {
        GameMaster.user.LoadFaceEdit();
        currentCategory = category;
        sliders.gameObject.SetActive(true);
        isLoadSlider = true;
        sliders.rectTransforms[0].gameObject.SetActive(true);
        sliders.rectTransforms[1].gameObject.SetActive(true);
        sliders.rectTransforms[2].gameObject.SetActive(true);
        switch (category)
        {
            case FaceCategory.Eyes:
                sliders.texts[0].text = "Distance";
                sliders.texts[1].text = "Height";
                sliders.texts[2].text = "Size";
                slider[0].value = GameMaster.user.faceEdit[0];
                slider[1].value = GameMaster.user.faceEdit[1];
                slider[2].value = GameMaster.user.faceEdit[2];
                break;
            case FaceCategory.Nose:
                sliders.rectTransforms[2].gameObject.SetActive(false);
                sliders.texts[0].text = "Height";
                sliders.texts[1].text = "Size";
                slider[0].value = GameMaster.user.faceEdit[3];
                slider[1].value = GameMaster.user.faceEdit[4];
                break;
            case FaceCategory.Mouth:
                sliders.texts[0].text = "Height";
                sliders.texts[1].text = "Chin";
                sliders.texts[2].text = "Size";
                slider[0].value = GameMaster.user.faceEdit[5];
                slider[1].value = GameMaster.user.faceEdit[6];
                slider[2].value = GameMaster.user.faceEdit[7];
                break;
            default:
                break;
        }
        isLoadSlider = false;
    }

    private void HideFaceSliders()
    {
        sliders.gameObject.SetActive(false);
    }

    private void FaceSliderUpdate(float val)
    {
        if(isLoadSlider)
        {
            return;
        }
        isSliderUpdate = true;
        switch (currentCategory)
        {
            case FaceCategory.Eyes:
                GameMaster.skeleton.SetEyes(slider[0].value, slider[1].value, slider[2].value);
                GameMaster.user.faceEdit[0] = slider[0].value;
                GameMaster.user.faceEdit[1] = slider[1].value;
                GameMaster.user.faceEdit[2] = slider[2].value;
                break;
            case FaceCategory.Nose:
                GameMaster.skeleton.SetNose(slider[0].value, slider[1].value);
                GameMaster.user.faceEdit[3] = slider[0].value;
                GameMaster.user.faceEdit[4] = slider[1].value;
                break;
            case FaceCategory.Mouth:
                GameMaster.skeleton.SetMouth(slider[0].value, slider[1].value, slider[2].value);
                GameMaster.user.faceEdit[5] = slider[0].value;
                GameMaster.user.faceEdit[6] = slider[1].value;
                GameMaster.user.faceEdit[7] = slider[2].value;
                break;
            default:
                break;
        }
    }

    private void AssignSliderListeners()
    {
        foreach (var s in slider)
        {
            s.onValueChanged.AddListener(FaceSliderUpdate);
        }
    }
}
