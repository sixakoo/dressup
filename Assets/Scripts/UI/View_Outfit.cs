using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UX;
using System.Linq;
using UnityEngine.EventSystems;

public class View_Outfit : MonoBehaviour, IPointerDownHandler
{
    [Header("Variable")]
    public int maxAccessories;

    [Header("Prefab")]
    public UIElement itemPrefab;
    public UIElement iconBtnPrefab;
    public UIElement colorPrefab;

    [Header("Component")]
    public UXAnimator anim;
    public RectTransform screenSize;
    public ScrollRect mainPanel;
    public ScrollRect subPanel;
    public Toggle tuckTgl;
    public static View_Outfit Instance;
    private Character.ColorizeParts highlightedPart;

    private const string PLUS = "Plus";

    private void Awake()
    {
        if(Instance != null)
        {
            Debug.LogError("Another instance exist!");
        }
        Instance = this;
    }

    private void Start()
    {
        GameMaster.controller.colorPalette.ColorResult += ChangeOutfitColor;
        tuckTgl.onValueChanged.AddListener(delegate {  ToggleTuckIn(GameMaster.controller.customizationController.isTuckedIn); } );
        GameMaster.controller.customizationController.onCharacterReset += CheckTuckable;
        LoadMainPanel();
        LoadSubPanel(OutfitData.OufitType.Upper, subPanel.content);
    }

    private void OnEnable()
    {
        if(GameMaster.skeleton)
        {
            GameMaster.skeleton.SetFaceCollidersActive(false);
        }
        mainPanel.horizontalNormalizedPosition = 0;
        subPanel.horizontalNormalizedPosition = 0;
    }

    private void Update()
    {
        RaycastOutfit();
    }

    private void ClearPanel(Transform panel)
    {
        foreach (Transform t in panel)
        {
            Destroy(t.gameObject);
        }
    }

    public void LoadMainPanel()
    {
        ClearPanel(mainPanel.content);
        foreach (var category in System.Enum.GetValues(typeof(OutfitData.OufitType))) //Normal outfit
        {
            if((OutfitData.OufitType)category == OutfitData.OufitType.None ||
                (OutfitData.OufitType)category == OutfitData.OufitType.Accessories)
            {
                continue;
            }
            UIElement uie = Instantiate(iconBtnPrefab, mainPanel.content, false);
            uie.texts[0].gameObject.SetActive(false);
            uie.images[0].sprite = SpriteManager.GetSprite(category.ToString());
            OutfitData.OufitType oType = (OutfitData.OufitType)category;
            uie.buttons[0].onClick.AddListener(() => LoadSubPanel(oType, subPanel.content));
        }
        var equipped = GameMaster.user.Accessories;
        foreach (var a in GameMaster.controller.itemManager.GetAccessoriesGroup(GameMaster.user.Accessories)) //Equipped accessories
        {
            UIElement uie = Instantiate(itemPrefab, mainPanel.content, false);
            uie.texts[0].text = a.outfitName;
            uie.texts[0].gameObject.SetActive(false);
            uie.images[0].gameObject.SetActive(true);// = SpriteManager.GetSprite(a.outfitType.ToString());
            uie.images[0].sprite = SpriteManager.GetSprite(a.outfitName);
            string tag = a.tag;
            OutfitData i = a;
            uie.buttons[0].onClick.AddListener(() =>
            {
                ClearPanel(subPanel.content);
                LoadAccessoryOption(i);
            });
        }
        if(GameMaster.user.Accessories.Count <= maxAccessories) //Add accessory button
        {
            UIElement uie = Instantiate(itemPrefab, mainPanel.content, false);
            uie.texts[0].gameObject.SetActive(false);
            uie.images[0].sprite = SpriteManager.GetSprite(PLUS);
            uie.buttons[0].onClick.AddListener(() =>
            {
                LoadSubPanel(OutfitData.OufitType.Accessories, subPanel.content, tag);
            });
        }
    }

    public void LoadSubPanel(OutfitData.OufitType outfitType, Transform panel, string tag = "")
    {
        ClearPanel(panel);
        List<OutfitData> items = new List<OutfitData>();
        if (outfitType == OutfitData.OufitType.Accessories)
        {
            var group = GameMaster.controller.itemManager.GetAccessoriesGroup(GameMaster.controller.itemManager.outfitDatas);
            foreach (var a in group) //don't show group if already equipped
            {
                bool equipped = false;
                foreach (var item in GameMaster.user.Accessories)
                {
                    if(item.outfitName == a.outfitName)
                    {
                        equipped = true;
                        break;
                    }
                }
                if(equipped)
                {
                    continue;
                }
                items.Add(a);
            }
        }
        else
        {
            items = GameMaster.controller.itemManager.outfitDatas.Where(x => x.outfitType == outfitType).ToList();
        }
        foreach (var item in items)
        {
            UIElement uie = Instantiate(itemPrefab, panel, false);
            uie.texts[0].text = item.outfitName;
            uie.texts[0].gameObject.SetActive(false);
            uie.images[0].gameObject.SetActive(true);
            uie.images[0].sprite = SpriteManager.GetSprite(item.outfitName);
            var i = item;
            if(outfitType == OutfitData.OufitType.Accessories) //Accessories list
            {
                uie.buttons[0].onClick.AddListener(() => {
                    GameMaster.controller.customizationController.AssignAccessory(i, true); 
                    LoadMainPanel();
                    ClearPanel(panel);
                });
            }
            else //Outfit
            {
                uie.buttons[0].onClick.AddListener(() => {
                    GameMaster.controller.customizationController.AssignOutfit(i); CheckTuckable(); GameMaster.controller.cameraController.Refresh();
                });
            }
            if(item.outfitModel == null)
            {
                uie.transform.SetAsFirstSibling();
            }
        }
        CheckTuckable();
    }

    private void LoadAccessoryOption(OutfitData accessoryData)
    {
        UIElement uie = Instantiate(itemPrefab, subPanel.content, false);
        uie.texts[0].text = "Remove";
        uie.buttons[0].onClick.AddListener(() => {
            GameMaster.controller.customizationController.RemoveAccesory(accessoryData);
            LoadMainPanel();
            ClearPanel(subPanel.content);
        });
        if (GameMaster.controller.itemManager.IsPairAccessory(accessoryData))
        {
            var b = GameMaster.controller.itemManager.GetAccessoryOptions(accessoryData);
            UIElement left = Instantiate(itemPrefab, subPanel.content, false);
            left.texts[0].text = "Left";
            left.buttons[0].onClick.AddListener(() => {
                GameMaster.controller.customizationController.RemoveAccesory(b[1]);
                GameMaster.controller.customizationController.AssignAccessory(b[0]);
                LoadMainPanel();
            });

            UIElement right = Instantiate(itemPrefab, subPanel.content, false);
            right.texts[0].text = "Right";
            right.buttons[0].onClick.AddListener(() => {
                GameMaster.controller.customizationController.RemoveAccesory(b[0]);
                GameMaster.controller.customizationController.AssignAccessory(b[1]);
                LoadMainPanel();
            });

            UIElement Both = Instantiate(itemPrefab, subPanel.content, false);
            Both.texts[0].text = "Both";
            ;
            Both.buttons[0].onClick.AddListener(() => {
                foreach (var item in b)
                {
                    GameMaster.controller.customizationController.AssignAccessory(item);
                    LoadMainPanel();
                }
            });
        }
    }

    public void CheckTuckable()
    {
        tuckTgl.isOn = GameMaster.controller.customizationController.isTuckedIn;
        bool tuckable = false;
        bool tuckSupport = false;
        foreach (var outfit in GameMaster.controller.customizationController.currentOutfits)
        {
            if (outfit.data.tuckable){ tuckable = true;}
            if (outfit.data.supportTuck) { tuckSupport = true; }
        }
        tuckTgl.gameObject.SetActive(tuckable && tuckSupport);
        tuckTgl.isOn = GameMaster.controller.customizationController.isTuckedIn;
    }

    private void ToggleTuckIn(bool tuck)
    {
        GameMaster.controller.customizationController.isTuckedIn = tuckTgl.isOn;
        GameMaster.controller.customizationController.ToggleTuckIn(GameMaster.controller.customizationController.isTuckedIn);
        GameMaster.controller.customizationController.SaveOutfit();
    }

    public void RaycastOutfit()
    {
        if (Input.GetMouseButtonDown(0) && !CanvasController.IsOnUI())
        {
            //LayerMask layer = LayerMask.GetMask(new string[] { "Outfit" });
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
            {
                Skeleton skeleton = hit.collider.gameObject.GetComponentInParent<Skeleton>();
                if (skeleton)
                {
                    if (skeleton)
                    {
                        highlightedPart = skeleton.GetPart(hit.collider);
                        GameMaster.controller.colorPalette.ShowColor(skeleton.GetColor(hit.collider), CanvasController.MouseToUIPos());
                    }
                }
            }
        }
    }

    public void ChangeOutfitColor(int colorIndex)
    {
        if (View_Bar.currentPage == View_Bar.Page.Outfit)
        {
            GameMaster.controller.customizationController.character.ChangeColor(highlightedPart, colorIndex);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        RaycastOutfit();
    }
}
