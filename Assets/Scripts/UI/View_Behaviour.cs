using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UX;

public class View_Behaviour : MonoBehaviour
{
    [Header("Sprites")]
    public Sprite[] bubbleSprites;

    [Header("Variables")]
    public Vector3 bubbleOffset;
    public List<int> notificationQueue = new List<int>();
    public float notificationDuration;

    [Header("Components")]
    public TextMeshProUGUI version;
    public LayerMask moveLayer;
    public LayerMask triggerLayer;
    public UIElement bubble;
    public UXProgress energyBar;
    public UXProgress moodBar;
    public UXProgress cleanBar;
    public UXProgress hungerBar;
    public UXProgress healthBar;
    public UIElement meterTab;

    [Header("Debug")]
    public bool debug;
    public UIElement debugElement;
    public List<CharacterAction> actions;
    public List<Button> meterBtns;

    //Const
    public const float UPDATEINTERVAL = 1f;
    public const string METERS = "meters";

    //Privates
    private float meterIntervalTimer;
    private bool isTabOpen;
    private float notificationTimer;
    private ActionTrigger currentTrigger;
    private static View_Behaviour instance;

    private void Awake()
    {
        version.text = "Version " + Application.version;
        if(instance != null)
        {
            Debug.LogError("Another instance exist!");
        }
        instance = this;
    }

    private void Start()
    {
        DebugStart();
        bubble.transform.SetParent(CanvasController.Instance.transform);
        AssignDebugButtons();
        meterTab.animators[0].Play(1);
        AssignButtons();
        LoadMeters();
        UpdateMeters();
        AssignDelegates();
        View_Options.onSelect += SelectAction;        
    }

    private void OnEnable()
    {
        if (GameMaster.skeleton)
        {
            GameMaster.skeleton.SetFaceCollidersActive(false);
        }
    }

    private void UpdateBubblePos()
    {
        ((RectTransform)bubble.transform).anchoredPosition =
                CanvasController.ScreenToUIPos(Camera.main.WorldToScreenPoint
                (GameMaster.skeleton.mouth.position + bubbleOffset));
    }

    private void Update()
    {
        RaycastPoint();
        DebugTools();
        meterIntervalTimer += Time.deltaTime;
        UpdateBubblePos();
        NotificationCheck();
        if (meterIntervalTimer >= UPDATEINTERVAL)
        {
            meterIntervalTimer = 0;
            UpdateMeters();
        }
    }

    private void SelectAction(int index)
    {
        if(currentTrigger != null)
        {
            currentTrigger.Action(index);
        }
    }

    private void RaycastPoint()
    {
        if (Input.GetMouseButtonDown(0) && View_Bar.currentPage == View_Bar.Page.Home && !CanvasController.IsOnUI())
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hitTrigger, Mathf.Infinity, triggerLayer))
            {
                if (View_Options.IsShowing())
                {
                    View_Options.CloseOptions();
                }
                else
                {
                    ShowActionOptions(hitTrigger.collider.gameObject.GetComponent<ActionTrigger>(), hitTrigger.point);
                }
            }
            else if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, Mathf.Infinity, moveLayer))
            {
                if (View_Options.IsShowing())
                {
                    View_Options.CloseOptions();
                }
                else
                {
                    ShowMove(hit.point);
                }
            }
        }
    }

    private bool IsInAction()
    {
        return GameMaster.controller.characterBehaviour.IsInAction();
    }

    private void ShowMove(Vector3 _point)
    {
        //GameMaster.controller.characterBehaviour.Sequence(_point);
        //View_Options.CloseOptions();
        View_Options.ShowOptions(null, _point);
    }

    private void ShowActionOptions(ActionTrigger _trigger, Vector3 _point)
    {
        currentTrigger = _trigger;
        View_Options.ShowOptions(_trigger.actions.ToList(), _point);
    }

    private void ShowCancel(Vector3 _point)
    {
        currentTrigger = null;
        var pos = CanvasController.ScreenToUIPos(Camera.main.WorldToScreenPoint(_point));
        View_Options.ShowOptions(null, pos);
    }

    private void UpdateMeters()
    {
        var energy = GameMaster.controller.characterBehaviour.energy;
        var mood = GameMaster.controller.characterBehaviour.mood;
        var hunger = GameMaster.controller.characterBehaviour.hunger;
        var clean = GameMaster.controller.characterBehaviour.clean;
        var health = GameMaster.controller.characterBehaviour.health;
        
        energyBar.SetCurrentProgress((int)energy.val, (int)energy.maxVal);
        moodBar.SetCurrentProgress((int)mood.val, (int)mood.maxVal);
        cleanBar.SetCurrentProgress((int)clean.val, (int)clean.maxVal);
        hungerBar.SetCurrentProgress((int)hunger.val, (int)hunger.maxVal);
        healthBar.SetCurrentProgress((int)health.val, (int)health.maxVal);
    }

    private void SaveMeters()
    {
        var energy = GameMaster.controller.characterBehaviour.energy;
        var mood = GameMaster.controller.characterBehaviour.mood;
        var hunger = GameMaster.controller.characterBehaviour.hunger;
        var clean = GameMaster.controller.characterBehaviour.clean;
        var health = GameMaster.controller.characterBehaviour.health;
        List<float> meters = new List<float>()
        {
            energy.val, 
            mood.val, 
            hunger.val, 
            clean.val, 
            health.val,
        };
        SaveManager.SaveFloatList(METERS, meters);
    }

    private void LoadMeters()
    {
        if(!PlayerPrefs.HasKey(METERS))
        {
            return;
        }
        var energy = GameMaster.controller.characterBehaviour.energy;
        var mood = GameMaster.controller.characterBehaviour.mood;
        var hunger = GameMaster.controller.characterBehaviour.hunger;
        var clean = GameMaster.controller.characterBehaviour.clean;
        var health = GameMaster.controller.characterBehaviour.health;
        var data = SaveManager.LoadFloatList(METERS);
        energy.val = data[0];
        mood.val = data[1];
        hunger.val = data[2];
        clean.val = data[3];
        health.val = data[4];
    }

    private void OnApplicationQuit()
    {
        SaveMeters();
    }

    private void AssignButtons()
    {
        meterTab.buttons[0].onClick.AddListener(() => ToggleMeterTab(!isTabOpen));
    }

    public static void ToggleMeterTab(bool isOpen)
    {
        instance.meterTab.animators[0].Play(isOpen ? 0 : 1);
        instance.meterTab.animators[1].Play(isOpen ? 1 : 0);
        instance.isTabOpen = isOpen;
        instance.debugElement.rectTransforms[0].gameObject.SetActive(instance.debug && isOpen ? true : false);
    }

    private void AssignDebugButtons()
    {
        for (int i = 0; i < 5; i++)
        {
            int ii = i;
            debugElement.buttons[i].onClick.AddListener(() => GameMaster.controller.characterBehaviour.Action(actions[ii]));
        }
    }

    private void NotificationCheck()
    {
        if(notificationQueue.Count > 0)
        {
            notificationTimer += Time.deltaTime;
            if(notificationTimer > notificationDuration)
            {
                notificationTimer = 0;
                ShowNotification(notificationQueue[0]);
                notificationQueue.RemoveAt(0);
            }
        }
    }

    private void AddNotification(int index)
    {
        if(!notificationQueue.Contains(index))
        {
            notificationQueue.Add(index);
        }
    }

    private void ShowNotification(int index)
    {
        bubble.images[0].sprite = bubbleSprites[index];
        bubble.animators[0].PlayThenClose(0);
    }

    private void ClearNotification()
    {
        notificationQueue = new List<int>();
    }

    private void AssignDelegates()
    {
        var meters = GameMaster.controller.characterBehaviour.meters;
        for (int i = 0; i < meters.Length; i++)
        {
            int ii = i;
            meters[i].onNotification += ()=> AddNotification(ii);
        }
    }

    #region Debug

    private void DebugStart()
    {
        meterBtns[0].onClick.AddListener(()=> DebugMeter(GameMaster.controller.characterBehaviour.energy));
        meterBtns[1].onClick.AddListener(() => DebugMeter(GameMaster.controller.characterBehaviour.mood));
        meterBtns[2].onClick.AddListener(() => DebugMeter(GameMaster.controller.characterBehaviour.clean));
        meterBtns[3].onClick.AddListener(() => DebugMeter(GameMaster.controller.characterBehaviour.hunger));
        meterBtns[4].onClick.AddListener(() => DebugMeter(GameMaster.controller.characterBehaviour.health));
    }

    private void DebugMeter(CharacterMeter meter)
    {
        if(meter.val > 0)
        {
            meter.val -= meter.maxVal / 2;
        }
        else
        {
            meter.val = meter.maxVal;
        }
        UpdateMeters();
    }

    private void DebugTools()
    {
        if(Input.GetKeyDown(KeyCode.F9))
        {
            foreach (var meter in GameMaster.controller.characterBehaviour.meters)
            {
                meter.val = 100;
            }
        }

        if (Input.GetKeyDown(KeyCode.F8))
        {
            foreach (var meter in GameMaster.controller.characterBehaviour.meters)
            {
                meter.val = 0;
            }
        }
    }

    #endregion
}
