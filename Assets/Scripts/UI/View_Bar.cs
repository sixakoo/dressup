using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UX;

public class View_Bar : MonoBehaviour
{
    [Header("Component")]
    public View_Outfit viewOutfit;
    public View_Face viewFace;
    public UXAnimator[] animators;

    [Header("Buttons")]
    public Button[] buttons;
    public static Page currentPage;

    public enum Page
    {
        Customize,
        Outfit,
        Home,
        Settings
    }

    private void Start()
    {
        currentPage = Page.Settings;
        SetupPage();
        SetAnim();
        GameMaster.skeleton.animator.Idle();
        StartCoroutine(InitPages());
        GameMaster.controller.characterBehaviour.onAction += DisableButton;
    }

    private void SetupPage()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            animators[i].gameObject.SetActive(false);
            int ii = i;
            buttons[i].onClick.AddListener(() =>
            {
                AssignPage(ii);
            });
        }
    }

    private void SetAnim()
    {
        buttons[0].onClick.AddListener(() => {GameMaster.skeleton.animator.Static(); GameMaster.skeleton.characterController.TurnToDefault(); });
        buttons[1].onClick.AddListener(() => {GameMaster.skeleton.animator.Static(); GameMaster.skeleton.characterController.TurnToDefault(); });
        buttons[2].onClick.AddListener(() => { GameMaster.skeleton.animator.Idle(); }); ;
    }

    private void AssignPage(int index)
    {
        if ((int)currentPage != index) //Open if not already opened
        {
            animators[index].Play(0);
        }
        for (int j = 0; j < animators.Length; j++)
        { //Close other pages execpt the one that is closing
            if (index != j && animators[j].gameObject.activeSelf && !animators[j].sequenceList[1].isAnimating)
            {
                animators[j].PlayThenClose(1);
            }
        }
        currentPage = (Page)index;
        GameMaster.controller.cameraController.SwitchCameraAngle(currentPage);
        GameMaster.controller.customizationController.skeleton.characterController.MoveState(false);
        View_Options.CloseOptions();
    }

    private IEnumerator InitPages()
    {
        foreach (var item in animators)
        {
            item.gameObject.SetActive(true);
        }
        yield return null;
        foreach (var item in animators)
        {
            item.gameObject.SetActive(false);
        }
        AssignPage((int)Page.Home);
        View_Behaviour.ToggleMeterTab(false);
    }

    public void DisableButton(bool action)
    {
        foreach (var item in buttons)
        {
            item.interactable = !action;
        }
    }
}