using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "SpriteManager", menuName = "ScriptableObjects/SpriteManager", order = 0)]
public class SpriteManager : ScriptableObject
{
    public SpriteAtlas[] atlases;

    public static Sprite GetSprite(string spriteName)
    {
        Sprite s;
        foreach (var atlas in GameMaster.controller.spriteManager.atlases)
        {
            s = atlas.GetSprite(spriteName);
            if(s != null)
            {
                return s;
            }
        }
        Debug.LogError("sprite not found : " + spriteName);
        return null;
    }
}
