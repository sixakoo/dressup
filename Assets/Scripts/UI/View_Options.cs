using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Linq;
using UX;

public class View_Options : MonoBehaviour
{
    public UXAnimator anim;
    public UIElement optionsPrefab;
    public float spacing;
    public Direction direction;
    private static View_Options instance;
    public static UnityAction<int> onSelect;
    private bool isShowing;

    public const string MOVE = "Move";

    public enum Direction
    {
        Horizontal,
        Vertical
    }

    private void Awake()
    {
        instance = this;
    }

    public static bool IsShowing()
    {
        return instance.isShowing;
    }

    public static void ShowOptions(List<CharacterAction> options, Vector3 hitPos)
    {
        instance.isShowing = true;
        var screenPos = CanvasController.ScreenToUIPos(Camera.main.WorldToScreenPoint(hitPos));
        List<string> optionNames = new List<string>() { MOVE };
        if(options != null)
        {
            optionNames = options.Select((x) => x.name).ToList();
        }
        instance.ClearOptions();
        var parentRect = ((RectTransform)instance.transform);
        var parentSize = parentRect.sizeDelta;
        var stackSize = ((Vector2.one * instance.spacing) * (optionNames.Count - 1)) + (instance.optionsPrefab.RectTransform.sizeDelta * optionNames.Count);
        var stackPos = (parentSize / 2) - (stackSize / 2);
        var startPos = (parentSize / 2) - (instance.optionsPrefab.RectTransform.sizeDelta / 2);
        var pivotOffset = instance.optionsPrefab.RectTransform.sizeDelta / 2;
        ((RectTransform)instance.transform).anchoredPosition = screenPos;
        for (int i = 0; i < optionNames.Count; i++)
        {
            var p = ((instance.optionsPrefab.RectTransform.sizeDelta + (Vector2.one * instance.spacing)) * i);
            Vector2 pos = instance.direction == Direction.Horizontal ? 
                new Vector2(stackPos.x + p.x, startPos.y) : 
                new Vector2(startPos.x, stackPos.y + p.y);
            UIElement uie = Instantiate(instance.optionsPrefab, instance.transform, false);
            int j = i;
            if(options != null) //Action
            {
                uie.buttons[0].onClick.AddListener(() => { onSelect(j); CloseOptions(); });
            }
            else //Move
            {
                //uie.buttons[0].onClick.AddListener(() => { GameMaster.controller.characterBehaviour.StopAction(); CloseOptions(); });
                uie.buttons[0].onClick.AddListener(() => {
                GameMaster.controller.characterBehaviour.Sequence(hitPos);
                View_Options.CloseOptions();
                });
            }
            uie.RectTransform.anchoredPosition = pos + pivotOffset;
            uie.texts[0].text = optionNames[i];
            uie.animators[0].sequenceList[0].move = ((RectTransform)uie.transform).anchoredPosition;
            uie.animators[0].Play(0);
        }
        instance.anim.Play(0);
    }

    public static void CloseOptions()
    {
        instance.anim.PlayThenClose(1);
        instance.isShowing = false;
    }

    private void ClearOptions()
    {
        foreach (Transform item in transform)
        {
            Destroy(item.gameObject);
        }
    }
}
