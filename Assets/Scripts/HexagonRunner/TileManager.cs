﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Gameplay
{
    public class TileManager : MonoBehaviour
    {
        public Orientation orientation;
        public int
            column,
            row;
        public float hexagonVertex;
        public float tileSpacing;
        public int mineCount;
        public Dictionary<GameObject, Tile> tileObjectDict;
        public Dictionary<int, Tile> tileDict;
        public Tile tilePrefab;
        public Transform tileGroup;
        public Color[] proximityTextColor, proximityTileColor;
        public Color
            initialColor,
            spawnColor,
            goalColor;
        public static TileManager Instance;
        [System.NonSerialized]
        public Vector2 tileSize;
        public const string TILE = "Tile";
        public static Tile
            startTile,
            endTile;

        public enum Orientation
        {
            FlatTop,
            PointyTop
        }

        private void Awake()
        {
            if (Instance != null)
            {
                Debug.LogError("Another instance exist", Instance);
                Debug.LogError("and this", this);
            }
            Instance = this;
        }

        private void SetHexagon()
        {
            Hexagon.HexagonVertex = hexagonVertex;
            tilePrefab.transform.localScale =  new Vector3(hexagonVertex, hexagonVertex, 2);
        }

        private void Start()
        {
            //Init();
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.J))
            {
                Init();
            }
            if (Input.GetKeyDown(KeyCode.K))
            {
                ShowAllProximityMarkings(true);
            }
        }       

        public void Init()
        {
            SetHexagon();
            LoadTiles();
            SetStartEndTiles();
            AddMines();
            //GetProximityMarkings();
        }

        public void ClearTiles()
        {
            foreach(Transform t in tileGroup)
            {
                Destroy(t.gameObject);
            }
        }

        public void LoadTiles()
        {            
            ClearTiles();
            tileObjectDict = new Dictionary<GameObject, Tile>();
            tileDict = new Dictionary<int, Tile>();
            tileSize = new Vector2(Hexagon.HexagonWidth + tileSpacing, Hexagon.HexagonHeight + tileSpacing);
            bool pointy = orientation == Orientation.PointyTop;
            for (int i = 0; i < column * row; i++)
            {
                Tile tile = Instantiate(tilePrefab, tileGroup, false);
                tile.SetTile(i + 1, column, row, orientation == Orientation.FlatTop);
                tile.name = tile.id.ToString();
                tile.SetText((i + 1).ToString());
                tileObjectDict.Add(tile.gameObject, tile);
                tileDict.Add(tile.id, tile);
                var c = pointy ? i % column * tileSize.x : Mathf.FloorToInt(i / row) * tileSize.y;
                var r = pointy ? Mathf.FloorToInt(i / column) * tileSize.y : i % row * tileSize.x;
                tile.transform.position = new Vector3(c, tile.transform.position.y, r);
                tile.isFallable = true;
                //offset even rows
                if(Mathf.CeilToInt(i / (pointy ? column : row)) % 2 == 0)
                {
                    var s = (pointy? tile.transform.position.x : tile.transform.position.z) + Hexagon.HexagonApothem + tileSpacing / 2;
                    tile.transform.position = new Vector3(
                        pointy? s : tile.transform.position.x,
                        tile.transform.position.y,
                        pointy? tile.transform.position.z : s);
                }
                tile.transform.rotation = pointy? Quaternion.Euler(Tile.PointyTopRot) : Quaternion.Euler(Tile.FlatTopRot);
            }
        }

        public void SetStartEndTiles()
        {
            bool pointy = orientation == Orientation.PointyTop;
            startTile = tileDict[pointy?
                Mathf.CeilToInt((float)column / 2) :
                Mathf.FloorToInt((float)column / 2) * row + 1];
            endTile = tileDict[pointy? 
                (column * row) - Mathf.CeilToInt((float)column / 2) + 1 :
                Mathf.CeilToInt((float)column / 2) * row];
            //Debug.Log("", startTile);
            //Debug.Log("", endTile);
            startTile.SetTileColor(spawnColor);
            endTile.SetTileColor(goalColor);
            startTile.isFallable = false;
            endTile.isFallable = false;
        }

        public void AddMines()
        {
            List<KeyValuePair<GameObject, Tile>> tempList = tileObjectDict.ToList();
            for(int i = 0; i < mineCount; i++)
            {
                int index = Random.Range(0, tempList.Count);
                var mine = tempList[index];
                if(mine.Key != startTile && mine.Key != endTile)
                {
                    mine.Value.SetMine();
                }
                tempList.RemoveAt(index);
            }
        }

        public void GetProximityMarkings()
        {
            foreach(var kvp in tileObjectDict)
            {
                if (kvp.Value.proximity != -1)
                {
                    foreach(int n in kvp.Value.neigbours)
                    {
                        if(n != -1) // if tile exist
                        {
                            if (tileDict[n].proximity == -1)
                            {
                                kvp.Value.proximity++;
                                //Debug.Log("id : " + kvp.Value.id, kvp.Value);
                            }
                        }
                    }
                }
            }
            ShowAllProximityMarkings(true);
        }

        public void ShowAllProximityMarkings(bool show)
        {
            foreach (var kvp in tileDict)
            {
                if(show)
                {
                    ShowTile(kvp.Value, true ,true);
                }
                else
                {
                    kvp.Value.HideTile();
                }
                kvp.Value.text.text = kvp.Value.proximity.ToString();
                kvp.Value.text.gameObject.SetActive(show);
            }
        }

        public void ShowTile(Tile tile, bool hideProximity = true, bool debug = false) //Move this because of singleton reference
        {
            if (this == null)
            {
                return;
            }
            tile.text.gameObject.SetActive(true);
            if (tile.proximity == -1)
            {
                if (this != TileManager.startTile && this != TileManager.endTile)
                {
                    tile.text.text = "ded";
                    tile.SetTileColor(Color.red);
                    tile.text.color = Color.white;
                    if (!debug)
                    {
                        //MinePlay.TriggerMine();
                    }
                }
            }
            else if (!hideProximity)
            {
                tile.text.text = tile.proximity.ToString();
                tile.SetTileColor(TileManager.Instance.proximityTileColor[tile.proximity]);
                tile.text.color = TileManager.Instance.proximityTextColor[tile.proximity];
            }
            else
            {
                tile.text.gameObject.SetActive(false);
                tile.SetTileColor(TileManager.Instance.initialColor);
                tile.text.color = TileManager.Instance.proximityTextColor[0];
            }
        }
    }
}