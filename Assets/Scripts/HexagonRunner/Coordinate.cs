﻿using System.Collections;
using UnityEngine;

namespace Gameplay
{
    [SerializeField]
    public struct Coordinate
    {
        int x;
        int y;
    }
}