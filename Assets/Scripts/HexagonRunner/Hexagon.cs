﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Hexagon
    {
        public static float HexagonVertex { set; get; }
        public static float HexagonApothem { get { return HexagonVertex * 0.433f; } }
        public static float HexagonSide { get { return HexagonVertex / 2; } }
        public static float HexagonWidth { get { return HexagonApothem * 2; } }
        public static float HexagonHeight { get { return HexagonSide + HexagonSide / 2; } }
        public static float HexagonDegree { get { return 30; } }
    }
}