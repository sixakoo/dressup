﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Tile : MonoBehaviour
    {
        public int id;
        public int proximity;
        public Coordinate coordinate;
        public List<int> neigbours;
        public TextMesh text;
        public MeshRenderer meshRenderer;
        public bool isFallable;

        private const float destroyHeight = -5;
        private const float fallSpeed = 9.81f;
        private const float fallDelay = .5f;

        public readonly static Vector3 PointyTopRot = new Vector3(-90, 0, 0);
        public readonly static Vector3 FlatTopRot = new Vector3(-90, 30, 0);

        public void SetTile(int _id, int column, int row, bool isFlatTop)
        {
            id = _id;
            SetNeighbouringTiles(column, row, isFlatTop);
            //text.text = _id.ToString();
        }

        public void SetMine()
        {
            proximity = -1;
            //meshRenderer.material.SetColor("_BaseColor", Color.red);
        }

        public void SetTileColor(Color color) //Move this because of singleton reference
        {
            meshRenderer.material.SetColor("_Color", color);
        }

        public void SetText(string _text)
        {
            text.text = _text;
        }

        public void HideTile()
        {
            text.gameObject.SetActive(false);
            SetTileColor(TileManager.Instance.initialColor);
        }

        public void SetNeighbouringTiles(int column, int row, bool isFlatTop)
        {
            neigbours = new List<int>();

            if (!isFlatTop)
            {
                int n1; //top right
                if (Mathf.CeilToInt((float)id / (float)column) != row) //if tile is not on the top row
                {
                    if (id % column != 0) //if tile is not on the far right
                    {
                        n1 = (((Mathf.CeilToInt((float)id / (float)column) + 1) % 2) * -1) + id + column + 1;
                    }
                    else
                    {
                        if (Mathf.CeilToInt((float)id / (float)column) % 2 == 0) // if tile is on even row
                        {
                            n1 = (((Mathf.CeilToInt((float)id / (float)column) + 1) % 2) * -1) + id + column + 1;
                        }
                        else
                        {
                            n1 = -1;
                        }
                    }
                }
                else
                {
                    n1 = -1;
                }
                neigbours.Add(n1);

                int n2; //right
                if (id % column != 0) //if tile is on the far right
                {
                    n2 = id + 1;
                }
                else
                {
                    n2 = -1;
                }
                neigbours.Add(n2);

                int n3; //bottom right
                if (Mathf.CeilToInt((float)id / (float)column) != 1) // if tile is not on the bottom row
                {
                    if (id % column != 0) //if tile is not on the far right
                    {
                        n3 = (((Mathf.CeilToInt((float)id / (float)column) + 1) % 2) * -1) + id - column + 1;
                    }
                    else
                    {
                        if (Mathf.CeilToInt((float)id / (float)column) % 2 == 0) // if tile is on even row
                        {
                            n3 = (((Mathf.CeilToInt((float)id / (float)column) + 1) % 2) * -1) + id - column + 1;
                        }
                        else
                        {
                            n3 = -1;
                        }
                    }
                }
                else
                {
                    n3 = -1;
                }
                neigbours.Add(n3);

                int n4; //bottom left
                if (Mathf.CeilToInt((float)id / (float)column) != 1) // if tile is not on the bottom row
                {
                    if (id % column != 1) // if tile is not on the far left
                    {
                        n4 = (((Mathf.CeilToInt((float)id / (float)column) + 1) % 2) * -1) + id - column;
                    }
                    else
                    {
                        if ((Mathf.CeilToInt((float)id / (float)column) + 1) % 2 == 0) // if tile is on odd row
                        {
                            n4 = (((Mathf.CeilToInt((float)id / (float)column) + 1) % 2) * -1) + id - column;
                        }
                        else
                        {
                            n4 = -1;
                        }
                    }
                }
                else
                {
                    n4 = -1;
                }
                neigbours.Add(n4);

                int n5; //left
                if (id % column != 1) // if tile is not on the far left
                {
                    n5 = id - 1;
                }
                else
                {
                    n5 = -1;
                }
                neigbours.Add(n5);

                int n6; //top left
                if (Mathf.CeilToInt((float)id / (float)column) != row) //if tile is not on the top row
                {
                    if (id % column != 1) // if tile is not on the far left
                    {
                        n6 = (((Mathf.CeilToInt((float)id / (float)column) + 1) % 2) * -1) + id + column;
                    }
                    else
                    {
                        if ((Mathf.CeilToInt((float)id / (float)column) + 1) % 2 == 0) // if tile is on odd row
                        {
                            n6 = (((Mathf.CeilToInt((float)id / (float)column) + 1) % 2) * -1) + id + column;
                        }
                        else
                        {
                            n6 = -1;
                        }
                    }
                }
                else
                {
                    n6 = -1;
                }
                neigbours.Add(n6);
            } //PointyTop
            else //FlatTop
            {
                int n1; //top
                if (id % row != 0) //if tile is not on the top row
                {
                    n1 = id + 1;
                }
                else
                {
                    n1 = -1;
                }
                neigbours.Add(n1);

                int n2; //top right
                if (Mathf.CeilToInt((float)id / (float)row) != column) // if tile is not on the right
                {
                    if ((Mathf.CeilToInt((float)id / (float)row) + 1) % 2 == 0) //if tile is on odd column
                    {
                        if (id % row != 0) //if tile is not on the far top
                        {
                            n2 = ((Mathf.CeilToInt((float)id / (float)row)) * row) + (id % row) + 1;
                        }
                        else
                        {
                            n2 = -1;
                        }
                        //n2 = 66;
                    }
                    else //if tile is on even column
                    {
                        if (id % row != 0) //if tile is not on the far top
                        {
                            n2 = ((Mathf.CeilToInt((float)id / (float)row)) * row) + (id % row);
                        }
                        else
                        {
                            n2 = ((Mathf.CeilToInt((float)id / (float)row) + 1) * row) + (id % row);
                        }
                    }
                }
                else //most right
                {
                    n2 = -1;
                }
                neigbours.Add(n2);

                int n3; //bottom right
                if (Mathf.CeilToInt((float)id / (float)row) != column) // if tile is not on the right
                {
                    if ((Mathf.CeilToInt((float)id / (float)row) + 1) % 2 == 0) //if tile is on odd column
                    {
                        n3 = ((Mathf.CeilToInt((float)id / (float)row)) * row) + (id % row);
                    }
                    else //if tile is on even column
                    {
                        if (id % row != 1) //if tile is not on the far bottom
                        {
                            n3 = ((Mathf.CeilToInt((float)id / (float)row)) * row) + (id % row) - 1;
                        }
                        else
                        {
                            n3 = -1;
                        }
                    }
                }
                else //most right
                {
                    n3 = -1;
                }
                neigbours.Add(n3);

                int n4; //bottom
                if (id % row != 1) //if tile is not on the bottom row
                {
                    n4 = id - 1;
                }
                else
                {
                    n4 = -1;
                }
                neigbours.Add(n4);

                int n5; //bottom left
                if (Mathf.CeilToInt((float)id / (float)row) != 1) // if tile is not on the left
                {
                    if ((Mathf.CeilToInt((float)id / (float)row) + 1) % 2 == 0) //if tile is on odd column
                    {
                        n5 = ((Mathf.FloorToInt((float)id / (float)row) - 1) * row) + (id % row);
                    }
                    else //if tile is on even column
                    {
                        if (id % row != 1) //if tile is not on the far bottom
                        {
                            n5 = ((Mathf.FloorToInt((float)id / (float)row) - 1) * row) + (id % row) - 1;
                        }
                        else
                        {
                            n5 = -1;
                        }
                    }
                }
                else //most left
                {
                    n5 = -1;
                }
                neigbours.Add(n5);

                int n6; //bottom left
                if (Mathf.CeilToInt((float)id / (float)row) != 1) // if tile is not on the left
                {
                    if ((Mathf.CeilToInt((float)id / (float)row) + 1) % 2 == 0) //if tile is on odd column
                    {
                        if (id % row != 0) //if tile is not on the far bottom
                        {
                            n6 = ((Mathf.FloorToInt((float)id / (float)row) - 1) * row) + (id % row) + 1;
                        }
                        else
                        {
                            n6 = -1;
                        }
                    }
                    else //if tile is on even column
                    {
                        n6 = ((Mathf.FloorToInt((float)id / (float)row) - 1) * row) + (id % row);
                    }
                }
                else //most left
                {
                    n6 = -1;
                }
                neigbours.Add(n6);
            } 
        }

        public void Fall()
        {
            StartCoroutine(FallCo());
        }

        private IEnumerator FallCo()
        {
            yield return new WaitForSeconds(fallDelay);
            while(transform.position.y > destroyHeight)
            { 
                transform.Translate(0, 0,-fallSpeed * Time.deltaTime);
                yield return null;
            }
            Destroy(gameObject);
        }
    }
}