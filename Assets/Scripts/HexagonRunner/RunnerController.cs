using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Gameplay;

public class RunnerController : MonoBehaviour
{
    [Header("Button")] //temp
    public Button leftBtn;
    public Button rightBtn;

    [Header("Component")]
    public Rigidbody rigidB;
    public Transform model;
    public TileManager tileManager;
    public RunnerGenerator runnerGenerator;

    [Header("Variable")]
    public float hopDuration;
    public AnimationCurve animCurve;
    public int maxBufferInput;
    public float fallSpeed;
    public float deathHeight;

    //private
    private Coroutine hopCo;
    private Tile currentTile;
    private List<Direction> hopBuffer;
    private bool isfalling = true;
    private List<GameObject> tiles = new List<GameObject>();

    public enum Direction
    {
        Left,
        Right
    }

    private void Start()
    {
        leftBtn.onClick.AddListener(()=> AddHopInput(Direction.Left));
        rightBtn.onClick.AddListener(() => AddHopInput(Direction.Right));
        ResetPos();
        hopBuffer = new List<Direction>();
    }

    private void Update()
    { 
        
        BufferCheck();

        if (isfalling)
        {
            Fall();
            PlayerFallCheck();
        }
        else
        {
            SetToPlane();
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                AddHopInput(Direction.Right);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                AddHopInput(Direction.Left);
            }
        }
    }

    private void BufferCheck()
    {
        if(hopBuffer.Count > 0 && hopCo == null && !isfalling)
        {
            Hop(hopBuffer[0]);
        }
    }

    private void Hop(Direction direction)
    {
        var isPointy = tileManager.orientation == TileManager.Orientation.PointyTop;
        var tileSize = tileManager.tileSize;
        var orientationSize = isPointy ? new Vector2(tileSize.x, tileSize.y) : new Vector2(tileSize.y, tileSize.x);
        var hop = direction == Direction.Right?
            orientationSize : new Vector2(orientationSize.x * -1, orientationSize.y);
        var b = rigidB.transform.position;
        var desination =  isPointy?
            new Vector3(b.x + hop.x / 2, b.y, b.z + hop.y) :
            new Vector3(b.x + hop.x, b.y, b.z + hop.y / 2);
        hopCo = StartCoroutine(HopAnim(desination));
    }

    private IEnumerator HopAnim(Vector3 pos)
    {
        var startPos = rigidB.transform.position;
        var startForward = model.transform.forward;
        var dir = pos - startPos;
        float timer = 0;
        float val = 0;
        while(timer < 1)
        {
            timer += Time.deltaTime / hopDuration;
            val = animCurve.Evaluate(timer);
            rigidB.transform.position = Vector3.Lerp(startPos, pos, val);
            model.transform.forward = Vector3.Lerp(startForward, dir, val);
            yield return null;
        }
        rigidB.transform.position = pos;
        model.transform.forward = dir;
        hopCo = null;
        hopBuffer.RemoveAt(0);
    }

    private void AddHopInput(Direction direction)
    {
        if(hopBuffer.Count < maxBufferInput)
        {
            hopBuffer.Add(direction);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        //MinePlay.OpenTile(collider.gameObject);
        if (collider.tag == TileManager.TILE)
        {
            tiles.Add(collider.gameObject);
            CheckTiles();
            isfalling = false;
            var t = collider.GetComponent<Tile>();
            if (t.isFallable)
            {
                t.Fall();
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == TileManager.TILE)
        {
            tiles.Remove(collider.gameObject);
            if (tiles.Count <= 0)
            {
                isfalling = true;
            }
        }
    }

    private void CheckTiles()
    {
        List<int> removeList = new List<int>();
        for (int i = 0; i < tiles.Count; i++)
        {
            if (tiles[i] == null)
            {
                removeList.Add(i);
            }
        }
        foreach (int i in removeList)
        {
            tiles.RemoveAt(i);
        }
    }

    public void SetToPlane()
    {
        rigidB.position = new Vector3(transform.position.x, TileManager.startTile.transform.position.y, transform.position.z);
    }

    public void Fall()
    {
        rigidB.transform.Translate(0, -fallSpeed * Time.deltaTime, 0);
    }

    public void ResetPos()
    {
        rigidB.transform.position = TileManager.startTile.transform.position;
    }

    private void PlayerFallCheck()
    {
        if (rigidB.transform.position.y <= deathHeight)
        {
            ResetGame();
        }
    }

    private void ResetGame()
    {
        tileManager.Init();
        runnerGenerator.GeneratePath();
        ResetPos();
    }
}
