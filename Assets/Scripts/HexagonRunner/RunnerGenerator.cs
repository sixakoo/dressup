using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Gameplay
{
    public class RunnerGenerator : MonoBehaviour
    {
        [Range(0, 100)]
        public int removePercentage;
        public TileManager tileManager;
        public int[] indexOptions;

        private List<Tile> pathTiles;

        private void Start()
        {
            GeneratePath();
        }

        public void GeneratePath()
        {
            pathTiles = new List<Tile>();
            Tile tileIndex = TileManager.startTile;
            pathTiles.Add(TileManager.startTile);
            while (tileIndex != null)
            {
                List<int> options = new List<int>();
                for (int i = 0; i < indexOptions.Length; i++)
                {
                    var item = tileIndex.neigbours[indexOptions[i]];
                    if (item != -1)
                    {
                        options.Add(item);
                    }
                }
                //if options are at least 1 available
                if (options.Count() > 0)
                {
                    tileIndex = tileManager.tileDict[options[Random.Range(0, options.Count)]];
                    //tileIndex.SetTileColor(Color.white);
                    pathTiles.Add(tileIndex);
                }
                else //none available
                {
                    tileIndex = null;
                }
            }
            RemoveTiles();
        }

        private void RemoveTiles()
        {
            List<KeyValuePair<GameObject, Tile>> tempList = tileManager.tileObjectDict.ToList();
            var removeCount = ((float)removePercentage / 100f) * tileManager.tileDict.Count;
            for (int i = 0; i < removeCount; i++)
            {
                int index = Random.Range(0, tempList.Count);
                var remove = tempList[index];
                if (!pathTiles.Contains(remove.Value))
                {
                    Debug.Log(remove.Key.gameObject);
                    Destroy(remove.Key.gameObject);
                }
                tempList.RemoveAt(index);
            }
        }
    }
}