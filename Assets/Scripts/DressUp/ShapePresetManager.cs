using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShapeKeyManager", menuName = "ScriptableObjects/ShapeKeyManager", order = 1)]
public class ShapePresetManager : ScriptableObject
{
    [System.Serializable]
    public class ShapeData
    {
        public string shapeName;
        public string shapeCategory;
        public ShapeKey[] shapeKeys;
    }

    public List<ShapeData> ShapeList;

}
