using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Character : MonoBehaviour
{
    [Header("Component")]
    public SkinnedMeshRenderer face;
    public SkinnedMeshRenderer eyelashes;
    public SkinnedMeshRenderer eyebrows;
    public SkinnedMeshRenderer eyes;
    public SkinnedMeshRenderer skin;
    public BodyPart[] bodyParts;
    public BodyPart[] otherParts;
    public Color[] browColors;
    public Color[] eyeColors;
    public Color[] skinColors;

    //Private
    private int[] makeupLayoutIndex = new int[] { -1, -1, -1 };
    private int[] makeupItemIndex = new int[] { -1, -1, -1 };

    //Const
    public const string FACESHAPE = "faceshape";
    public const string MAKEUPITEM = "makeupitem";
    public const string MAKEUPLAYOUT = "makeuplayout";
    public const string COLOR = "color";
    public const string SPLIT = ":";
    public const string EYEMAKEUP = "_EyeMakeup";
    public const string EYEMAKEUPCOLOR = "_EyeMakeupColor";
    public const string EYEMAKEUPMETALLIC = "_EyeMakeupMetallic";
    public const string EYEMAKEUPSMOOTHNESS = "_EyeMakeupSmoothness";
    public const string BLUSHER = "_Blusher";
    public const string BLUSHERCOLOR = "_BlusherColor";
    public const string BLUSHERMETALLIC = "_BlusherMetallic";
    public const string BLUSHERSMOOTHNESS = "_BlusherSmoothness";
    public const string LIPSTICK = "_Lipstick";
    public const string LIPSTICKCOLOR = "_LipstickColor";
    public const string LIPSTICKMETALLIC = "_LipstickMetallic";
    public const string ELIPSTICKSMOOTHNESS = "_LipstickSmoothness";
    private const string SEPARATOR = "_";

    public enum ColorizeParts
    {
        Eyes,
        Brows,
        Lashes,
        Skin,
        Hair,
        Upper,
        Lower,
        Shoes
    }

    [System.Serializable]
    public class BodyPart
    {
        public OutfitData.Parts part;
        public SkinnedMeshRenderer skin;
    }

    public void Reset()
    {
        face = transform.Find(OutfitData.Parts.Head.ToString()).GetComponent<SkinnedMeshRenderer>();
        eyelashes = transform.Find(OutfitData.Parts.EyeLashes.ToString()).GetComponent<SkinnedMeshRenderer>();
        eyebrows = transform.Find(OutfitData.Parts.Brows.ToString()).GetComponent<SkinnedMeshRenderer>();
        eyes = transform.Find(OutfitData.Parts.Eyes.ToString()).GetComponent<SkinnedMeshRenderer>();
        skin = transform.Find(OutfitData.Parts.Torso.ToString()).GetComponent<SkinnedMeshRenderer>();
        var parts = System.Enum.GetValues(typeof(OutfitData.Parts));
        bodyParts = new BodyPart[parts.Length];
        int i = 0;
        foreach (var part in parts)
        {
            bodyParts[i] = new BodyPart() { part = (OutfitData.Parts)part, skin = transform.Find(part.ToString()).GetComponent<SkinnedMeshRenderer>()};
            i++;
        }
        otherParts = new BodyPart[5];
        i = 0;
        OutfitData.Parts[] op = new OutfitData.Parts[] { OutfitData.Parts.Brows, OutfitData.Parts.EyeLashes, OutfitData.Parts.Eyes, OutfitData.Parts.Head, OutfitData.Parts.Teeth };
        foreach (var part in op)
        {
            otherParts[i] = new BodyPart() { part = part, skin = transform.Find(part.ToString()).GetComponent<SkinnedMeshRenderer>() };
            i++;
        }
    }

    public void SetFaceShape(int index, float shape)
    {
        if (index < face.sharedMesh.blendShapeCount)
        {
            face.SetBlendShapeWeight(index, shape);
        }
        if (index < eyelashes.sharedMesh.blendShapeCount)
        {
            eyelashes.SetBlendShapeWeight(index, shape);
        }
    }

    public void SetFaceShapes(ShapeKey[] shapeKeys)
    {
        foreach (var shapeKey in shapeKeys)
        {
            SetFaceShape(shapeKey.key, shapeKey.shape);
        }
        GameMaster.user.faceKeys = shapeKeys.ToList();
    }

    public void ToggleFaceShape(int index, float shape, string category)
    {
        var bs = face.sharedMesh;
        for (int i = 0; i < bs.blendShapeCount; i++)
        {
            string s = bs.GetBlendShapeName(i);
            if (i == index)
            {
                SetFaceShape(index, shape);
            }
            else if (s.Contains(category))
            {
                SetFaceShape(i, 0);
            }
        }
        SaveFaceShape();
    }

    public static ShapeKey[] CreateFaceParts(string category)
    {
        Mesh face = GameMaster.controller.customizationController.character.face.sharedMesh;
        List<ShapeKey> shapeList = new List<ShapeKey>();
        shapeList.Add(new ShapeKey(-1, 0, category));
        for (int i = 0; i < face.blendShapeCount; i++)
        {
            //create default 
            string s = face.GetBlendShapeName(i);
            if (s.Contains(category))
            {
                shapeList.Add(new ShapeKey(i, 100, s.Split(SEPARATOR.ToCharArray())[0]));
            }
        }
        return shapeList.ToArray();
    }

    public void SaveFaceShape()
    {
        List<string> data = new List<string>();
        for (int i = 0; i < face.sharedMesh.blendShapeCount; i++)
        {
            data.Add(face.sharedMesh.GetBlendShapeName(i) + SPLIT + face.GetBlendShapeWeight(i));
        }
        SaveManager.SaveStringList(FACESHAPE, data);
    }

    public void LoadFaceShape()
    {
        List<string> datas = SaveManager.LoadStringList(FACESHAPE);
        foreach (string data in datas)
        {
            string[] arr = data.Split(new string[] { SPLIT }, System.StringSplitOptions.RemoveEmptyEntries);
            face.SetBlendShapeWeight(face.sharedMesh.GetBlendShapeIndex(arr[0]), float.Parse(arr[1]));
            int lashIndex = eyelashes.sharedMesh.GetBlendShapeIndex(arr[0]);
            if (lashIndex != -1 && lashIndex < eyelashes.sharedMesh.blendShapeCount)
            {
                eyelashes.SetBlendShapeWeight(lashIndex, float.Parse(arr[1]));
            }

        }
    }

    public void SaveColors()
    {
        List<string> data = new List<string>();
        foreach (int indexColor in GameMaster.user.colors)
        {
            data.Add(indexColor.ToString());
        }
        SaveManager.SaveStringList(COLOR, data);
    }

    public void LoadColors()
    {
        List<string> datas = SaveManager.LoadStringList(COLOR);
        for (int i = 0; i < GameMaster.user.colors.Length; i++)
        {
            if(i >= datas.Count)
            {
                GameMaster.user.colors[i] = 0;
                AssignColors(i, 0);
            }
            else
            {
                int colorIndex = int.Parse(datas[i]);
                GameMaster.user.colors[i] = colorIndex;
                AssignColors(i, colorIndex);
            }
        }
    }

    public void ChangeColor(ColorizeParts facePart, int colorIndex)
    {
        switch (facePart)
        {
            case ColorizeParts.Eyes:
                GameMaster.user.colors[0] = colorIndex;
                eyes.materials[1].color = eyeColors[colorIndex];
                break;
            case ColorizeParts.Brows:
                GameMaster.user.colors[1] = colorIndex;
                eyebrows.material.color = browColors[colorIndex];
                break;
            case ColorizeParts.Skin:
                GameMaster.user.colors[2] = colorIndex;
                skin.material.color = skinColors[colorIndex];
                face.material.color = skinColors[colorIndex];
                break;
            case ColorizeParts.Hair:
                var hair = GameMaster.controller.customizationController;
                try
                {
                    var headOutfit = hair.currentOutfits.Single(x => x.data.outfitType == OutfitData.OufitType.Head);
                    GameMaster.controller.customizationController.ChangeOutfitColor(headOutfit, colorIndex);
                }
                catch { }
                break;
            case ColorizeParts.Upper:
                var upper = GameMaster.controller.customizationController;
                try
                {
                    var upperOutfit = upper.currentOutfits.Single(x => x.data.outfitType == OutfitData.OufitType.Upper);
                    GameMaster.controller.customizationController.ChangeOutfitColor(upperOutfit, colorIndex);
                }
                catch { }
                break;
            case ColorizeParts.Lower:
                var lower = GameMaster.controller.customizationController;
                try
                {
                    var lowerOutfit = lower.currentOutfits.Single(x => x.data.outfitType == OutfitData.OufitType.Lower);
                    GameMaster.controller.customizationController.ChangeOutfitColor(lowerOutfit, colorIndex);
                }
                catch { }
                break;
            case ColorizeParts.Shoes:
                var shoes = GameMaster.controller.customizationController;
                try
                {
                    var shoesOutfit = shoes.currentOutfits.Single(x => x.data.outfitType == OutfitData.OufitType.Shoes);
                    GameMaster.controller.customizationController.ChangeOutfitColor(shoesOutfit, colorIndex);
                }
                catch { }
                break;
            default:
                break;
        }
        SaveColors();
    }

    public void AssignColors(int index, int colorIndex)
    {
        switch (index)
        {
            case 0:
                eyes.materials[1].color = eyeColors[colorIndex];
                    break;
            case 1:
                eyebrows.material.color = browColors[colorIndex];
                break;
            case 2:
                skin.material.color = skinColors[colorIndex];
                face.material.color = skinColors[colorIndex];
                break;
            default:
                break;
        }
    }

    public void ClearMakeup(MakeupData.Part makeupData)
    {
        makeupItemIndex[(int)makeupData] = -1;
        makeupLayoutIndex[(int)makeupData] = -1;
        switch (makeupData)
        {
            case MakeupData.Part.EyeMakeup:
                face.material.SetTexture(EYEMAKEUP, GameMaster.controller.itemManager.emptyTex);
                break;
            case MakeupData.Part.Blusher:
                face.material.SetTexture(BLUSHER, GameMaster.controller.itemManager.emptyTex);
                break;
            case MakeupData.Part.Lipstick:
                face.material.SetTexture(LIPSTICK, GameMaster.controller.itemManager.emptyTex);
                break;
            default:
                break;
        }
    }

    public void InitMakeup()
    {
        GameMaster.controller.itemManager.AssignMakeupIndexes();
        if (!LoadMakeup()) //load default if no key
        {
            ClearMakeup(MakeupData.Part.EyeMakeup);
            ClearMakeup(MakeupData.Part.Blusher);
            ClearMakeup(MakeupData.Part.Lipstick);
        }
    }

    public void ApplyMakeup(MakeupData makeupData, MakeupData.Part clearPart = MakeupData.Part.None)
    {
        if(makeupData == null)
        {
            ClearMakeup(clearPart);
            return;
        }
        if (makeupLayoutIndex[(int)makeupData.part] == -1)
        {
            ChangeLayout(makeupData.part, 0);
            SaveMakeupLayout();
        }

        switch (makeupData.part)
        {
            case MakeupData.Part.EyeMakeup:
                face.material.SetColor(EYEMAKEUPCOLOR, makeupData.color);
                face.material.SetFloat(EYEMAKEUPMETALLIC, makeupData.metallic);
                face.material.SetFloat(EYEMAKEUPSMOOTHNESS, makeupData.smoothness);
                makeupItemIndex[0] = makeupData.index;
                break;
            case MakeupData.Part.Blusher:
                face.material.SetColor(BLUSHERCOLOR, makeupData.color);
                face.material.SetFloat(BLUSHERMETALLIC, makeupData.metallic);
                face.material.SetFloat(BLUSHERSMOOTHNESS, makeupData.smoothness);
                makeupItemIndex[1] = makeupData.index;
                break;
            case MakeupData.Part.Lipstick:
                face.material.SetColor(LIPSTICKCOLOR, makeupData.color);
                face.material.SetFloat(LIPSTICKMETALLIC, makeupData.metallic);
                face.material.SetFloat(ELIPSTICKSMOOTHNESS, makeupData.smoothness);
                makeupItemIndex[2] = makeupData.index;
                break;
            default:
                break;
        }
    }

    public void ChangeLayout(MakeupData.Part part, int index)
    {
        makeupLayoutIndex[(int)part] = index;
        switch (part)
        {
            case MakeupData.Part.EyeMakeup:
                if(index == -1)
                {
                    face.material.SetTexture(EYEMAKEUP, GameMaster.controller.itemManager.emptyTex);
                    
                    return;
                }
                if (makeupItemIndex[(int)part] == -1)
                {
                    ApplyMakeup(GameMaster.controller.itemManager.makeupDatas.First(x => x.part == MakeupData.Part.EyeMakeup));
                    SaveMakeupItems();
                }
                face.material.SetTexture(EYEMAKEUP, GameMaster.controller.itemManager.eyemakeUpLayout[index]);
                break;
            case MakeupData.Part.Blusher:
                if (index == -1)
                {
                    face.material.SetTexture(BLUSHER, GameMaster.controller.itemManager.emptyTex);
                    return;
                }
                if (makeupItemIndex[(int)part] == -1)
                {
                    ApplyMakeup(GameMaster.controller.itemManager.makeupDatas.First(x => x.part == MakeupData.Part.Blusher));
                    SaveMakeupItems();
                }
                face.material.SetTexture(BLUSHER, GameMaster.controller.itemManager.blushLayout[index]);
                break;
            case MakeupData.Part.Lipstick:
                if (index == -1)
                {
                    face.material.SetTexture(LIPSTICK, GameMaster.controller.itemManager.emptyTex);
                    return;
                }
                if (makeupItemIndex[(int)part] == -1)
                {
                    ApplyMakeup(GameMaster.controller.itemManager.makeupDatas.First(x => x.part == MakeupData.Part.Lipstick));
                    SaveMakeupItems();
                }
                face.material.SetTexture(LIPSTICK, GameMaster.controller.itemManager.lipstickLayout[index]);
                break;
            default:
                break;
        }
    }

    public void SaveMakeupItems()
    {
        SaveManager.SaveFloatInt(MAKEUPITEM, makeupItemIndex.ToList());
    }

    public void SaveMakeupLayout()
    {
        SaveManager.SaveFloatInt(MAKEUPLAYOUT, makeupLayoutIndex.ToList());
    }

    public bool LoadMakeup()
    {
        bool isLoaded = false;
        List<int> layout = null;
        if(PlayerPrefs.HasKey(MAKEUPITEM)) //assign indexes
        {
            var makeup = SaveManager.LoadIntList(MAKEUPITEM);
            makeupItemIndex = makeup.ToArray();
            
            isLoaded = true;
        }
        else
        {
            //Debug.LogError("no makeup key");
        }

        if (PlayerPrefs.HasKey(MAKEUPLAYOUT)) //assign indexes
        {
            layout = SaveManager.LoadIntList(MAKEUPLAYOUT);
            makeupLayoutIndex = layout.ToArray();
            
            isLoaded = true;
        }
        else
        {
            //Debug.LogError("no layout key");
        }

        //Assign makeup
        for (int i = 0; i < makeupItemIndex.Length; i++)
        {
            if (makeupItemIndex[i] != -1)
            {
                ApplyMakeup(GameMaster.controller.itemManager.makeupDatas[makeupItemIndex[i]]);
            }
            else
            {
                var part = (MakeupData.Part)i;
                ClearMakeup(part);
            }
        }

        //Assign layout
        int j = 0;
        if(layout != null)
        {
            foreach (var part in System.Enum.GetValues(typeof(MakeupData.Part)))
            {
                if (j < layout.Count)
                {
                    ChangeLayout((MakeupData.Part)part, layout[j]);
                }
                j++;
            }
        }
        
        return isLoaded;
    }
}


