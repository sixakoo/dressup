using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinConst
{
    //Face
    public const string EYE2 = "Eye_2";
    public const string EYE3 = "Eye_3";
    public const string NOSE2 = "Nose_2";
    public const string NOSE3 = "Nose_3";
    public const string MOUTH2 = "Mouth_2";
    public const string MOUTH3 = "Mouth_3";
}
