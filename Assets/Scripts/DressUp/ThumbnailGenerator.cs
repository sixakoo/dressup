using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ThumbnailGenerator : MonoBehaviour
{
    public MakeupData previewEyeMakeup;
    public MakeupData previewBlusher;
    public MakeupData previewLipstick;
    public OutfitData upperPreview;
    public OutfitData lowerPreview;
    public OutfitData shoePreview;
    public string dir;
    public Image[] captureAreas;
    public Vector3 manualCamOffset;

    //Const
    public const string OUTFITS = "outfits";
    public const string ACCESSORIES = "accessories";

    //0.Eyes
    //1.Nose
    //2.Mouth
    //3.Hair
    //4.Face
    //5.Upper
    //6.Lower
    //7.Shoes

    private IEnumerator Start()
    {
        HideAllCaptureRects();
        yield return new WaitForSeconds(.1f);
        GameMaster.controller.customizationController.character.ChangeColor(Character.ColorizeParts.Skin, 0);
        GameMaster.skeleton.animator.Static();
        GameMaster.skeleton.animator.Normal();
        yield return new WaitForSeconds(2.2f);
        GameMaster.skeleton.animator.animator.enabled = false;

        StartCoroutine(GenerateThumbnails());

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(GenerateThumbnails());
        }
    }

    public IEnumerator GenerateThumbnails()
    {
        Debug.Log("Generating Thumbnails");
        ResetCharacter();

        //ShapeKeys
        DefaultFace();
        ClearAllMakeUp();
        GameMaster.controller.cameraController.SwitchCameraAngle(View_Bar.Page.Customize, true);
        foreach (var category in System.Enum.GetValues(typeof(View_Face.FaceCategory)))
        {
            int i = 0;
            var c = (View_Face.FaceCategory)category;
            var shapes = Character.CreateFaceParts(c.ToString());
            foreach (var part in shapes)
            {
                yield return null;
                GameMaster.controller.customizationController.character.ToggleFaceShape(part.key, part.shape, part.name);
                StartCoroutine(Screenshot(
                    c == View_Face.FaceCategory.Eyes ? captureAreas[0].rectTransform :
                    c == View_Face.FaceCategory.Nose ? captureAreas[1].rectTransform : captureAreas[2].rectTransform, part.name + (i + 1)));
                i++;
            }
        }

        //LayoutMakeup
        DefaultFace();
        foreach (var category in System.Enum.GetValues(typeof(MakeupData.Part)))
        {
            var c = (MakeupData.Part)category;
            if (c == MakeupData.Part.None) { continue; }
            for (int i = 0; i < GameMaster.controller.itemManager.GetMakeUpLayoutCount(c); i++)
            {
                switch (c)
                {
                    case MakeupData.Part.EyeMakeup:
                        GameMaster.controller.customizationController.character.ApplyMakeup(previewEyeMakeup);
                        break;
                    case MakeupData.Part.Blusher:
                        GameMaster.controller.customizationController.character.ApplyMakeup(previewBlusher);
                        break;
                    case MakeupData.Part.Lipstick:
                        GameMaster.controller.customizationController.character.ApplyMakeup(previewLipstick);
                        break;
                }
                GameMaster.controller.customizationController.character.ChangeLayout(c, i);
                StartCoroutine(Screenshot(
                        c == MakeupData.Part.EyeMakeup ? captureAreas[0].rectTransform :
                        c == MakeupData.Part.Blusher ? captureAreas[4].rectTransform : captureAreas[2].rectTransform, category.ToString() + (i + 1)));
                yield return null;
                GameMaster.controller.customizationController.character.ApplyMakeup(null, c);
            }
        }

        //Outfit
        foreach (var category in System.Enum.GetValues(typeof(OutfitData.OufitType)))
        {
            RectTransform rt = null;
            var c = (OutfitData.OufitType)category;
            switch (c)
            {
                case OutfitData.OufitType.Accessories:
                    continue;
                case OutfitData.OufitType.None:
                    continue;
                case OutfitData.OufitType.Head:
                    ResetCharacter();
                    GameMaster.controller.cameraController.SwitchCameraAngle(View_Bar.Page.Customize, true);
                    rt = captureAreas[3].rectTransform;
                    break;
                case OutfitData.OufitType.Upper:
                    ResetCharacter();
                    GameMaster.controller.cameraController.SwitchCameraAngle(View_Bar.Page.Outfit, true);
                    GameMaster.controller.customizationController.ToggleTuckIn(false);
                    rt = captureAreas[5].rectTransform;
                    break;
                case OutfitData.OufitType.Lower:
                    ResetCharacter();
                    GameMaster.controller.cameraController.SwitchCameraAngle(View_Bar.Page.Outfit, true);
                    GameMaster.controller.customizationController.ToggleTuckIn(true);
                    rt = captureAreas[6].rectTransform;
                    break;
                case OutfitData.OufitType.Shoes:
                    ResetCharacter();
                    GameMaster.controller.cameraController.SwitchCameraAngle(View_Bar.Page.Outfit, true);
                    rt = captureAreas[7].rectTransform;
                    break;
            }
            var items = GameMaster.controller.itemManager.outfitDatas.Where(x => x.outfitType == c).ToList();
            int i = 1;
            foreach (var item in items)
            {
                GameMaster.controller.customizationController.AssignOutfit(item);
                StartCoroutine(Screenshot(rt, item.outfitName));
                yield return null;
                i++;
            }
            
        }
        ////Accessories
        //var accessories = GameMaster.controller.itemManager.outfitDatas.Where(x => x.outfitType == OutfitData.OufitType.Accessories).ToList();
        //foreach (var a in accessories)
        //{
        //    ResetCharacter();
        //    GameMaster.controller.customizationController.AssignAccessory(a);
        //    yield return null;
        //    var accessory = GameMaster.user.GetAccessory(a);
        //    Debug.LogError("a : " + a.outfitName);
        //    var bone = GameMaster.skeleton.FindBone(accessory.data.bone);
        //    yield return new WaitForEndOfFrame();
        //    var pos = ApplyAccessoryOffset(bone, accessory.data.offset + manualCamOffset);
        //    GameMaster.controller.cameraController.SetCameraAngle(pos, bone, 60);
        //    yield return new WaitForSeconds(1);
        //}
        ResetCharacter();
        yield return null;
        Debug.Log("-------Task Completed-------");
    }

    private void ClearAllMakeUp()
    {
        foreach (var category in System.Enum.GetValues(typeof(MakeupData.Part)))
        {
            var c = (MakeupData.Part)category;
            if (c == MakeupData.Part.None) { continue; }
            GameMaster.controller.customizationController.character.ApplyMakeup(null, c);
        }
    } 

    private void DefaultFace()
    {
        foreach (var category in System.Enum.GetValues(typeof(View_Face.FaceCategory)))
        {
            int i = 0;
            var c = (View_Face.FaceCategory)category;
            var shapes = Character.CreateFaceParts(c.ToString());
            GameMaster.controller.customizationController.character.ToggleFaceShape(shapes[0].key, shapes[0].shape, shapes[0].name);
        }
    }

    private void HideAllCaptureRects()
    {
        foreach (var item in captureAreas)
        {
            item.enabled = false;
        }
    }

    private IEnumerator Screenshot(RectTransform rectTransform, string filename)
    {
        yield return new WaitForEndOfFrame();

        var screenPos = CanvasController.UIToScreenPos(rectTransform.anchoredPosition);
        var screenSize = CanvasController.UIToScreenPos(rectTransform.sizeDelta);
        Texture2D tex = new Texture2D((int)screenSize.x, (int)screenSize.y, TextureFormat.RGB24, false);
        Rect rex = new Rect(screenPos.x, screenPos.y, screenSize.x, screenSize.y);

        tex.ReadPixels(rex, 0, 0);
        tex.Apply();
        var bytes = tex.EncodeToPNG();
        Destroy(tex);

        System.IO.File.WriteAllBytes(Application.dataPath + dir + filename + ".png", bytes);
        Debug.Log("Captured screenshot : " + Application.dataPath + dir + filename + ".png");
    }

    private void ResetCharacter()
    {
        GameMaster.controller.customizationController.user.ClearAllOutfits();
        if (upperPreview != null) { GameMaster.controller.customizationController.AssignOutfit(upperPreview); }
        if (lowerPreview != null) { GameMaster.controller.customizationController.AssignOutfit(lowerPreview); }
        if (shoePreview != null) { GameMaster.controller.customizationController.AssignOutfit(shoePreview); }
        GameMaster.controller.customizationController.ResetCharacter();
    }

    private Vector3 ApplyAccessoryOffset(Transform source, Vector3 offset)
    {
        var x = source.right * offset.x;
        var y = source.up * offset.y;
        var z = source.forward * offset.z;
        return source.position + x + y + z;
    }
}
