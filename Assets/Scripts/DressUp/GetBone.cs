using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetBone : MonoBehaviour
{
    public SkinnedMeshRenderer source;

    private void Start()
    {
        SkinnedMeshRenderer smr = source.GetComponent<SkinnedMeshRenderer>();
        Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();
        foreach (Transform bone in smr.bones)
        {
            boneMap[bone.gameObject.name] = bone;
        }

        SkinnedMeshRenderer targetRenderer = gameObject.GetComponent<SkinnedMeshRenderer>();
        Transform[] newBones = new Transform[targetRenderer.bones.Length];
        for(int i = 0; i < targetRenderer.bones.Length; i++)
        {
            GameObject bone = targetRenderer.bones[i].gameObject;
            if(!boneMap.TryGetValue(bone.name, out newBones[i]))
            {
                Debug.Log("Unable to map bone " + bone.name + " to get skeleton");
                break;
            }
        }
        Destroy(targetRenderer.rootBone.transform.parent.gameObject);
        targetRenderer.bones = newBones;
        var oldParent = targetRenderer.gameObject.transform.parent;
        targetRenderer.gameObject.transform.SetParent(source.transform.parent);
        targetRenderer.rootBone = source.rootBone;
        Destroy(oldParent.gameObject);
    }
}
