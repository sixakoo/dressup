using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShapeKey
{
    public int key;
    public float shape;
    public string name;

    public ShapeKey(int _key, float _shape, string _name)
    {
        key = _key;
        shape = _shape;
        name = _name;
    }
}
