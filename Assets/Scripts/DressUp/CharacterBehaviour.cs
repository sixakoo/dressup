using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

public class CharacterBehaviour : MonoBehaviour
{
    [Header("Components")]
    public Transform spoonPrefab;
    public Transform forkPrefab;
    public Transform platePrefab;
    public Vector3 foodOffset;
    public Vector3 foodRot;
    public Vector3 spoonOffset;
    public Vector3 spoonRot;
    public Vector3 forkOffset;
    public Vector3 forkRot;

    [Header("Behaviour")]
    public CharacterMeter energy;
    public CharacterMeter mood;
    public CharacterMeter hunger;
    public CharacterMeter clean;
    public CharacterMeter health;
    public CharacterAction currentAction;
    public CharacterMeter[] meters;

    public UnityAction<bool> onAction;

    public const float stressedTh = 25;
    public const float normalTh = 50;
    public const float tiredTh = 25;
    public const float sadTh = 36;
    public const float happyTh = 67;
    public const float faceMonitorInterval = 2;

    private Coroutine sequenceCo;
    private bool isEnd, isStopping;
    private float faceMonitorTimer;
    private Faces currentFace;
    private string lastEntry;
    private Vector3 lastTargetPos;
    private const string EAT = "Eat";
    [System.NonSerialized]
    private List<Transform> foodStuff;

    private enum Faces
    {
        Sad,
        Tired,
        Normal,
        Happy,
        Agitated
    }

    private void Awake()
    {
        meters = new CharacterMeter[] { energy, mood, hunger, clean, health };
    }

    private void Update()
    {
        energy.Update();
        mood.Update();
        hunger.Update();
        clean.Update();
        health.Update();
        EmotionMonitorUpdates();
    }

    public void Action(CharacterAction action, Transform target = null, string entryAnimation = "", List<Transform> customPos = null)
    {
        onAction(true);
        //string animationName = string.IsNullOrEmpty(animationOverride) ? action.animationName : animationOverride;
        List<string> actions = new List<string>();
        if(!string.IsNullOrEmpty(entryAnimation) && lastEntry != entryAnimation)
        {
            lastEntry = entryAnimation;
            actions.Add(entryAnimation);
        }
        actions.Add(action.animationName);
        GameMaster.skeleton.characterController.Turn(target.rotation.eulerAngles, () => GameMaster.skeleton.animator.Action(actions));
        currentAction = action;
        foreach (var meter in action.meters)
        {
            meters.Single(x => x.meterName == meter.meterName).ApplyEffect(meter, action.name);
        }
        if (action.name == EAT)
        {
            ShowFoodStuff(customPos);
        }
    }

    public void StopAction()
    {
        lastEntry = "";
        isStopping = true;
        if (currentAction != null)
        {
            if(currentAction.name == EAT)
            {
                HideFoodStuff();
            }
            foreach (var meter in currentAction.meters)
            {
                meters.Single(x => x.meterName == meter.meterName).ClearEffect(currentAction);
            }
            currentAction = null;
        }
        GameMaster.skeleton.animator.Stop();
        onAction(false);
    }

    public bool IsInAction()
    {
        if(currentAction != null)
        {
            return true;
        }
        return false;
    }

    public void Move(Vector3 target, UnityAction actionAfter)
    {
        lastTargetPos = target;
        GameMaster.skeleton.characterController.Move(target, actionAfter);
    }

    public void Sequence(Vector3 target, UnityAction actionAfter = null, string entryAnim = "")
    {
        if(sequenceCo != null)
        {
            StopCoroutine(sequenceCo);
        }
        sequenceCo = StartCoroutine(SequenceCo(target, actionAfter, entryAnim));
    }

    private IEnumerator SequenceCo(Vector3 target, UnityAction actionAfter, string entryAnim)
    {
        isEnd = false;
        if ((currentAction != null || isStopping) && lastTargetPos != target)
        {
            StopAction();
            yield return new WaitUntil(() => isEnd);
            isStopping = false;
        }
        Move(target, actionAfter);        
    }

    public void EndOfStop()
    {
        isEnd = true;
    }

    private void EmotionMonitorUpdates()
    {
        if(faceMonitorTimer > faceMonitorInterval)
        {
            faceMonitorTimer = 0;
            EmotionMonitor();
        }
        faceMonitorTimer += Time.deltaTime;
    }

    private void EmotionMonitor()
    {
        Faces face = currentFace;
        //Normal
        if (meters.Where(x=> x.val > stressedTh).Count() == meters.Length) //all more than stressedThreshold
        {
            if(mood.val > happyTh && meters.Where(x => x.val > normalTh).Count() == meters.Length) //all more than normalThreshold
            {
                face = Faces.Happy;
            }
            else
            {
                face = Faces.Normal;
            }
        }
        else //Stressed
        {
            if(energy.val < tiredTh)
            {
                face = Faces.Tired;
            }
            else if(mood.val < sadTh)
            {
                face = Faces.Sad;
            }
            else
            {
                face = Faces.Agitated;
            }
        }

        if (face == currentFace)
        {
            return;
        }
        currentFace = face;

        switch (face)
        {
            case Faces.Sad:
                GameMaster.skeleton.animator.Sad();
                break;
            case Faces.Tired:
                GameMaster.skeleton.animator.Tired();
                break;
            case Faces.Normal:
                GameMaster.skeleton.animator.Normal();
                break;
            case Faces.Happy:
                GameMaster.skeleton.animator.Happy();
                break;
            case Faces.Agitated:
                GameMaster.skeleton.animator.Agitated();
                break;
            default:
                break;
        }
    }

    private void ShowFoodStuff(List<Transform> customPos)
    {
        float nearest = float.MaxValue;
        Transform nearPos = null;
        for (int i = 0; i < customPos.Count; i++)
        {
            float distance = Vector3.Distance(customPos[i].position, GameMaster.skeleton.transform.position);
            if (distance < nearest)
            {
                nearest = distance;
                nearPos = customPos[i];
            }
        }
        if (foodStuff == null) //Init
        {
            foodStuff = new List<Transform>()
            {
                Instantiate(spoonPrefab),
                Instantiate(forkPrefab),
                Instantiate(platePrefab, transform),
            };
            foodStuff[0].SetParent(GameMaster.skeleton.rHand);
            foodStuff[0].localPosition = spoonOffset;
            foodStuff[0].localRotation = Quaternion.Euler(spoonRot);
            foodStuff[1].SetParent(GameMaster.skeleton.lHand);
            foodStuff[1].localPosition = forkOffset;
            foodStuff[1].localRotation = Quaternion.Euler(forkRot);
            
        }
        else
        {
            foodStuff.ForEach(x => x.gameObject.SetActive(true));
        }
        foodStuff[2].position = nearPos.position;
    }

    private void HideFoodStuff()
    {
        if (foodStuff != null)
        {
            foodStuff.ForEach(x => x.gameObject.SetActive(false));
        }
    }
}
