using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CanvasController : MonoBehaviour
{
    public RectTransform screen;
    public static CanvasController Instance;
    public EventSystem eventSystem;

    private void Awake()
    {
        if(Instance != null)
        {
            Debug.LogError("Another instance exist!");
        }
        Instance = this;
    }

    public static Vector2 MouseToUIPos()
    {
        Vector2 normalized = Input.mousePosition / new Vector2(Screen.width, Screen.height);
        return Instance.screen.sizeDelta * normalized;
    }

    public static Vector2 ScreenToUIPos(Vector3 screenPos)
    {
        Vector2 normalized = screenPos / new Vector2(Screen.width, Screen.height);
        return Instance.screen.sizeDelta * normalized;
    }

    public static Vector3 UIToScreenPos(Vector2 UIPos)
    {
        Vector2 normalized = UIPos / Instance.screen.sizeDelta;
        return new Vector3(Screen.width, Screen.height) * normalized;
    }

    public static bool IsOnUI()
    {
        return EventSystem.current.IsPointerOverGameObject() || EventSystem.current.IsPointerOverGameObject(0);
    }
}
