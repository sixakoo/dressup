using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Skeleton : MonoBehaviour
{
    public CharacterController characterController;
    public SkinnedMeshRenderer skin;
    public CharacterAnimator animator;    
    public Collider headCollider;
    public Collider upperCollider;
    public Collider lowerCollider;
    public Collider shoeCollider;
    public Collider browCollider;
    public Collider skinCollider;
    public Collider eyeCollider;
    public Collider hairCollider;
    public Transform eyesocketL;
    public Transform eyesocketR;
    public Transform nose;
    public Transform mouth;
    public Transform chin;
    public Transform lHand;
    public Transform rHand;
    public Transform root;
    [HideInInspector]
    public float[] initialFacePos;
    public const float sliderDistance = 0.001f;
    public const float eyeScale = 0.2f;
    public const float noseScale = 0.3f;
    public const float mouthScale = 0.2f;

    [HideInInspector]
    public CustomizationController customizationController;

    private Vector3 lEyePos;
    private Vector3 rEyePos;
    private Vector3 nosePos;
    private Vector3 mouthPos;
    private Vector3 chinPos;
    private float eyeSize;
    private float noseSize;
    private float mouthSize;

    private void Awake()
    {
        SetInitialPos();
    }

    private void LateUpdate()
    {
        eyesocketL.localPosition = lEyePos;
        eyesocketR.localPosition = rEyePos;
        nose.localPosition = nosePos;
        mouth.localPosition = mouthPos;
        chin.localPosition = chinPos;
        eyesocketL.localScale = Vector3.one * eyeSize;
        eyesocketR.localScale = Vector3.one * eyeSize;
        nose.localScale = Vector3.one * noseSize;
        mouth.localScale = Vector3.one * mouthSize;
    }

    public Color[] GetColor(Collider collider)
    {
        if(collider == hairCollider)
        {
            try { return GameMaster.controller.customizationController.currentOutfits.Single(x => x.data.outfitType == OutfitData.OufitType.Head).data.ColorOptions; } catch { };
        }
        else if (collider == upperCollider)
        {
            try { return GameMaster.controller.customizationController.currentOutfits.Single(x => x.data.outfitType == OutfitData.OufitType.Upper).data.ColorOptions; } catch { };
        }
        else if (collider == lowerCollider)
        {
            try { return GameMaster.controller.customizationController.currentOutfits.Single(x => x.data.outfitType == OutfitData.OufitType.Lower).data.ColorOptions; } catch { };
        }
        else if (collider == shoeCollider)
        {
            try { return GameMaster.controller.customizationController.currentOutfits.Single(x => x.data.outfitType == OutfitData.OufitType.Shoes).data.ColorOptions; } catch { };
        }
        else if(collider == eyeCollider)
        {
            return GameMaster.controller.customizationController.character.eyeColors;
        }
        else if (collider == browCollider)
        {
            return GameMaster.controller.customizationController.character.browColors;
        }
        else if (collider == skinCollider)
        {
            return GameMaster.controller.customizationController.character.skinColors;
        }
        return null;
    }

    public Character.ColorizeParts GetPart(Collider collider)
    {
        if (collider == hairCollider)
        {
            return Character.ColorizeParts.Hair;
        }
        else if (collider == eyeCollider)
        {
            return Character.ColorizeParts.Eyes;
        }
        else if (collider == browCollider)
        {
            return Character.ColorizeParts.Brows;
        }
        else if (collider == skinCollider)
        {
            return Character.ColorizeParts.Skin;
        }
        else if (collider == upperCollider)
        {
            return Character.ColorizeParts.Upper;
        }
        else if (collider == lowerCollider)
        {
            return Character.ColorizeParts.Lower;
        }
        else if (collider == shoeCollider)
        {
            return Character.ColorizeParts.Shoes;
        }
        return Character.ColorizeParts.Skin;
    }

    public void SetFaceCollidersActive(bool active)
    {
        GameMaster.skeleton.eyeCollider.enabled = active;
        GameMaster.skeleton.browCollider.enabled = active;
    }

    private void SetInitialPos()
    {
        initialFacePos = new float[5];
        initialFacePos[0] = eyesocketL.localPosition.x;
        initialFacePos[1] = eyesocketL.localPosition.z;
        initialFacePos[2] = nose.localPosition.z;
        initialFacePos[3] = mouth.localPosition.z;
        initialFacePos[4] = chin.localPosition.z;
    }

    public void LoadFaceEdits()
    {
        SetEyes(GameMaster.user.faceEdit[0], GameMaster.user.faceEdit[1], GameMaster.user.faceEdit[2]);
        SetNose(GameMaster.user.faceEdit[3], GameMaster.user.faceEdit[4]);
        SetMouth(GameMaster.user.faceEdit[5], GameMaster.user.faceEdit[6], GameMaster.user.faceEdit[7]);
    }

    public void SetEyes(float val1, float val2, float val3)
    {
        val1 = 1 - val1;
        val2 = 1 - val2;
        float half = Skeleton.sliderDistance / 2;
        var s = GameMaster.skeleton;
        var el = s.eyesocketL.localPosition;
        var er = s.eyesocketR.localPosition;
        var left = s.initialFacePos[0] - half;
        var right = -s.initialFacePos[0] + half;
        var height = s.initialFacePos[1] - half;
        lEyePos = new Vector3(left + (Skeleton.sliderDistance * val1), el.y, height + (Skeleton.sliderDistance * val2));
        rEyePos = new Vector3(right - (Skeleton.sliderDistance * val1), er.y, height + (Skeleton.sliderDistance * val2));
        eyeSize = 1 - (eyeScale / 2) + (eyeScale * val3);
    }

    public void SetNose(float val1, float val2)
    {
        val1 = 1 - val1;
        float half = Skeleton.sliderDistance / 2;
        var s = GameMaster.skeleton;
        var nose = s.initialFacePos[2] - half;
        var n = s.nose.localPosition;
        nosePos = new Vector3(n.x, n.y, nose + (Skeleton.sliderDistance * val1));
        noseSize = 1 - (noseScale/ 2) + (noseScale * val2);
    }

    public void SetMouth(float val1, float val2, float val3)
    {
        val1 = 1 - val1;
        float half = Skeleton.sliderDistance / 2;
        var s = GameMaster.skeleton;
        var mouth = s.initialFacePos[3] - half;
        var m = s.mouth.localPosition;
        var chin = s.initialFacePos[4] - half;
        var c = s.chin.localPosition;
        mouthPos = new Vector3(m.x, m.y, mouth + (Skeleton.sliderDistance * val1));
        chinPos = new Vector3(c.x, c.y, chin + (Skeleton.sliderDistance * val2));
        mouthSize = 1 - (mouthScale / 2) + (mouthScale * val3);
    }

    public Transform FindBone(string bone)
    {
        foreach (var b in skin.bones)
        {
            if(b.name == bone)
            {
                return b;
            }
        }
        Debug.LogError("bone not found : " + bone);
        return null;
    }
}
