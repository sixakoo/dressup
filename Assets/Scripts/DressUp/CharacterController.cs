using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

public class CharacterController : MonoBehaviour
{
    public Skeleton skeleton;
    public Transform pivot;
    public NavMeshAgent agent;

    public Vector3 defaultRotation;
    public float rotateSpeed;
    public AnimationCurve turnCurve;
    public Coroutine turnCo;
    public Vector2 minMaxTurnDuration = new Vector2(0.5f, 1f);
    public float minTurnTrigger = 0.4f;
    public const string PLAYERTRANSFORM = "playertransform";
    public Coroutine actionCo;
    public UnityAction onArrive;
    public UnityAction onEndTurn;

    private const float arriveDistance = 0.1f;
    
    //private
    public bool isMoving;

    public void Start()
    {        
        LoadTransform();
    }

    public void Update()
    {
        CharacterState();
        MoveUpdate();
    }

    public void CharacterState()
    {
        if(agent.velocity != Vector3.zero)
        {
            skeleton.animator.Walk(agent.velocity.magnitude);
        }
    }

    public void MoveState(bool move)
    {
        agent.isStopped = !move;
    }

    public void Move(Vector3 target, UnityAction actionOnArrive = null)
    {
        MoveState(true);
        agent.SetDestination(target);
        isMoving = true;
        onArrive = actionOnArrive;
    }

    private void MoveUpdate()
    {
        if(isMoving && !agent.pathPending && agent.remainingDistance < arriveDistance)
        {
            if(!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
            {
                Arrived();
            }
        }
    }

    private void Arrived()
    {
        isMoving = false;
        onArrive?.Invoke();
    }

    public void TurnToDefault()
    {
        Turn(defaultRotation);
    }

    public void Turn(Vector3 rotation, UnityAction afterTurnAction = null)
    {
        if (turnCo != null)
        {
            StopCoroutine(turnCo);
        }
        turnCo = StartCoroutine(TurnCo(rotation, afterTurnAction));
    }

    private IEnumerator TurnCo(Vector3 rotation, UnityAction afterTurnAction = null)
    {
        onEndTurn = afterTurnAction;
        float turnTime = (1 - (Vector3.Dot(agent.transform.forward, Quaternion.Euler(rotation) * Vector3.forward)));
        float rightDot = Vector3.Dot(agent.transform.right, Quaternion.Euler(rotation) * Vector3.forward);
        if (turnTime > minTurnTrigger)
        {
            skeleton.animator.Turn(rightDot < 0);
        }
        float frac = turnTime / 2;
        float turnDuration = Mathf.Clamp(minMaxTurnDuration.y * frac, minMaxTurnDuration.x, Mathf.Infinity);
        float timer = 0;
        float val = 0;
        Vector3 startRot = agent.transform.forward;
        while (timer < 1)
        {
            timer += Time.deltaTime / turnDuration;
            val = turnCurve.Evaluate(timer);
            agent.transform.forward = Vector3.Slerp(startRot, Quaternion.Euler(rotation) * Vector3.forward, val);
            yield return null;
        }
        onEndTurn?.Invoke();
    }

    public void SaveTransform()
    {
        var t = agent.transform;
        List<float> characterTransform = new List<float> { 
            t.position.x, t.position.y, t.position.z, 
            t.eulerAngles.x, t.eulerAngles.y, t.eulerAngles.z};
        SaveManager.SaveFloatList(PLAYERTRANSFORM, characterTransform);
    }

    public void LoadTransform()
    {
        if (!PlayerPrefs.HasKey(PLAYERTRANSFORM))
        {
            return;
        }
        agent.enabled = false;
        List<float> t = SaveManager.LoadFloatList(PLAYERTRANSFORM);
        //agent.transform.position = new Vector3(t[0], t[1], t[2]);
        agent.Warp(new Vector3(t[0], t[1], t[2]));
        agent.transform.rotation = Quaternion.Euler(new Vector3(t[3], t[4], t[5]));
        agent.enabled = true;
    }

    public void SetAction(IEnumerator coFunction)
    {
        if(actionCo != null)
        {
            StopCoroutine(actionCo);
        }
        actionCo  = StartCoroutine(coFunction);
    }

    public void OnApplicationQuit()
    {
        SaveTransform();
    }

    public void EndOfStop()
    {
        GameMaster.controller.characterBehaviour.EndOfStop();
    }
}
