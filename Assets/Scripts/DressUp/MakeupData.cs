using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Makeup", menuName = "ScriptableObjects/Makeup", order = 0)]
public class MakeupData : ScriptableObject
{
    public string makeupName;
    public Color color;
    [Range(0, 1)]
    public float metallic;
    [Range(0, 1)]
    public float smoothness;
    public Part part;
    public int index;

    public enum Part
    {
        None = -1,
        EyeMakeup = 0,
        Blusher = 1,
        Lipstick = 2
    }
}
