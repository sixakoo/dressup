using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test1 : MonoBehaviour
{
    private void AddMorph(GameObject _ref, GameObject selection_mesh)
    {
        Mesh _m = new Mesh();

        // _selection - Bounds for selecting group of vertices within.
        var _selection = new Bounds(selection_mesh.transform.position, selection_mesh.transform.localScale);

        // movement delta for future group of vertices.
        Vector3 newDelta = new Vector3(0, -0.4f, 0);

        // name for the future blend shape.
        string MorphName = "Custom[50-250]";

        //
        // Convert Mesh Renderer to Skinned Mesh Renderer, and create and instance of the shared mesh.
        //
        if (_ref.GetComponent<SkinnedMeshRenderer>() == null && _ref.GetComponent<MeshRenderer>() != null)
        {
            _m = Instantiate(_ref.GetComponent<MeshFilter>().sharedMesh);
            Destroy(_ref.GetComponent<MeshRenderer>());
            Destroy(_ref.GetComponent<MeshFilter>());
            _ref.AddComponent<SkinnedMeshRenderer>();
            _ref.GetComponent<SkinnedMeshRenderer>().sharedMesh = _m;
        }
        else if (_ref.GetComponent<SkinnedMeshRenderer>() != null) // Create instance of shared mesh.
        {
            _m = Instantiate(_ref.GetComponent<SkinnedMeshRenderer>().sharedMesh);
            _ref.GetComponent<SkinnedMeshRenderer>().sharedMesh = _m;
        }
        else return; // if object has no renderer at all, stop the script execution.

        // Assign the new mesh we instantiated before.
        _ref.GetComponent<SkinnedMeshRenderer>().sharedMesh = _m;
        // create array with deltas.
        Vector3[] newVertexDeltas = new Vector3[_m.vertices.Length];

        for (int i = 0; i < newVertexDeltas.Length; i++)
        {
            if (_selection.Contains(_ref.transform.TransformPoint(_m.vertices[i])))
            {
                // each vertex found within the _selection bounds, gets the delta.
                newVertexDeltas[i] = newDelta;

                //GREEN draw line - current position of vertices.
                Debug.DrawLine(
                _ref.transform.TransformPoint(_m.vertices[i]), _ref.transform.TransformPoint(_m.vertices[i] + _m.normals[i] * 0.05f), Color.green, 1000000.0f);

                //MAGENTA draw line - path of each vertex from initial position to delta position.
                Debug.DrawLine(
                _ref.transform.TransformPoint(_m.vertices[i]), _ref.transform.TransformPoint(_m.vertices[i] + newDelta), Color.magenta, 1000000.0f);

                //BLUE draw line - position of each vertex in it's delta position.
                Debug.DrawLine(
                _ref.transform.TransformPoint(_m.vertices[i] + newDelta), _ref.transform.TransformPoint((_m.vertices[i] - _m.normals[i] * 0.05f) + newDelta), Color.blue, 1000000.0f);
            }
            else
            {
                //RED draw line - position of each vertex that are not within the _selection bounds.
                newVertexDeltas[i] = Vector3.zero;
                Debug.DrawLine(
                _ref.transform.TransformPoint(_m.vertices[i]), _ref.transform.TransformPoint(_m.vertices[i] - _m.normals[i] * 0.05f), Color.red, 1000000.0f);
            }
        }

        // Create the new blendshape with vertecies deltas.
        _m.AddBlendShapeFrame(MorphName, 100f, newVertexDeltas, null, null);
        _m.RecalculateNormals();
        _m.RecalculateTangents();
    }

    void Test2(SkinnedMeshRenderer source, SkinnedMeshRenderer target)
    {
        Vector3[] verts = new Vector3[target.sharedMesh.vertexCount];

        source.sharedMesh.ClearBlendShapes();
        target.sharedMesh.ClearBlendShapes();

        // Calculate delta verts from source to target
        for (int i = 0; i < verts.Length; i++)
        {
            verts[i] = target.sharedMesh.vertices[i] - source.sharedMesh.vertices[i];
        }

        source.sharedMesh.AddBlendShapeFrame("test", 1, verts, null, null);

        source.sharedMesh.RecalculateNormals();
        source.sharedMesh.RecalculateTangents();
    }
}
