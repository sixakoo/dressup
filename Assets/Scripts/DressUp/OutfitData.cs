using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Outfit", menuName = "ScriptableObjects/Outfit", order = 1)]
public class OutfitData : ScriptableObject
{
    public string outfitName;
    public Sprite thumbnail;
    public OufitType outfitType;
    public Outfit outfitModel;
    public Parts[] hideParts;
    public Parts[] requiredParts;
    public Color[] ColorOptions;
    public bool tuckable;
    public bool supportTuck;
    public string tag;
    public string bone;
    public Vector3 offset;

    public enum OufitType
    {
        None,
        Head,
        Upper,
        Lower,
        Shoes,
        Accessories,
    }

    public enum Parts
    {
        Abs,
        Ankles,
        Biceps,
        Brows,
        Bust,
        Calves,
        Chest,
        Collar,
        Elbows,
        EyeLashes,
        Eyes,
        Feet,
        Forearms,
        Hands,
        Head,
        Hips,
        Knees,
        Neck,
        Shoulders,
        Teeth,
        Thighs,
        Torso
    }
}
