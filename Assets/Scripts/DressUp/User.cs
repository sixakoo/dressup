using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class User
{
    public List<OutfitPref> OutfitPrefs { 
        get { if (outfitPrefs == null) { outfitPrefs = new List<OutfitPref>(); } return outfitPrefs; } private set { outfitPrefs = value; } }
    private List<OutfitPref> outfitPrefs;
    public List<OutfitData> Accessories { 
    get { if (accessories == null) { accessories = new List<OutfitData>(); }return accessories; } private set { accessories = value; } }
    private List<OutfitData> accessories;
    public List<ShapeKey> faceKeys;
    public int[] colors;
    public float[] faceEdit;

    public const string FACEEDIT = "faceedit";

    public User()
    {
        outfitPrefs = new List<OutfitPref>();
        Accessories = new List<OutfitData>();
        faceKeys = new List<ShapeKey>();
        colors = new int[3];
        faceEdit = new float[8];
    }

    public class OutfitPref
    {
        public OutfitData data;
        public int colorIndex;
        public bool tuck;

        public OutfitPref(OutfitData _outfitData, int _colorIndex, bool _tuck)
        {
            data = _outfitData;
            colorIndex = _colorIndex;
            tuck = _tuck;
        }
    }

    public OutfitPref this[OutfitData outfitData] => GetOutfitPref(outfitData);

    public OutfitPref GetOutfitPref(OutfitData outfitData)
    {
        foreach (var outfitPref in outfitPrefs)
        {
            if (outfitPref.data.outfitName == outfitData.outfitName)
            {
                return outfitPref;
            }
        }
        Debug.LogError("outfitPreft not found");
        return null;
    }

    public Outfit GetAccessory(OutfitData accessory)
    {
        foreach (var a in GameMaster.controller.customizationController.currentAccessories)
        {
            if(a.data.outfitName == accessory.outfitName)
            {
                return a;
            }
        }
        return null;
    }

    public OutfitPref CheckOutfitTypeOccupied(OutfitData.OufitType outfitType)
    {
        if (outfitPrefs == null) { outfitPrefs = new List<OutfitPref>(); }
        foreach (var ofp in outfitPrefs)
        {
            if(ofp.data.outfitType == outfitType)
            {
                return ofp;
            }
        }
        return null;
    }

    public OutfitData CheckAccessoriesOccupied(string tag)
    {
        if (accessories == null) { accessories = new List<OutfitData>(); }
        foreach (var a in accessories)
        {
            if(a.tag == tag)
            {
                return a;
            }
        }
        return null;
    }

    public OutfitData CheckAccessoriesOccupied(OutfitData data)
    {
        if (accessories == null) { accessories = new List<OutfitData>(); }
        foreach (var a in accessories)
        {
            if (a == data)
            {
                return a;
            }
        }
        return null;
    }

    public void AddOutfit(OutfitData outfitData, int colorIndex, bool tuck)
    {
        if (outfitPrefs == null) { outfitPrefs = new List<OutfitPref>(); }
        var occupied = CheckOutfitTypeOccupied(outfitData.outfitType);
        if (occupied != null)
        {
            outfitPrefs.Remove(occupied);
        }
        outfitPrefs.Add(new OutfitPref(outfitData, colorIndex, tuck));
    }

    public void AddOutfit(OutfitData outfitData)
    {
        AddOutfit(outfitData, 0, false);
    }

    public void AddAccessory(OutfitData outfitData, bool assignOnFreeSlot = false)
    {
        if (accessories == null) { accessories = new List<OutfitData>(); }
        if(GameMaster.controller.itemManager.IsPairAccessory(outfitData)) //for pair accessories
        {
            if (assignOnFreeSlot)
            {                                   
                var options = GameMaster.controller.itemManager.GetAccessoryOptions(outfitData);
                OutfitData left = null;
                OutfitData right = null;
                foreach (var o in options)
                {
                    left = CheckAccessoriesOccupied(options[0].tag);
                    right = CheckAccessoriesOccupied(options[1].tag);
                }
                if (left == null)
                {
                    accessories.Add(options[0]);
                }
                else if (right == null)
                {
                    accessories.Add(options[1]);
                }
                else
                {
                    accessories.Remove(left);
                    accessories.Add(options[0]);
                }
            }
            else
            {
                var occupied = CheckAccessoriesOccupied(outfitData.tag);
                if (occupied != null)
                {
                    accessories.Remove(occupied);
                }
                accessories.Add(outfitData);
            }
        }
        else //for normal accessories
        {
            var occupied = CheckAccessoriesOccupied(outfitData.tag);
            if (occupied != null)
            {
                accessories.Remove(occupied);
            }
            accessories.Add(outfitData);
        }
    }

    public void RemoveAccessory(OutfitData accessory)
    {
        if (accessories == null) { accessories = new List<OutfitData>(); }
        var occupied = CheckAccessoriesOccupied(accessory);
        if (occupied != null)
        {
            accessories.Remove(occupied);
        }
    }

    public void RemoveAccessory(string tag)
    {
        if (accessories == null) { accessories = new List<OutfitData>(); }
        var occupied = CheckAccessoriesOccupied(tag);
        if (occupied != null)
        {
            accessories.Remove(occupied);
        }
    }

    public void SaveFaceEdit()
    {
        SaveManager.SaveFloatList(FACEEDIT, faceEdit.ToList());
    }

    public void LoadFaceEdit()
    {
        if(!PlayerPrefs.HasKey(FACEEDIT))
        {
            faceEdit = new float[8];
            for (int i = 0; i < faceEdit.Length; i++)
            {
                faceEdit[i] = 0.5f;
            }
            SaveFaceEdit();
        }
        faceEdit = SaveManager.LoadFloatList(FACEEDIT).ToArray();
    }

    public void ClearAllOutfits()
    {
        outfitPrefs = new List<OutfitPref>();
        accessories = new List<OutfitData>();
    }
}
