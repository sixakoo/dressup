using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action", menuName = "ScriptableObjects/Character Action", order = 1)]
public class CharacterAction : ScriptableObject
{
    public float actionDuration;
    public string animationName;
    public AffectMeter[] meters;

    [System.Serializable]
    public class AffectMeter
    {
        public string meterName;
        public float duration;
        public float modifier;
    }
}
