using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ActionTrigger : MonoBehaviour
{
    public CharacterAction[] actions;
    public Transform[] triggerPositions;
    public Transform[] customPositions;
    public string[] animNames;

    public void Action(int index)
    {
        var s = GameMaster.skeleton;
        float nearest = float.MaxValue;
        string animationOverride = "";
        Transform nearTransform = null;
        for (int i = 0; i < triggerPositions.Length; i++)
        {
            float distance = Vector3.Distance(triggerPositions[i].position, s.transform.position);
            if(distance < nearest)
            {
                nearest = distance;
                nearTransform = triggerPositions[i];
                if(i < animNames.Length)
                {
                    animationOverride = animNames[i];
                }
            }
        }
        if(nearTransform == null)
        {
            Debug.LogError("no trigger positions");
            return;
        }
        //GameMaster.skeleton.characterController.Move(nearTransform.position, () => Execute(index, nearTransform));
        GameMaster.controller.characterBehaviour.Sequence(nearTransform.position, () => Execute(index, nearTransform, animationOverride, customPositions.ToList()), animationOverride);
    }

    private void Execute(int index, Transform target, string entryAnimation = "", List<Transform> customPos = null)
    {
        GameMaster.controller.characterBehaviour.Action(actions[index], target, entryAnimation, customPos);
    }
}
