using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimator : MonoBehaviour
{
    public const string STATIC = "Static";
    public const string IDLE = "Idle";
    public const string WALK = "Walk";
    public const string RIGHT = "RightTurn";
    public const string LEFT = "LeftTurn";
    public const string HAPPY = "Happy";
    public const string AGITATED = "Agitated";
    public const string SAD = "Sad";
    public const string NORMAL = "Normal";
    public const string TIRED = "Tired";
    public const string STOP = "Stop";
    public const string SIDE = "Side";
    public const string SIDESPLITTER = "/";
    public Animator animator;
    private CharacterState characterState;
    private List<string> lastTriggers;

    public enum CharacterState
    {
        Static,
        Idle,
        Walk
    }

    #region Body

    public void Static()
    {
        if(characterState != CharacterState.Static)
        {
            characterState = CharacterState.Static;
            animator.SetTrigger(STATIC);
        }
    }

    public void Idle()
    {
        if (characterState != CharacterState.Idle)
        {
            characterState = CharacterState.Idle;
            animator.SetTrigger(IDLE);
        }
    }

    public void Turn(bool left)
    {
        animator.SetTrigger(left? LEFT : RIGHT);
    }

    public void Walk(float speed)
    {
        animator.SetFloat(WALK, speed);
    }

    public void Action(string action)
    {
        
    }

    public void Action(List<string> actions)
    {
        if (lastTriggers != null)
        {
            for (int i = 0; i < lastTriggers.Count; i++)
            {
                animator.ResetTrigger(lastTriggers[i]);
            }
        }
        for (int i = 0; i < actions.Count; i++)
        {
            animator.SetTrigger(actions[i]);
        }
        lastTriggers = actions;
    }

    public void Stop()
    {
        animator.SetTrigger(STOP);
    }

    #endregion

    #region Expression

    public void Agitated()
    {
        animator.SetTrigger(AGITATED);
    }

    public void Happy()
    {
        animator.SetTrigger(HAPPY);
    }

    public void Normal()
    {
        animator.SetTrigger(NORMAL);
    }

    public void Sad()
    {
        animator.SetTrigger(SAD);
    }

    public void Tired()
    {
        animator.SetTrigger(TIRED);
    }

    #endregion

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Turn(true); 
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Turn(false);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            animator.SetTrigger(IDLE);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            animator.SetTrigger(STATIC);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            animator.SetTrigger("d");
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            //animator.SetTrigger("q");
            Happy();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            Agitated();
            //animator.SetTrigger("w");
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            Normal();
            //animator.SetTrigger("e");
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Tired();
            //animator.SetTrigger("z");
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            Sad();
            //animator.SetTrigger("x");
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            animator.SetTrigger("c");
        }
    }
}
