using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombineSkinnedMesh : MonoBehaviour
{
    public SkinnedMeshRenderer[] sources;

    public void Start()
    {
        Vector3 originalPosition = this.transform.position;
        this.transform.position = Vector3.zero;
        List<CombineInstance> combineInstances = new List<CombineInstance>();

        //boneInfo
        List<Transform> bones = new List<Transform>();
        List<BoneWeight> boneWeights = new List<BoneWeight>();
        Hashtable bonesByHash = new Hashtable();
        List<Matrix4x4> bindPoses = new List<Matrix4x4>();

        int boneIndex = 0;
        foreach (Transform bone in sources[0].bones)
        {
            bones.Add(bone);
            bonesByHash.Add(bone.name, boneIndex);
            boneIndex++;
        }

        for(int b = 0; b < bones.Count; b++)
        {
            bindPoses.Add(bones[b].worldToLocalMatrix * transform.worldToLocalMatrix);
        }

        int i = 0;
        while(i < sources.Length)
        {
            if(!sources[i].gameObject.activeSelf)
            {
                i++;
                continue;
            }
            CombineInstance ci = new CombineInstance();
            ci.mesh = sources[i].sharedMesh;
            ci.transform = sources[i].transform.localToWorldMatrix;
            foreach (BoneWeight bw in sources[i].sharedMesh.boneWeights)
            {
                BoneWeight bWeight = bw;
                bWeight.boneIndex0 = (int)bonesByHash[sources[i].bones[bw.boneIndex0].name];
                bWeight.boneIndex1 = (int)bonesByHash[sources[i].bones[bw.boneIndex1].name];
                bWeight.boneIndex2 = (int)bonesByHash[sources[i].bones[bw.boneIndex2].name];
                bWeight.boneIndex3 = (int)bonesByHash[sources[i].bones[bw.boneIndex3].name];
                boneWeights.Add(bWeight);
            }
            combineInstances.Add(ci);
            i++;
        }

        SkinnedMeshRenderer parentSkin = gameObject.AddComponent<SkinnedMeshRenderer>();
        parentSkin.sharedMesh = new Mesh();
        parentSkin.sharedMesh.vertices = sources[0].sharedMesh.vertices;
        parentSkin.sharedMesh.CombineMeshes(combineInstances.ToArray(), true);
        parentSkin.bones = sources[0].bones;
        parentSkin.rootBone = sources[0].rootBone;
        parentSkin.sharedMesh.boneWeights = boneWeights.ToArray();
        parentSkin.sharedMesh.bindposes = bindPoses.ToArray();
        parentSkin.sharedMesh.RecalculateBounds();
        parentSkin.material = sources[0].material;
        transform.position = originalPosition;
        foreach (var s in sources)
        {
            Destroy(s.gameObject);
        }
        sources = new SkinnedMeshRenderer[0];
    }
}
