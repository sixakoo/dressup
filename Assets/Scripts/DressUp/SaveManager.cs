using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SaveManager
{
    private const string separator = "|";

    public static void SaveFloatInt(string key, List<int> list)
    {
        string data = "";
        foreach (int o in list)
        {
            data += o + separator;
        }
        PlayerPrefs.SetString(key, data);
    }

    public static List<int> LoadIntList(string key)
    {
        string data = PlayerPrefs.GetString(key);
        List<string> outfits = new List<string>();
        string[] arr = data.Split(new string[] { separator }, System.StringSplitOptions.RemoveEmptyEntries);
        var intArr = arr.Select(x => int.Parse(x));
        return intArr.ToList();
    }

    public static void SaveFloatList(string key, List<float> list)
    {
        string data = "";
        foreach (float o in list)
        {
            data += o + separator;
        }
        PlayerPrefs.SetString(key, data);
    }

    public static List<float> LoadFloatList(string key)
    {
        string data = PlayerPrefs.GetString(key);
        List<string> outfits = new List<string>();
        string[] arr = data.Split(new string[] { separator }, System.StringSplitOptions.RemoveEmptyEntries);
        var floatArr = arr.Select(x => float.Parse(x));
        return floatArr.ToList();
    }

    public static void SaveStringList(string key, List<string> list)
    {
        string data = "";
        foreach (string o in list)
        {
            data += o + separator;
        }
        PlayerPrefs.SetString(key, data);        
    }

    public static List<string> LoadStringList(string key)
    {
        string data = PlayerPrefs.GetString(key);
        List<string> outfits = new List<string>();
        string[] arr = data.Split(new string[]{separator}, System.StringSplitOptions.RemoveEmptyEntries);
        return arr.ToList();
    }

    public static void SaveBool(string key, bool b)
    {
        PlayerPrefs.SetInt(key, b ? 1 : 0);
    }

    public static bool LoadBool (string key)
    {
        return PlayerPrefs.GetInt(key) == 1;
    }
}
