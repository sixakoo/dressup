using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

public class CustomizationController : MonoBehaviour
{
    [Header("Prefab")]
    public Character characterPrefab;
    public Skeleton skeletonPrefab;

    [Header("Component")]
    //public List<OutfitData> outfitDatas;
    public List<Outfit> currentOutfits;
    public List<Outfit> currentAccessories;
    public OutfitData.Parts[] combineParts;
    public UnityAction onCharacterReset;
    public bool isTuckedIn;

    [Header("Variables")]
    public Vector3 playerSpawn;

    //privates
    [HideInInspector]
    private Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();
    [HideInInspector]
    public Character character;
    [HideInInspector]
    public Skeleton skeleton;
    public User user;
    private List<OutfitData.Parts> hiddenParts;

    //Const
    public const string OUTFITS = "outfits";
    public const string ACCESSORIES = "accessories";
    public const string TUCK = "tuck";
    public const string SUBSPLIT = ":";
    public const int TUCKOFFSET = 1000;
    public readonly int[] SHOESHAPEINDEX = { 1, 11 };

    private void Start()
    {
        Application.targetFrameRate = 60;
        LoadSkeleton();
        LoadUserData();
        ResetCharacter();
    }

    private void Update()
    {
        DebugKeys();
    }

    public void SaveOutfit()
    {
        SaveManager.SaveStringList(OUTFITS, user.OutfitPrefs.Select(x => x.data.outfitName + SUBSPLIT + (x.tuck ? x.colorIndex + TUCKOFFSET : x.colorIndex)).ToList());
    }

    private void InstantiateOutfits()
    {
        currentOutfits = new List<Outfit>();
        isTuckedIn = false;
        foreach (User.OutfitPref ofp in user.OutfitPrefs)
        {
            if (ofp.data.outfitModel == null) { continue; }
            Outfit outfitModel = Instantiate(ofp.data.outfitModel);
            outfitModel.data = ofp.data;
            if (ofp.data.ColorOptions.Length > 0)
            {
                outfitModel.skin.material.color = ofp.data.ColorOptions[ofp.colorIndex];
            }
            if (ofp.tuck)
            {
                isTuckedIn = true;
            }
            AttachOutfitToBone(outfitModel);
            currentOutfits.Add(outfitModel);
            ApplyOutfitShapekey(ofp.data);
        }
        ToggleTuckIn(isTuckedIn);
    }

    private void SaveAccessories()
    {
        SaveManager.SaveStringList(ACCESSORIES, user.Accessories.Select(x => x.outfitName + ":" + x.tag).ToList());
    }

    private void InstantiateAccessories()
    {
        currentAccessories = new List<Outfit>();
        foreach (OutfitData od in user.Accessories)
        {
            if(CheckHidden(od))
            {
                continue;
            }
            if (od.outfitModel == null) { continue; }
            Outfit outfitModel = Instantiate(od.outfitModel);
            outfitModel.data = od;
            AttachOutfitToBone(outfitModel);
            currentAccessories.Add(outfitModel);
        }
    }

    private bool CheckHidden(OutfitData od)
    {
        if(hiddenParts == null || od.requiredParts == null)
        {
            return false;
        }
        foreach (var hp in hiddenParts)
        {
            foreach (var rp in od.requiredParts)
            {
                if(rp == hp)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void LoadUserData()
    {
        user = new User();
        GameMaster.user = user;
        //isTuckedIn = SaveManager.LoadBool(TUCK);
        var outfits = SaveManager.LoadStringList(OUTFITS);
        var accessories = SaveManager.LoadStringList(ACCESSORIES);
        currentOutfits = new List<Outfit>();
        currentAccessories = new List<Outfit>();
        foreach (var data in outfits)
        {
            string[] arr = data.Split(new string[] { SUBSPLIT }, System.StringSplitOptions.RemoveEmptyEntries);
            //assign name & get data
            OutfitData outfitData = GameMaster.controller.itemManager.GetOutfitData(arr[0]);
            int colorIndex = int.Parse(arr[1]);
            bool tuck = false;
            if (arr.Length > 1) // assign color if exist
            {
                if (colorIndex > TUCKOFFSET - 1)
                {
                    colorIndex -= TUCKOFFSET;
                    tuck = true;
                }
            }
            user.AddOutfit(outfitData, colorIndex, tuck);
        }
        foreach (var data in accessories)
        {
            var arr = data.Split(new string[] { SUBSPLIT }, System.StringSplitOptions.RemoveEmptyEntries);
            OutfitData accessory = GameMaster.controller.itemManager.GetOutfitData(arr[0], arr[1]);
            user.AddAccessory(accessory);
        }
    }

    private void LoadSkeleton()
    {
        skeleton = Instantiate(skeletonPrefab);
        skeleton.customizationController = this;
        GameMaster.skeleton = skeleton;
        GameMaster.controller.cameraController.characterView.SetParent(skeleton.characterController.pivot);
        skeleton.characterController.agent.Warp(playerSpawn);
    }

    public void ResetCharacter()
    {
        if (character)
        {
            ClearCharacter();
        }
        LoadCharacter();
        InitBone();
        AttachOtherPartsToBone();
        HideParts();
        InstantiateOutfits();
        InstantiateAccessories();
        OptimizeBody();
        MergeToSkeleton();
        character.LoadMakeup();
        character.LoadColors();
        onCharacterReset?.Invoke();
        skeleton.LoadFaceEdits();        
    }

    private void LoadCharacter()
    {
        character = Instantiate(characterPrefab);
        character.LoadFaceShape();        
    }

    private void MergeToSkeleton()
    {
        AttachToBone(character.skin, false);
        character.transform.SetParent(skeleton.skin.transform.parent);
    }

    private void InitBone()
    {
        foreach (Transform bone in skeleton.skin.bones)
        {
            boneMap[bone.gameObject.name] = bone;
        }
    }

    private void ClearCharacter()
    {
        //Debug.LogError("character : " + character, character);
        Destroy(character.gameObject);
    }

    private void OptimizeBody()
    {
        var parts = character.bodyParts.Where(x => combineParts.Contains(x.part));
        CombineSkin(parts.Select(x => x.skin).ToArray());
    }

    private void HideParts()
    {
        hiddenParts = new List<OutfitData.Parts>();
        foreach (var ofp in user.OutfitPrefs)
        {
            foreach (var part in ofp.data.hideParts)
            {
                hiddenParts.Add(part);
                character.bodyParts.Single(x => x.part == part).skin.gameObject.SetActive(false);
            }
        }
    }

    private void AttachOutfitToBone(Outfit source)
    {
        AttachToBone(source.skin, true);
        source.transform.SetParent(character.skin.transform.parent);
    }

    private void AttachOtherPartsToBone()
    {
        foreach (var other in character.otherParts)
        {
            AttachToBone(other.skin, true);
            other.skin.transform.SetParent(character.skin.transform.parent);
        }
    }

    private void AttachToBone(SkinnedMeshRenderer skin, bool removeBone)
    {
        Transform[] newBones = new Transform[skin.bones.Length];
        for (int i = 0; i < skin.bones.Length; i++)
        {
            GameObject bone = skin.bones[i].gameObject;
            if (!boneMap.TryGetValue(bone.name, out newBones[i]))
            {
                Debug.Log("Unable to map bone " + bone.name + " to get skeleton : " + skin.name);
                break;
            }
        }
        skin.bones = newBones;
        if (removeBone) { Destroy(skin.rootBone.transform.parent.gameObject); };
        skin.rootBone = skeleton.skin.rootBone;
    }

    public void CombineSkin(SkinnedMeshRenderer[] sources)
    {
        Vector3 originalPosition = this.transform.position;
        this.transform.position = Vector3.zero;
        List<CombineInstance> combineInstances = new List<CombineInstance>();

        //boneInfo
        List<Transform> bones = new List<Transform>();
        List<BoneWeight> boneWeights = new List<BoneWeight>();
        Hashtable bonesByHash = new Hashtable();
        List<Matrix4x4> bindPoses = new List<Matrix4x4>();

        int boneIndex = 0;
        foreach (Transform bone in sources[0].bones)
        {
            bones.Add(bone);
            bonesByHash.Add(bone.name, boneIndex);
            boneIndex++;
        }

        for (int b = 0; b < bones.Count; b++)
        {
            bindPoses.Add(bones[b].worldToLocalMatrix * transform.worldToLocalMatrix);
        }

        int i = 0;
        while (i < sources.Length)
        {
            if (!sources[i].gameObject.activeSelf)
            {
                i++;
                continue;
            }
            CombineInstance ci = new CombineInstance();
            Mesh t = new Mesh();
            sources[i].BakeMesh(t); //Apply Blenshape
            t.boneWeights = sources[i].sharedMesh.boneWeights;
            sources[i].sharedMesh = t;
            ci.mesh = sources[i].sharedMesh;
            ci.transform = sources[i].transform.localToWorldMatrix;
            foreach (BoneWeight bw in sources[i].sharedMesh.boneWeights)
            {
                BoneWeight bWeight = bw;
                bWeight.boneIndex0 = (int)bonesByHash[sources[i].bones[bw.boneIndex0].name];
                bWeight.boneIndex1 = (int)bonesByHash[sources[i].bones[bw.boneIndex1].name];
                bWeight.boneIndex2 = (int)bonesByHash[sources[i].bones[bw.boneIndex2].name];
                bWeight.boneIndex3 = (int)bonesByHash[sources[i].bones[bw.boneIndex3].name];
                boneWeights.Add(bWeight);
            }
            combineInstances.Add(ci);
            i++;
        }
        character.skin = character.gameObject.AddComponent<SkinnedMeshRenderer>();
        character.skin.sharedMesh = new Mesh();
        character.skin.sharedMesh.vertices = skeleton.skin.sharedMesh.vertices;
        character.skin.sharedMesh.CombineMeshes(combineInstances.ToArray(), true);
        character.skin.bones = skeleton.skin.bones;
        character.skin.rootBone = skeleton.skin.rootBone;
        character.skin.sharedMesh.boneWeights = boneWeights.ToArray();
        character.skin.sharedMesh.bindposes = bindPoses.ToArray();
        character.skin.sharedMesh.RecalculateBounds();
        character.skin.material = sources[0].material;
        transform.position = originalPosition;
        foreach (var s in sources)
        {
            Destroy(s.gameObject);
        }
        sources = new SkinnedMeshRenderer[0];
    }

    public void AssignOutfit(OutfitData outfitData)
    {
#if UNITY_EDITOR
        UnityEditor.EditorGUIUtility.PingObject(outfitData);
#endif
        user.AddOutfit(outfitData);
        SaveOutfit();
        ResetCharacter();
    }

    public void RemoveAccesory(string tag)
    {
        user.RemoveAccessory(tag);
        SaveAccessories();
        ResetCharacter();
    }

    public void RemoveAccesory(OutfitData accessory)
    {
        if(GameMaster.controller.itemManager.IsPairAccessory(accessory))
        {
            var group = GameMaster.controller.itemManager.GetAccessoryOptions(accessory);
            foreach (var g in group)
            {
                user.RemoveAccessory(g);
            }
        }
        else
        {
            user.RemoveAccessory(accessory);
        }
        SaveAccessories();
        ResetCharacter();
    }

    public void AssignAccessory(OutfitData outfitData, bool checkFreeSlot = false)
    {
#if UNITY_EDITOR
        UnityEditor.EditorGUIUtility.PingObject(outfitData);
#endif
        user.AddAccessory(outfitData, checkFreeSlot);
        SaveAccessories();
        ResetCharacter();
    }

    public bool CheckTuckSupport()
    {
        foreach (var outfit in currentOutfits)
        {
            if (outfit.data.supportTuck) { return true; }
        }
        return false;
    }

    public void ToggleTuckIn(bool tuck)
    {
        bool tuckSupport = CheckTuckSupport();
        foreach (var outfit in currentOutfits)
        {
            if (outfit.data.tuckable)
            {
                if (tuckSupport)
                {
                    outfit.skin.SetBlendShapeWeight(0, tuck ? 100f : 0f);
                    user[outfit.data].tuck = tuck;
                }
                else
                {
                    outfit.skin.SetBlendShapeWeight(0, 0);
                }
                isTuckedIn = tuck;
            }
        }
    }

    public void ChangeOutfitColor(Outfit outfit, int colorIndex)
    {
        if (colorIndex < 0)
        {
            return;
        }
        outfit.skin.material.color = outfit.data.ColorOptions[colorIndex]; //remove this later
        outfit.skin.material.SetColor("_Color",outfit.data.ColorOptions[colorIndex]);
        user[outfit.data].colorIndex = colorIndex;
        SaveOutfit();
    }

    public void ApplyOutfitShapekey(OutfitData outfitData)
    {
        var tagData = outfitData.tag.Split(new string[] { SUBSPLIT }, System.StringSplitOptions.RemoveEmptyEntries);
        var pivot = skeleton.characterController.pivot;
        if(outfitData.outfitType == OutfitData.OufitType.Shoes)
        {
            pivot.localPosition = outfitData.offset;
            if (tagData.Length > 0)
            {
                foreach (var shoeShapeIndex in SHOESHAPEINDEX)
                {
                    var skin = character.bodyParts[shoeShapeIndex].skin;
                    skin.SetBlendShapeWeight(skin.sharedMesh.GetBlendShapeIndex(tagData[0]), float.Parse(tagData[1]));
                }
            }
        }
    }

    #region Debug

    private void DebugKeys()
    {
        if(Input.GetKeyDown(KeyCode.F12))
        {
            PlayerPrefs.DeleteKey(OUTFITS);
            PlayerPrefs.DeleteKey(ACCESSORIES);
            PlayerPrefs.DeleteKey(CharacterController.PLAYERTRANSFORM);
            PlayerPrefs.DeleteKey(Character.MAKEUPITEM);
            PlayerPrefs.DeleteKey(Character.MAKEUPLAYOUT);
            PlayerPrefs.DeleteKey(User.FACEEDIT);
            Debug.Log("Clear Prefs");
        }
    }

    #endregion
}
