using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UX;

[System.Serializable]
public class CharacterMeter
{
    public string meterName;
    public float val;
    public float maxVal;
    public float changePerMinute;
    public UnityEngine.Events.UnityAction onNotification;
    private List<ActionEffect> effects;
    private List<ActionEffect> deleteList;

    public CharacterMeter()
    {
        effects = new List<ActionEffect>();
        deleteList = new List<ActionEffect>();
    }

    //Const
    public const float notifyInterval = 30f;
    public const float secondsPerMinute = 60;
    public const float minNotify = 20;

    //Private
    private float notifyTimer = notifyInterval;
    
    public class ActionEffect
    {
        public string actionName;
        public float modifier;
        public float duration;
    }

    public CharacterMeter(string _meterName, float _val, float _maxVal, float _changePerMinute)
    {
        meterName = _meterName;
        val = _val;
        maxVal = _maxVal;
        changePerMinute = _changePerMinute;
    }

    public void Update()
    {
        float totalMod = 0;
        foreach (ActionEffect actionEffect in effects)
        {
            actionEffect.duration -= Time.deltaTime;
            if(actionEffect.duration <= 0)
            {
                deleteList.Add(actionEffect);
            }
            else
            {
                totalMod += actionEffect.modifier;
            }
        }
        foreach (ActionEffect actionInstance in deleteList)
        {
            effects.Remove(actionInstance);
        }
        deleteList = new List<ActionEffect>();
        val += (changePerMinute + totalMod)/ secondsPerMinute * Time.deltaTime;
        val = Mathf.Clamp(val, 0, maxVal);

        if(val < minNotify) //Notification
        {
            notifyTimer += Time.deltaTime;
            if (notifyTimer > notifyInterval)
            {
                notifyTimer = 0;
                onNotification.Invoke();
            }
        }
    }

    public void ApplyEffect(CharacterAction.AffectMeter affectMeter, string actionName)
    {
        ActionEffect effect = new ActionEffect()
        {
            actionName = actionName,
            duration = affectMeter.duration,
            modifier = affectMeter.modifier
        };
        effects.Add(effect);
    }

    public void ClearEffect(CharacterAction action)
    {
        effects.RemoveAll(x => x.actionName == action.name);
    }
}
