using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Test : MonoBehaviour
{
    public Vector3 offset;

    public void OnDrawGizmos()
    {
        var x = transform.right * offset.x;
        var y = transform.up * offset.y;
        var z = transform.forward * offset.z;
        var offsetPos = transform.position + x + y + z;
        Gizmos.DrawSphere(offsetPos, 0.1f);
    }
}
