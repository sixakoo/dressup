using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    [Header("Manager")]
    public ItemManager itemManager;
    public ShapePresetManager facePart;
    public SpriteManager spriteManager;

    [Header("Controller")]
    public CameraController cameraController;
    public View_Bar viewBar;
    public CustomizationController customizationController;
    public View_ColorPalette colorPalette;
    public CharacterBehaviour characterBehaviour;

    //Static
    public static Skeleton skeleton;
    public static User user;
    public static GameMaster controller;

    private void Awake()
    {
        if(controller != null)
        {
            Debug.LogError("Another Instance Exist!");
        }
        controller = this;
    }
}
