using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Outfit : MonoBehaviour
{
    public SkinnedMeshRenderer skin;
    [HideInInspector]
    public OutfitData data;
}
