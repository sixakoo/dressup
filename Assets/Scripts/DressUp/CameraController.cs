using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform characterView;
    public Transform[] positions;
    public Transform[] targets;
    public float[] fov;
    public float switchDuration;
    public Camera mainCamera;
    public Transform camTarget;
    public Coroutine switchCo;
    public AnimationCurve camCurve;
    public AnimationCurve fovCurve;

    private Vector3 lastTarget;
    private View_Bar.Page currentPage;

    public void SwitchCameraAngle(View_Bar.Page page, bool byPass = false)
    {
        currentPage = page;
        if (switchCo != null)
        {
            StopCoroutine(switchCo);
        }
        switch (page)
        {
            case View_Bar.Page.Customize:
                switchCo = StartCoroutine(MoveCamera(positions[0], targets[0],fov[0], byPass));
                break;
            case View_Bar.Page.Outfit:
                switchCo = StartCoroutine(MoveCamera(positions[1], targets[1], fov[1], byPass));
                break;
            case View_Bar.Page.Home:
                switchCo = StartCoroutine(MoveCamera(positions[2], targets[2], fov[2], byPass));
                break;
            case View_Bar.Page.Settings:
                break;
            default:
                break;
        }
    }

    public void SetCameraAngle(Vector3 pos, Transform target, float fov = 40)
    {
        Debug.LogError(mainCamera, mainCamera);
        mainCamera.transform.position = pos;
        mainCamera.transform.LookAt(target);
        mainCamera.fieldOfView = fov;
    }

    public void Refresh()
    {
        SwitchCameraAngle(currentPage);
    }

    public IEnumerator MoveCamera(Transform pos, Transform target,float fov, bool byPass = false)
    {
        float timer = 0;
        float val = 0;
        float fovVal = 0;
        Vector3 initPos = mainCamera.transform.position;
        Vector3 camInit = camTarget.position;
        float initFov = mainCamera.fieldOfView;
        if(byPass)
        {
            mainCamera.transform.position = pos.position;
            mainCamera.fieldOfView = fov;
            camTarget.position = target.position;
            mainCamera.transform.LookAt(camTarget);
            yield break;
        }
        while (timer < 1)
        {
            timer += Time.deltaTime / switchDuration;
            val = camCurve.Evaluate(timer);
            fovVal = fovCurve.Evaluate(timer);
            mainCamera.transform.position = Vector3.Lerp(initPos, pos.position, val);
            camTarget.position = Vector3.Lerp(camInit, target.position, val);
            mainCamera.transform.LookAt(camTarget);
            mainCamera.fieldOfView = Mathf.Lerp(initFov, fov, fovVal);
            yield return null;
        }
        mainCamera.transform.position = pos.position;
        mainCamera.fieldOfView = fov;
    }
}
