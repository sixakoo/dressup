%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: BaseAFacialMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Abs
    m_Weight: 0
  - m_Path: Ankles
    m_Weight: 0
  - m_Path: Base_Armature
    m_Weight: 0
  - m_Path: Base_Armature/Base
    m_Weight: 0
  - m_Path: Base_Armature/Base/eyesTarget.001
    m_Weight: 0
  - m_Path: Base_Armature/Base/footIK.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/footIK.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/headTarget.001
    m_Weight: 0
  - m_Path: Base_Armature/Base/root
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/coreIK.001
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/hip.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/hip.L/shin.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/hip.L/shin.L/foot.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/hip.L/shin.L/foot.L/toe.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/hip.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/hip.R/shin.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/hip.R/shin.R/foot.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/hip.R/shin.R/foot.R/toe.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/cheek1.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/cheek1.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/cheek2.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/cheek2.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.L/brow1.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.L/brow2.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.L/brow3.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.L/eye.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.L/lid1.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.L/lid2.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.R/brow1.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.R/brow2.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.R/brow3.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.R/eye.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.R/lid1.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/eyesocket.R/lid2.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/mouth
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/mouth/jaw
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/mouth/jaw/chin
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/mouth/jaw/mouth3.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/mouth/jaw/mouth3.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/mouth/mouth1.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/mouth/mouth1.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/mouth/mouth2.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/mouth/mouth2.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/nose
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/nose/nose1.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/nose/nose1.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/nose2.L
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/neck/head/nose2.R
    m_Weight: 1
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/middle1.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/middle1.L/middle2.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/middle1.L/middle2.L/middle3.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/pinky1.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/pinky1.L/pinky2.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/pinky1.L/pinky2.L/pinky3.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/pointer1.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/pointer1.L/pointer2.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/pointer1.L/pointer2.L/pointer3.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/ring1.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/ring1.L/ring2.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/ring1.L/ring2.L/ring3.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/thumb1.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/thumb1.L/thumb2.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.L/bicep.L/forearm.L/hand.L/thumb1.L/thumb2.L/thumb3.L
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/middle1.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/middle1.R/middle2.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/middle1.R/middle2.R/middle3.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/pinky1.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/pinky1.R/pinky2.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/pinky1.R/pinky2.R/pinky3.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/pointer1.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/pointer1.R/pointer2.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/pointer1.R/pointer2.R/pointer3.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/ring1.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/ring1.R/ring2.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/ring1.R/ring2.R/ring3.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/thumb1.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/thumb1.R/thumb2.R
    m_Weight: 0
  - m_Path: Base_Armature/Base/root/spine1/spine2/spine3/spine4/shoulder.R/bicep.R/forearm.R/hand.R/thumb1.R/thumb2.R/thumb3.R
    m_Weight: 0
  - m_Path: Base_Armature/elbowIK.L
    m_Weight: 0
  - m_Path: Base_Armature/elbowIK.R
    m_Weight: 0
  - m_Path: Biceps
    m_Weight: 0
  - m_Path: Brows
    m_Weight: 0
  - m_Path: Bust
    m_Weight: 0
  - m_Path: Calves
    m_Weight: 0
  - m_Path: Chest
    m_Weight: 0
  - m_Path: Collar
    m_Weight: 0
  - m_Path: Elbows
    m_Weight: 0
  - m_Path: EyeLashes
    m_Weight: 0
  - m_Path: Eyes
    m_Weight: 0
  - m_Path: Feet
    m_Weight: 0
  - m_Path: Forearms
    m_Weight: 0
  - m_Path: Hands
    m_Weight: 0
  - m_Path: Head
    m_Weight: 0
  - m_Path: Hips
    m_Weight: 0
  - m_Path: Knees
    m_Weight: 0
  - m_Path: Neck
    m_Weight: 0
  - m_Path: Shoulders
    m_Weight: 0
  - m_Path: Teeth
    m_Weight: 0
  - m_Path: Thighs
    m_Weight: 0
  - m_Path: Torso
    m_Weight: 0
