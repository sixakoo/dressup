using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "ItemManager", menuName = "ScriptableObjects/ItemManager", order = 0)]
public class ItemManager : ScriptableObject
{
    public List<OutfitData> outfitDatas;
    public List<MakeupData> makeupDatas;
    public Texture2D emptyTex;
    public List<Texture2D> eyemakeUpLayout;
    public List<Texture2D> blushLayout;
    public List<Texture2D> lipstickLayout;

    public const string LEFT = ".L";
    public const string RIGHT = ".R";
    public enum AccessorySelection
    {
        Left,
        Right,
        Both
    }

    public int GetMakeUpLayoutCount(MakeupData.Part _part)
    {
        switch (_part)
        {
            case MakeupData.Part.None:
                return -1;
            case MakeupData.Part.EyeMakeup:
                return eyemakeUpLayout.Count;
            case MakeupData.Part.Blusher:
                return blushLayout.Count;
            case MakeupData.Part.Lipstick:
                return lipstickLayout.Count;
            default:
                return -1;
        }
    }

    public OutfitData GetOutfitData(string outfitName, string tag = "")
    {
        if(!string.IsNullOrEmpty(tag)) //filter accessory
        {
            try
            {
                return outfitDatas.Where(x => x.outfitName == outfitName).Single(x => x.tag == tag);
            }
            catch 
            { 
                Debug.LogError("Accessory not found : " + outfitName); 
                return null; 
            }
        }
        foreach (var item in outfitDatas) //normal outfit
        {
            if(item.outfitName == outfitName)
            {
                return item;
            }
        }
        Debug.LogError("Outfit not found! : " + outfitName);
        return null;
    }

    public MakeupData GetMakeupData(string makeupName)
    {
        foreach (var item in makeupDatas)
        {
            if (item.makeupName == makeupName)
            {
                return item;
            }
        }
        Debug.LogError("Makeup not found! : " + makeupName);
        return null;
    }

    public void AssignMakeupIndexes()
    {
        for (int i = 0; i < makeupDatas.Count; i++)
        {
            makeupDatas[i].index = i;
        }
    }

    public List<OutfitData> GetAccessoriesGroup(List<OutfitData> dataList)
    {
        List<OutfitData> group = new List<OutfitData>();
        foreach (var a in dataList.Where(x => x.outfitType == OutfitData.OufitType.Accessories))
        {
            bool existed = false;
            foreach (var g in group)
            {
                if (g.outfitName == a.outfitName)
                {
                    existed = true;
                    break;
                }
            }
            if (existed)
            {
                continue;
            }
            group.Add(a);
        }
        return group;
    }

    public bool IsPairAccessory(OutfitData accessory)
    {
        foreach (var a in outfitDatas.Where(x => x.outfitType == OutfitData.OufitType.Accessories))
        {
            if(a != accessory && accessory.outfitName == a.outfitName)
            {
                return true;
            }
        }
        return false;
    }

    public List<OutfitData> GetAccessoryOptions(OutfitData accessory)
    {
        List<OutfitData> accessories = new List<OutfitData>(2) { null, null };
        bool firstPair = false;
        foreach (var a in outfitDatas.Where(x => x.outfitType == OutfitData.OufitType.Accessories))
        {
            if (a.outfitName == accessory.outfitName)
            {
                accessories[a.tag.Contains(LEFT) ? 0 : 1] = a;
                if (firstPair)
                {
                    break;
                }
                firstPair = true;
            }
        }
        return accessories;
    }
}
