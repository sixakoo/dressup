using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CharacterAssembler : MonoBehaviour
{
     
    //3d 
    public string headResource = "Heads/BaseHead";

    //TODO remove this method and call ConstructCharacter() from the script that instantiates this object
    void Start()
    {

        ConstructCharacter();
    }


    /// <summary>
    /// Character construction method.
    /// </summary>
    void ConstructCharacter()
    {

        // 1. reset object to zero-position to avoid issues with world/local during combine
        Vector3 originalPosition = this.transform.position;
        this.transform.position = Vector3.zero;


        // 2. instantiate head
        GameObject headGO = Instantiate(Resources.Load(headResource)) as GameObject;
        headGO.transform.parent = this.transform;


        // 3. get base mesh by name 
        SkinnedMeshRenderer[] smRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();

        SkinnedMeshRenderer smrBase = FindByName(smRenderers, "base");

        if (smrBase != null)
            Debug.Log("Base mesh successfully loaded with " + smrBase.bones.Length + " bones.");
        else
            Debug.LogError("Base mesh is invalid.");

        // 4. keep a list of objects to destroy (at the end of the script)
        List<SkinnedMeshRenderer> toDestroy = new List<SkinnedMeshRenderer>();


        // 5. get bone information from base and destroy it
        List<Transform> bones = new List<Transform>();
        Hashtable bonesByHash = new Hashtable();
        List<BoneWeight> boneWeights = new List<BoneWeight>();

        //keep bone info
        int boneIndex = 0;
        foreach (Transform bone in smrBase.bones)
        {

            bones.Add(bone);
            bonesByHash.Add(bone.name, boneIndex);
            boneIndex++;
        }

        //keep bindposes 
        List<Matrix4x4> bindposes = new List<Matrix4x4>();

        for (int b = 0; b < bones.Count; b++)
            bindposes.Add(bones[b].worldToLocalMatrix * transform.worldToLocalMatrix);

        //destroy
        toDestroy.Add(smrBase);


        // 6. Keep a list of combine instances as we start digging into objects.

        List<CombineInstance> combineInstances = new List<CombineInstance>();


        // 7. get body smr, alter UVs, insert into combine, delete

        Vector2[] uvs;
        List<Vector2> totalUVs = new List<Vector2>();

        SkinnedMeshRenderer smrBody = FindByName(smRenderers, "body");

        uvs = smrBody.sharedMesh.uv;

        for (int n = 0; n < uvs.Length; n++)
            uvs[n] = new Vector2(uvs[n].x * 0.5f, uvs[n].y);

        //smrBody.sharedMesh.uv = uvs;
        totalUVs.AddRange(uvs);

        InsertSMRToCombine(smrBody, bonesByHash, boneWeights, combineInstances);

        toDestroy.Add(smrBody);


        // 8. get head, alter UVs, insert into combine, delete

        SkinnedMeshRenderer smrHead = FindByName(smRenderers, "cabeca"); //TODO fbx should contain 'head'

        uvs = smrHead.sharedMesh.uv;

        for (int n = 0; n < uvs.Length; n++)
            uvs[n] = new Vector2(uvs[n].x * 0.5f, uvs[n].y);

        //smrHead.sharedMesh.uv = uvs;
        totalUVs.AddRange(uvs);

        InsertSMRToCombine(smrHead, bonesByHash, boneWeights, combineInstances);

        toDestroy.Add(smrHead);


        // 9. get uniform smr, alter UVs, insert into combine, delete

        SkinnedMeshRenderer smrUniform = FindByName(smRenderers, "uniform");

        uvs = smrUniform.sharedMesh.uv;

        for (int n = 0; n < uvs.Length; n++)
            uvs[n] = new Vector2(0.5f + uvs[n].x * 0.5f, uvs[n].y);

        //smrUniform.sharedMesh.uv = uvs;
        totalUVs.AddRange(uvs);

        InsertSMRToCombine(smrUniform, bonesByHash, boneWeights, combineInstances);

        toDestroy.Add(smrUniform);


        // 10. get shirt smr, alter UVs, insert into combine, delete

        SkinnedMeshRenderer smrShirt = FindByName(smRenderers, "shirt");

        uvs = smrShirt.sharedMesh.uv;

        for (int n = 0; n < uvs.Length; n++)
            uvs[n] = new Vector2(0.5f + uvs[n].x * 0.5f, uvs[n].y);

        //smrShirt.sharedMesh.uv = uvs;
        totalUVs.AddRange(uvs);

        InsertSMRToCombine(smrShirt, bonesByHash, boneWeights, combineInstances);

        toDestroy.Add(smrShirt);





        //combine
        //add an empty skinned mesh renderer, and combine meshes into it
        SkinnedMeshRenderer r = gameObject.AddComponent<SkinnedMeshRenderer>();

        r.sharedMesh = new Mesh();
        r.sharedMesh.CombineMeshes(combineInstances.ToArray());
        r.sharedMesh.uv = totalUVs.ToArray();

        r.bones = bones.ToArray();
        r.rootBone = bones[0]; // TODO we can search bonehash for the name of the root node
        r.sharedMesh.boneWeights = boneWeights.ToArray();
        r.sharedMesh.bindposes = bindposes.ToArray();

        //make shadermanager texture

        //apply

        //late destroy all skinnedmeshrenderers

        //special destroy for head to get rid of the extra bip
        Object.Destroy(smrHead.transform.parent.gameObject);

        //then all smrs
        foreach (SkinnedMeshRenderer t in toDestroy)
        {

            // TODO destroy unnecessary bips
            //            Transform bipRoot = t.gameObject.transform.FindChild("Bip001");
            //
            //            if(bipRoot != null) 
            //                Object.Destroy(bipRoot);

            Object.Destroy(t.gameObject);
        }

        //material 
        r.material = ShaderManager.Instance.GetCharacterMaterial(
            teamString,
            uniformIndex,
            faceTextureName,
            uniformNumber);

        //recalculate bounds and return to original position
        r.sharedMesh.RecalculateBounds();
        this.transform.position = originalPosition;
    }







    #region extra methods

    private void InsertSMRToCombine(SkinnedMeshRenderer smr, Hashtable boneHash,
                                     List<BoneWeight> boneWeights, List<CombineInstance> combineInstances)
    {

        BoneWeight[] meshBoneweight = smr.sharedMesh.boneWeights;

        // remap bone weight bone indexes to the hashtable obtained from base object
        foreach (BoneWeight bw in meshBoneweight)
        {

            BoneWeight bWeight = bw;

            bWeight.boneIndex0 = (int)boneHash[smr.bones[bw.boneIndex0].name];
            bWeight.boneIndex1 = (int)boneHash[smr.bones[bw.boneIndex1].name];
            bWeight.boneIndex2 = (int)boneHash[smr.bones[bw.boneIndex2].name];
            bWeight.boneIndex3 = (int)boneHash[smr.bones[bw.boneIndex3].name];

            boneWeights.Add(bWeight);
        }

        //add the smr to the combine list; also add to destroy list
        CombineInstance ci = new CombineInstance();
        ci.mesh = smr.sharedMesh;

        ci.transform = smr.transform.localToWorldMatrix;
        combineInstances.Add(ci);
    }


    /// <summary>
    /// Finds a SkinnedMeshRenderer in the list.
    /// </summary>
    /// <returns>Found SMR.</returns>
    /// <param name="source">Source array to search.</param>
    /// <param name="name">Name of the SMR to be searched.</param>
    private SkinnedMeshRenderer FindByName(SkinnedMeshRenderer[] source, string name)
    {

        SkinnedMeshRenderer target = null;

        foreach (SkinnedMeshRenderer s in source)
        {

            if (s.name.Contains(name))
            {

                target = s;
                break;
            }
        }

        if (target == null)
            Debug.LogError("SkinnedMeshRenderer " + name + " not found.");

        return target;
    }

    #endregion
}